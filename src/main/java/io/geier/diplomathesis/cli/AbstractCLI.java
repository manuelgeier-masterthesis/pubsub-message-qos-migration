package io.geier.diplomathesis.cli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class AbstractCLI extends Thread {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractCLI.class);

    private final int consolePort;
    private final Map<String, Command> commands = new HashMap<>();

    public AbstractCLI(final int consolePort) {
        super("cli");
        this.consolePort = consolePort;

        registerDefaultCommands();
    }

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(consolePort);

            while (true) {
                Socket socket = serverSocket.accept();
                PrintWriter out = new PrintWriter(socket.getOutputStream());

                out.println("Welcome in the console :)");

                try {
                    Scanner scanner = new Scanner(socket.getInputStream());
                    while (true) {
                        out.print("> ");
                        out.flush();

                        String enteredCommand = scanner.nextLine();
                        enteredCommand = enteredCommand.trim();

                        Scanner commandScanner = new Scanner(enteredCommand);
                        if (commandScanner.hasNext()) {
                            String commandTrigger = commandScanner.next();
                            if (commands.containsKey(commandTrigger)) {
                                Command command = commands.get(commandTrigger);
                                Matcher matcher = command.commandPattern.matcher(enteredCommand);
                                boolean isValid = matcher.find();
                                if (isValid) {
                                    try {
                                        command.commandAction.accept(new CommandInput(command, matcher, out));
                                    } catch (Exception e) {
                                        LOG.warn("Error occurred! :(", e);
                                        e.printStackTrace();
                                        out.println("Error occurred! :(");
                                        out.println(e.getMessage());
                                    }
                                } else {
                                    out.println("wrong syntax: " + command.commandDescription);
                                }
                            } else {
                                out.println("Unknown command :/ Type 'help' to get more information.");
                            }
                            out.flush();
                        }
                    }
                } catch (NoSuchElementException e) {
                    // socket quit
                } finally {
                    socket.close();
                }
            }

        } catch (IOException e) {
            LOG.error("error", e);
        }
    }

    private void registerDefaultCommands() {
        addCommand("help", new Command(
            "^help$",
            "help",
            commandInput -> {
                String commandsString = commands.keySet().stream().collect(Collectors.joining(", "));
                commandInput.out.println("Commands: " + commandsString);
            }));
    }

    public void addCommand(String key, Command command) {
        commands.put(key, command);
    }

    public class Command {
        public Pattern commandPattern;
        public String commandDescription;
        public Consumer<CommandInput> commandAction;

        public Command(String commandRegEx, String commandDescription, Consumer<CommandInput> commandAction) {
            this.commandPattern = Pattern.compile(commandRegEx);
            this.commandDescription = commandDescription;
            this.commandAction = commandAction;
        }
    }

    public class CommandInput {
        public Command command;
        public Matcher matcher;
        public PrintWriter out;

        public CommandInput(Command command, Matcher matcher, PrintWriter out) {
            this.command = command;
            this.matcher = matcher;
            this.out = out;
        }
    }
}
