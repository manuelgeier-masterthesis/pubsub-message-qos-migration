package io.geier.diplomathesis.messaging;

import io.geier.diplomathesis.messaging.comparator.GlobalMessageIdComparator;

import java.util.Objects;

public class GlobalMessageId {

    private String globalMessageId;

    private static final GlobalMessageIdComparator globalMessageIdComparator = new GlobalMessageIdComparator();

    public GlobalMessageId() {
    }

    public GlobalMessageId(String publisherId, String messageId) {
        this(publisherId + "-" + messageId);
    }

    public GlobalMessageId(String globalMessageId) {
        isValidFormatOrThrowException(globalMessageId);
        this.globalMessageId = globalMessageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GlobalMessageId that = (GlobalMessageId) o;
        return Objects.equals(globalMessageId, that.globalMessageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(globalMessageId);
    }

    public static GlobalMessageId min(GlobalMessageId globalMessageId1, GlobalMessageId globalMessageId2) {
        if (globalMessageId1.isGreaterTo(globalMessageId2)) {
            return globalMessageId2;
        }
        return globalMessageId1;
    }

    public static GlobalMessageId max(GlobalMessageId globalMessageId1, GlobalMessageId globalMessageId2) {
        if (globalMessageId1.isGreaterTo(globalMessageId2)) {
            return globalMessageId1;
        }
        return globalMessageId2;
    }

    public boolean isEqualTo(GlobalMessageId otherGlobalMessageId) {
        return globalMessageIdComparator.compare(this, otherGlobalMessageId) == 0;
    }

    public boolean isLessTo(GlobalMessageId otherGlobalMessageId) {
        return globalMessageIdComparator.compare(this, otherGlobalMessageId) < 0;
    }

    public boolean isLessOrEqualTo(GlobalMessageId otherGlobalMessageId) {
        return globalMessageIdComparator.compare(this, otherGlobalMessageId) <= 0;
    }

    public boolean isGreaterTo(GlobalMessageId otherGlobalMessageId) {
        return globalMessageIdComparator.compare(this, otherGlobalMessageId) > 0;
    }

    public boolean isGreaterOrEqualTo(GlobalMessageId otherGlobalMessageId) {
        return globalMessageIdComparator.compare(this, otherGlobalMessageId) >= 0;
    }

    public static boolean isValidFormat(String globalMessageId) {
        return globalMessageId.split("-").length == 2;
    }

    public static void isValidFormatOrThrowException(String globalMessageId) {
        if (!isValidFormat(globalMessageId)) {
            throw new IllegalArgumentException("wrong format: PUBLISHER_ID-MESSAGE_ID. actual globalMessageId=" + globalMessageId);
        }
    }

    public String getGlobalMessageId() {
        return toString();
    }

    @Override
    public String toString() {
        return globalMessageId;
    }
}
