package io.geier.diplomathesis.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.IOException;
import java.nio.charset.Charset;

public class MqttPayload {

    static ObjectMapper objectMapper = new ObjectMapper();

    MqttPayloadHeader header;
    MqttPayloadBody body;

    public MqttPayload() {
    }

    public MqttPayload(MqttPayloadHeader header, MqttPayloadBody body) {
        this.header = header;
        this.body = body;
    }

    public MqttPayloadHeader getHeader() {
        return header;
    }

    public void setHeader(MqttPayloadHeader header) {
        this.header = header;
    }

    public MqttPayloadBody getBody() {
        return body;
    }

    public void setBody(MqttPayloadBody body) {
        this.body = body;
    }

    public ByteBuf toByteBuf() {
        return Unpooled.wrappedBuffer(toBytes());
    }

    public byte[] toBytes() {
        return toString().getBytes();
    }

    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static MqttPayload fromBytes(byte[] bytes) {
        return fromJsonString(new String(bytes));
    }

    public static MqttPayload fromByteBuf(ByteBuf byteBuf) {
        return fromJsonString(byteBuf.toString(Charset.defaultCharset()));
    }

    private static MqttPayload fromJsonString(String jsonString) {
        try {
            return objectMapper.readValue(jsonString, MqttPayload.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
