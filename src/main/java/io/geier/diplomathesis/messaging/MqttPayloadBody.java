package io.geier.diplomathesis.messaging;

public class MqttPayloadBody {

    private String payload;

    public MqttPayloadBody() {
    }

    public MqttPayloadBody(String payload) {
        this.payload = payload;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

//
//    public MQTTPayloadBody(ByteBuf payload) {
//        this.payload = payload.toString(Charsets.UTF_8);
//    }
//
//    public ByteBuf getPayload() {
//        return Unpooled.wrappedBuffer(payload.getBytes());
//    }
}
