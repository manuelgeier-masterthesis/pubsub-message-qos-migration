package io.geier.diplomathesis.messaging;

public class MqttPayloadHeader {

    private GlobalMessageId globalMessageId;

    public MqttPayloadHeader() {
    }

    public MqttPayloadHeader(GlobalMessageId globalMessageId) {
        this.globalMessageId = globalMessageId;
    }

    public MqttPayloadHeader(String globalMessageId) {
        this.globalMessageId = new GlobalMessageId(globalMessageId);
    }

    public GlobalMessageId getGlobalMessageId() {
        return globalMessageId;
    }

    public void setGlobalMessageId(GlobalMessageId globalMessageId) {
        this.globalMessageId = globalMessageId;
    }
}
