package io.geier.diplomathesis.messaging.comparator;

import io.geier.diplomathesis.messaging.GlobalMessageId;

import java.util.Comparator;

public class GlobalMessageIdComparator implements Comparator<GlobalMessageId> {
    @Override
    public int compare(GlobalMessageId gmi1, GlobalMessageId gmi2) {
        Long gmi1AsLong = Long.parseLong(gmi1.toString().split("-")[1]);
        Long gmi2AsLong = Long.parseLong(gmi2.toString().split("-")[1]);
        return gmi1AsLong.compareTo(gmi2AsLong);
    }
}
