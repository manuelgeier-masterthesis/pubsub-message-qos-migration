package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.Subscription;
import io.netty.handler.codec.mqtt.MqttQoS;

import java.util.List;
import java.util.Map;

public interface BrokerMigrationBridge {

    boolean clientIdExists(String clientId);

    void removeSubscription(String clientId, String topic);

    void addSubscription(String clientId, String topicName, MqttQoS qos);

    Map<String, List<Subscription>> getSubscriptions();

    void subscribeClient(String clientId, List<Migration.Subscription> subscriptions);

    List<Subscription> getSubscriptionsByClientId(String clientId);

    void disconnectClient(String clientId);

    void publish(String clientId, List<MigrationMessageStore.StoredMessage> storedMessages);

}
