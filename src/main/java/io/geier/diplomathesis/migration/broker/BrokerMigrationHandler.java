package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.geier.diplomathesis.migration.communication.MigrationPacketHandler;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncPacket;
import io.geier.diplomathesis.migration.communication.packet.MigToAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BrokerMigrationHandler extends MigrationHandlerBase implements MigrationPacketHandler {

    private static final Logger LOG = LoggerFactory.getLogger(BrokerMigrationHandler.class);

    private final MigrationHandlerSource migrationHandlerSource;
    private final MigrationHandlerTarget migrationHandlerTarget;

    public BrokerMigrationHandler(final String thisBrokerHost,
                                  final int thisBrokerPort,
                                  final MqttMessageBarrier messageBarrier,
                                  final MigrationStore migrationStore,
                                  final MigrationMessageStore migrationMessageStore,
                                  final BrokerMigrationBridge migrationBridge,
                                  final MigrationPacketService migrationPacketService) {
        this(thisBrokerHost, thisBrokerPort, messageBarrier, migrationStore, migrationMessageStore, migrationBridge, migrationPacketService, null);
    }

    public BrokerMigrationHandler(final String thisBrokerHost,
                                  final int thisBrokerPort,
                                  final MqttMessageBarrier messageBarrier,
                                  final MigrationStore migrationStore,
                                  final MigrationMessageStore migrationMessageStore,
                                  final BrokerMigrationBridge migrationBridge,
                                  final MigrationPacketService migrationPacketService,
                                  final Metric metric) {
        super(migrationBridge, messageBarrier, migrationStore, migrationMessageStore, thisBrokerHost, thisBrokerPort, migrationPacketService, metric);
        this.migrationHandlerSource = new MigrationHandlerSource(this);
        this.migrationHandlerTarget = new MigrationHandlerTarget(this);
    }

    synchronized public MigrationHandlerMigrateStatus migrate(final String targetBrokerHost, final int targetBrokerMessagePort, final String clientId, final String clientHost, final int clientMigPort) {
        return migrationHandlerSource.migrate(targetBrokerHost, targetBrokerMessagePort, clientId, clientHost, clientMigPort);
    }

    @Override
    public synchronized void handle(Channel channel, MigrationPacket packet) throws MigrationException {
        if (packet instanceof MigratePacket) {
            migrationHandlerTarget.handleMigratePacket(channel, (MigratePacket) packet);
        } else if (packet instanceof MigAckPacket) {
            migrationHandlerSource.handleMigAckPacket((MigAckPacket) packet);
        } else if (packet instanceof MigSyncPacket) {
            migrationHandlerSource.handleMigSyncPacket((MigSyncPacket) packet);
        } else if (packet instanceof MigSyncAckPacket) {
            migrationHandlerTarget.handleMigSyncAckPacket((MigSyncAckPacket) packet);
        } else if (packet instanceof MigToAckPacket) {
            migrationHandlerTarget.handleMigToAckPacket((MigToAckPacket) packet);
        } else {
            LOG.error("Unknown packet. packet={}", packet);
        }
    }

    @Override
    protected MigrationRole getRole() {
        return null;
    }
}
