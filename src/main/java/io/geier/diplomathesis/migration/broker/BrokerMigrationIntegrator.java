package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0Target;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1Target;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2Target;
import io.geier.diplomathesis.migration.communication.MigrationPacketServer;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BrokerMigrationIntegrator {

    private static final Logger LOG = LoggerFactory.getLogger(BrokerMigrationIntegrator.class);

    private BrokerMigrationHandler migrationHandler = null;
    private final MqttMessageBarrier mqttMessageBarrier;
    private final MigrationStore migrationStore;
    private final MigrationMessageStore migrationMessageStore;

    private Metric metric = null;
    private boolean active = false;

    public BrokerMigrationIntegrator() {
        this(false);
    }

    public BrokerMigrationIntegrator(boolean withMetrics) {
        if (withMetrics) {
            this.metric = new Metric();
        }
        migrationMessageStore = new MigrationMessageStore();
        migrationStore = new MigrationStore();
        MqttMessageBarrierQoS0Source messageBarrierQoS0Source = new MqttMessageBarrierQoS0Source();
        MqttMessageBarrierQoS0Target messageBarrierQoS0Target = new MqttMessageBarrierQoS0Target();
        MqttMessageBarrierQoS0 messageBarrierQoS0 = new MqttMessageBarrierQoS0(messageBarrierQoS0Source, messageBarrierQoS0Target);
        MqttMessageBarrierQoS1Source messageBarrierQoS1Source = new MqttMessageBarrierQoS1Source();
        MqttMessageBarrierQoS1Target messageBarrierQoS1Target = new MqttMessageBarrierQoS1Target();
        MqttMessageBarrierQoS1 messageBarrierQoS1 = new MqttMessageBarrierQoS1(messageBarrierQoS1Source, messageBarrierQoS1Target);
        MqttMessageBarrierQoS2Source messageBarrierQoS2Source = new MqttMessageBarrierQoS2Source();
        MqttMessageBarrierQoS2Target messageBarrierQoS2Target = new MqttMessageBarrierQoS2Target();
        MqttMessageBarrierQoS2 messageBarrierQoS2 = new MqttMessageBarrierQoS2(messageBarrierQoS2Source, messageBarrierQoS2Target);
        mqttMessageBarrier = new MqttMessageBarrier(migrationStore, migrationMessageStore, messageBarrierQoS0, messageBarrierQoS1, messageBarrierQoS2);
    }

    public void integrate(String thisBrokerHost, int thisBrokerPort, int migrationMessageServerPort, BrokerMigrationBridge migrationBridge) {
        LOG.info("Initializing Migration Message Service...");
        MigrationPacketService migrationPacketService = new MigrationPacketService();

        LOG.info("Initializing Migration Handler...");
        migrationHandler = new BrokerMigrationHandler(thisBrokerHost, thisBrokerPort,
                mqttMessageBarrier, migrationStore, migrationMessageStore, migrationBridge, migrationPacketService,
                this.metric);

        LOG.info("Initializing Migration Message Server...");
        MigrationPacketServer messageServer = new MigrationPacketServer(migrationMessageServerPort, migrationHandler);
        messageServer.start();
        LOG.info("Migration Message Server started. host={}, port={}", thisBrokerHost, migrationMessageServerPort);

        this.active = true;
    }

    public boolean isActive() {
        return active;
    }

    public BrokerMigrationHandler getMigrationHandler() {
        return migrationHandler;
    }

    public MqttMessageBarrier getMessageBarrier() {
        return mqttMessageBarrier;
    }

    public MigrationStore getMigrationStore() {
        return migrationStore;
    }

    public MigrationMessageStore getMigrationMessageStore() {
        return migrationMessageStore;
    }
}
