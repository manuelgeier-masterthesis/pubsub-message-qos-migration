package io.geier.diplomathesis.migration.broker;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Metric {

    public enum Type {
        PRODUCED, RECEIVED
    }

    File outputFolder = new File("/shared/output");
    File outputFile;
    OutputStreamWriter writer;

    public Metric() {
        outputFolder.mkdirs();
        outputFile = new File(outputFolder, "events.csv");

        try {
            writer = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(outputFile)));
            writer.write(String.format("timestamp,event,migrationId%n"));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addEvent(String eventName, String migrationId) {
        new Thread(() -> {
            long timestamp = System.currentTimeMillis();
            try {
                synchronized (writer) {
                    writer.write(String.format("%d,%s,%s%n", timestamp, eventName, migrationId));
                    writer.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
