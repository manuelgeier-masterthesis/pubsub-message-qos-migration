package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.Subscription;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.geier.diplomathesis.migration.communication.MigrationPacketClient;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public abstract class MigrationHandlerBase {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationHandlerSource.class);

    protected final BrokerMigrationBridge migrationBridge;
    protected final MqttMessageBarrier messageBarrier;
    protected final MigrationStore migrationStore;
    protected final MigrationMessageStore migrationMessageStore;
    protected final String thisBrokerHost;
    protected final int thisBrokerPort;
    protected final MigrationPacketService migrationPacketService;
    protected final Metric metric;

    public MigrationHandlerBase(BrokerMigrationBridge migrationBridge,
                                MqttMessageBarrier messageBarrier,
                                MigrationStore migrationStore,
                                MigrationMessageStore migrationMessageStore,
                                String thisBrokerHost,
                                int thisBrokerPort,
                                MigrationPacketService migrationPacketService,
                                Metric metric) {
        this.migrationBridge = migrationBridge;
        this.messageBarrier = messageBarrier;
        this.migrationStore = migrationStore;
        this.migrationMessageStore = migrationMessageStore;
        this.thisBrokerHost = thisBrokerHost;
        this.thisBrokerPort = thisBrokerPort;
        this.migrationPacketService = migrationPacketService;
        this.metric = metric;
    }

    public MigrationHandlerBase(MigrationHandlerBase migrationHandlerBase) {
        this(migrationHandlerBase.migrationBridge,
                migrationHandlerBase.messageBarrier,
                migrationHandlerBase.migrationStore,
                migrationHandlerBase.migrationMessageStore,
                migrationHandlerBase.thisBrokerHost,
                migrationHandlerBase.thisBrokerPort,
                migrationHandlerBase.migrationPacketService,
                migrationHandlerBase.metric);
    }

    protected Migration createMigration(String migrationId, String clientId, String clientHost, int clientPort, MigrationPacketClient opponentMessageClient, MigrationRole expectedRole, List<Subscription> subscriptions) {
        Migration migration = new Migration(migrationId, clientId, clientHost, clientPort, opponentMessageClient, expectedRole, metric);

        for (Subscription subscription : subscriptions) {
            migration.addSubscription(subscription.getTopicName(), subscription.getRequestedQos());
        }

        return migration;
    }

    protected Migration afterCreateMigration(Migration migration) {
        LOG.info("Migration created. migration={}", migration);
        migrationStore.addMigration(migration);

        return migration;
    }

    public boolean clientIdExists(String clientId) {
        return migrationBridge.clientIdExists(clientId);
    }

    public void addSubscription(String clientId, String topicName, MqttQoS qos) {
        migrationBridge.addSubscription(clientId, topicName, qos);
    }

    public Map<String, List<Subscription>> getSubscriptions() {
        return migrationBridge.getSubscriptions();
    }

    protected void subscribeClient(String clientId, List<Migration.Subscription> migrationSubscriptions) {
        migrationBridge.subscribeClient(clientId, migrationSubscriptions);
    }

    public void removeSubscription(String clientId, String topicName) {
        LOG.info("Unsubscribed. Stop sending messages to client. clientId={}, topicName={}", clientId, topicName);
        migrationBridge.removeSubscription(clientId, topicName);
    }

    protected List<Subscription> getSubscriptionsByClientId(String clientId) {
        return migrationBridge.getSubscriptionsByClientId(clientId);
    }

    protected void disconnectClient(String clientId) {
        migrationBridge.disconnectClient(clientId);
    }

    protected Migration getVerifiedMigration(String migrationId) {
        if (migrationId == null) {
            throw new MigrationException("Migration id is null.");
        }
        Migration migration;
        if (!migrationStore.isMigrating(migrationId)) {
            throw new MigrationException("Unknown migration. migrationId=" + migrationId);
        }
        migration = migrationStore.getMigration(migrationId);
        verifyRole(migration, getRole());
        return migration;
    }

    protected Migration getVerifiedMigration(String migrationId, MigrationState expectedState) {
        Migration migration = getVerifiedMigration(migrationId);
        verifyMigrationState(migration, expectedState);
        return migration;
    }

    protected void verifyMigrationState(Migration migration, MigrationState expectedState) {
        if (migration == null) {
            throw new MigrationException("Migration is null.");
        }

        if (migration.getState() != expectedState) {
            throw new MigrationException("Migration not valid. migrationId=" + migration.getMigrationId() + ", expected=" + expectedState + ", actual=" + migration.getState());
        }
    }

    protected void verifyRole(Migration migration, MigrationRole expectedRole) {
        if (migration == null) {
            throw new MigrationException("Migration is null.");
        }
        switch (migration.getRole()) {
            case SOURCE:
            case TARGET:
                if (migration.getRole() != expectedRole) {
                    throw new MigrationException("Role verification failed. expectedRole=" + expectedRole + ", actualRole=" + migration.getRole());
                }
                break;
            default:
                throw new MigrationException("Unknown role. role=" + migration.getRole());
        }
    }

    protected abstract MigrationRole getRole();

}
