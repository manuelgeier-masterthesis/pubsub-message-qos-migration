package io.geier.diplomathesis.migration.broker;

public enum MigrationHandlerMigrateStatus {
    STARTED, ERROR_CLIENT_NOT_FOUND, ERROR_CLIENT_ALREADY_MIGRATING;
}
