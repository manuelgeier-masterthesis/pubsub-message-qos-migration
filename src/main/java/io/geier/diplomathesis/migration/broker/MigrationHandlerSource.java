package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionStateListenerCallback;
import io.geier.diplomathesis.migration.broker.domain.Subscription;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.geier.diplomathesis.migration.communication.MigrationPacketClient;
import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class MigrationHandlerSource extends MigrationHandlerBase {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationHandlerSource.class);

    protected final BrokerMigrationHandler migrationHandler;

    public MigrationHandlerSource(BrokerMigrationHandler migrationHandler) {
        super(migrationHandler);
        this.migrationHandler = migrationHandler;
    }

    @Override
    protected MigrationRole getRole() {
        return MigrationRole.SOURCE;
    }

    synchronized public MigrationHandlerMigrateStatus migrate(final String targetBrokerHost, final int targetBrokerMessagePort, final String clientId, final String clientHost, final int clientMigPort) {
        if (!clientIdExists(clientId)) {
            LOG.warn("Migration error. Client id not found. clientId={}, targetBrokerHost={}, targetBrokerMessagePort={}", clientId, targetBrokerHost, targetBrokerMessagePort);
            return MigrationHandlerMigrateStatus.ERROR_CLIENT_NOT_FOUND;
        }

        if (migrationStore.isMigratingByClientId(clientId)) {
            LOG.warn("Migration error. Client is already migrating. clientId={}, targetBrokerHost={}, targetBrokerMessagePort={}", clientId, targetBrokerHost, targetBrokerMessagePort);
            return MigrationHandlerMigrateStatus.ERROR_CLIENT_ALREADY_MIGRATING;
        }

        LOG.info("Migrate client. clientId={}, targetBrokerHost={}, targetBrokerMessagePort={}", clientId, targetBrokerHost, targetBrokerMessagePort);
        MigrationPacketClient migrationMessageClient = migrationPacketService.createPacketClient(targetBrokerHost, targetBrokerMessagePort, migrationHandler);

        // get current subscriptions
        List<Subscription> subscriptions = getSubscriptionsByClientId(clientId);

        Migration migration = createSourceMigration(clientId, clientHost, clientMigPort, migrationMessageClient, subscriptions);

        // barrier is active

        // set last global message id that has been processed
        // more messages will not be processed anymore (for QoS 2), since the migration got created and the barrier is active
        for (Migration.Subscription migrationSubscription : migration.getSubscriptions()) {
            String topic = migrationSubscription.getTopicName();
            GlobalMessageId lastProcessedGlobalMessageId = migration.getSubscriptionByTopicName(topic).getLastProcessedGlobalMessageId();
            migrationSubscription.setLastProcessedGlobalMessageId(lastProcessedGlobalMessageId);
        }

        for (Migration.Subscription subscription : migration.getSubscriptions()) {
            switch (subscription.getQos()) {
                case AT_MOST_ONCE:
                    // since we do not sync with QoS 0, it is synced by default
                    subscription.setState(MigrationSubscriptionState.SYNCED);
                    break;
                case AT_LEAST_ONCE:
                case EXACTLY_ONCE:
                    break;
            }
        }

        // open connection
        migrationMessageClient.startSync();

        sendMigMessage(migration);

        return MigrationHandlerMigrateStatus.STARTED;
    }

    Migration createSourceMigration(String clientId, String clientHost, int clientPort, MigrationPacketClient receiverChannel, List<Subscription> subscriptions) {
        String migrationId = UUID.randomUUID().toString();
        Migration migration = createMigration(migrationId, clientId, clientHost, clientPort, receiverChannel, MigrationRole.SOURCE, subscriptions);

        migration.addStateListener((migration1, oldState, newState) -> {
            if (newState == MigrationState.FINISHED) {
                finishMigration(migration);
            }
        });

        // set correct message ids
        for (Migration.Subscription subscription : migration.getSubscriptions()) {
            GlobalMessageId lastProcessedGlobalMessageId = migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(subscription.getTopicName());
            if (lastProcessedGlobalMessageId == null) {
                throw new MigrationException("lastProcessedGlobalMessageId is null. topicName=" + subscription.getTopicName());
            }
            subscription.setLastProcessedGlobalMessageId(lastProcessedGlobalMessageId);
            // subscription.lastProcessedGlobalMessageIdByOpponent will be set with MIGSYNC if necessary by sender
        }

        return afterCreateMigration(migration);
    }

    private void sendMigMessage(Migration migration) {
        List<MigratePacket.MigrateSubscription> migrateSubscriptions = migration.getSubscriptions().stream()
                .map(sub -> new MigratePacket.MigrateSubscription(sub.getQos(), sub.getTopicName(), sub.getLastProcessedGlobalMessageId()))
                .collect(Collectors.toList());

        MigratePacket migratePacket = new MigratePacket(migration.getMigrationId(), migration.getClient().getId(), migration.getClient().getHost(), migration.getClient().getPort(), migrateSubscriptions);
        LOG.info("Send MIG message. clientId={}", migration.getClient().getId());
        migration.getBrokerOpponent().getMessageClient().sendSync(migratePacket);
    }

    public void handleMigAckPacket(MigAckPacket packet) {
        Migration migration = getVerifiedMigration(packet.getMigrationId());

        if (packet.getStatusCode() == MigAckPacket.MigStatus.OK) {
            finalizeMigration(migration);
        } else if (packet.getStatusCode() == MigAckPacket.MigStatus.ERROR) {
            rollbackMigration(migration);
        } else {
            throw new MigrationException("Unknown statusCode. statusCode=" + packet.getStatusCode());
        }
    }

    private void finalizeMigration(Migration migration) {
        if (migration.getSubscriptions().isEmpty()) {
            // if there are no subscriptions, we set it to FINISHED, since the onFINISH trigger will not be called
            migration.setState(MigrationState.FINISHED);
        } else {
            // all subscriptions that did not need any synchronisation will be set to FINISHED
            for (Migration.Subscription subscription : migration.getSubscriptions()) {
                if (subscription.getQos() == MqttQoS.AT_LEAST_ONCE || subscription.getQos() == MqttQoS.EXACTLY_ONCE) {
                    if (subscription.getState() != MigrationSubscriptionState.INITIALIZED && subscription.getState() != MigrationSubscriptionState.SYNCED) {
                        LOG.error("This should not happen. State error. migrationId={}, subscription={}", migration.getMigrationId(), subscription);
                    }
                }
                subscription.setState(MigrationSubscriptionState.FINISHED);
            }

            if (migration.getState() != MigrationState.FINISHED) {
                LOG.error("Migration is not FINISHED. It should be finished, since all Subscriptions should are FINISHED. migrationId={}, subscription={}", migration.getMigrationId(), migration.getSubscriptions());
            }
        }
    }

    private void rollbackMigration(Migration migration) {
        List<MigrationMessageStore.StoredMessage> messagesToSend = new ArrayList<>();

        for (Migration.Subscription subscription : migration.getSubscriptions()) {
            if (shouldStoreMessagesForSubscription(subscription)) {
                messagesToSend.addAll(migrationMessageStore.getAll(migration.getClient().getId(), subscription.getTopicName()));
            }

            // set error state and open sending messages via barrier
            subscription.setState(MigrationSubscriptionState.ERROR);
        }

        publishAndReleaseMessages(migration, messagesToSend);

        migration.setState(MigrationState.ERROR);

        cleanupMigration(migration);
    }

    private void publishAndReleaseMessages(Migration migration, List<MigrationMessageStore.StoredMessage> messagesToSend) {
        if (!messagesToSend.isEmpty()) {
            // mark messages to be force send (this is necessary, because otherwise the state could already prohibit the sending of messages, but we need these to be send!)
            for (MigrationMessageStore.StoredMessage storedMessage : messagesToSend) {
                migrationMessageStore.release(storedMessage);
                migration.getSubscriptionByTopicName(storedMessage.getTopicName()).addMessageToByPass(storedMessage.getGlobalMessageId());
            }

            if (LOG.isDebugEnabled()) {
                List<GlobalMessageId> globalMessageIds = messagesToSend.stream().map(MigrationMessageStore.StoredMessage::getGlobalMessageId).collect(Collectors.toList());
                LOG.debug("Sending missed messages from store. count={}, globalMessageIds={}", messagesToSend.size(), globalMessageIds);
            } else {
                LOG.info("Sending missed messages from store. count={}", messagesToSend.size());
            }

            migrationBridge.publish(migration.getClient().getId(), messagesToSend);
        }
    }

    private void cleanupMigration(Migration migration) {
        migrationMessageStore.release(migration.getClient().getId());
        migrationStore.remove(migration);
    }

    private boolean shouldStoreMessagesForSubscription(Migration.Subscription subscription) {
        switch (subscription.getQos()) {
            case AT_MOST_ONCE:
                // it is ok, if messages are lost, so we do not need to store
                return false;
            case AT_LEAST_ONCE:
            case EXACTLY_ONCE:
                return true;
            default:
                throw new MigrationException("Unknown qos. qos=" + subscription.getQos());
        }
    }

    private void finishMigration(Migration migration) {
        if (migration.getState() != MigrationState.FINISHED) {
            throw new MigrationException("Migration not finished yet. migrationId=" + migration.getMigrationId());
        }

        for (Migration.Subscription subscription : migration.getSubscriptions()) {
            removeSubscription(migration.getClient().getId(), subscription.getTopicName());
        }

        disconnectClient(migration.getClient().getId());

        cleanupMigration(migration);
    }

    public void handleMigSyncPacket(MigSyncPacket packet) {
        Migration migration = getVerifiedMigration(packet.getMigrationId());

        migration.setState(MigrationState.SYNCING);

        List<Migration.Subscription> allSyncedSubscriptions = new ArrayList<>();

        for (MigSyncPacket.MigSyncSubscription subscriptionFromMessage : packet.getSubscriptions()) {
            Migration.Subscription subscription = migration.getSubscriptionByTopicName(subscriptionFromMessage.getTopicName());

            if (subscription.getQos() == MqttQoS.AT_MOST_ONCE) {
                LOG.error("This should not happen. migration={}", migration);
                continue;
            }

            allSyncedSubscriptions.add(subscription);

            GlobalMessageId currentLastProcessedGlobalMessageIdForTopic = migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(subscription.getTopicName());
            GlobalMessageId lastProcessedGlobalMessageId = subscription.getLastProcessedGlobalMessageId();
            GlobalMessageId lastProcessedGlobalMessageIdByOpponent = subscriptionFromMessage.getLastProcessedGlobalMessageId();

            subscription.setLastProcessedGlobalMessageIdByOpponent(lastProcessedGlobalMessageIdByOpponent);
            subscription.setState(MigrationSubscriptionState.SYNCING);
            LOG.info("Migration synchronization. currentLastProcessedGlobalMessageIdForTopic={}, lastProcessedGlobalMessageId={}, lastProcessedGlobalMessageIdByOpponent={}", currentLastProcessedGlobalMessageIdForTopic, lastProcessedGlobalMessageId, lastProcessedGlobalMessageIdByOpponent);

            switch (subscription.getQos()) {
                case AT_LEAST_ONCE:
                    if (lastProcessedGlobalMessageIdByOpponent.isGreaterTo(currentLastProcessedGlobalMessageIdForTopic)) {
                        // B1: will send all messages that B2 already sent and then stop
                        // B2: will continue sending
                        // syncing
                        // synced by barrier
                    } else {
                        // B1 >= B2
                        // B1: will not send anything
                        // B2: will wait and continue sending
                        subscription.setState(MigrationSubscriptionState.SYNCED);
                    }
                    break;

                case EXACTLY_ONCE:
                    if (lastProcessedGlobalMessageIdByOpponent.isGreaterOrEqualTo(lastProcessedGlobalMessageId)) {
                        // send stored

                        // lower bound is excluded, since it was already processed
                        GlobalMessageId lowerBoundExcluded = lastProcessedGlobalMessageId;
                        GlobalMessageId upperBound = GlobalMessageId.min(lastProcessedGlobalMessageIdByOpponent, currentLastProcessedGlobalMessageIdForTopic);
                        if (upperBound.isGreaterTo(lowerBoundExcluded)) {
                            List<MigrationMessageStore.StoredMessage> storedMessages = migrationMessageStore.findAllBetweenLowExAndUp(migration.getClient().getId(), subscription.getTopicName(), lowerBoundExcluded, upperBound);
                            LOG.info("Sending missed messages from store. lowerBound={} (excluded), upperBound={} (included), count={}", lowerBoundExcluded, upperBound, storedMessages.size());
                            publishAndReleaseMessages(migration, storedMessages);
                        }

                        if (lastProcessedGlobalMessageIdByOpponent.isGreaterTo(currentLastProcessedGlobalMessageIdForTopic)) {
                            // syncing
                            // synced by barrier
                        } else {
                            subscription.setState(MigrationSubscriptionState.SYNCED);
                        }
                    } else {
                        // nothing to do
                        subscription.setState(MigrationSubscriptionState.SYNCED);
                    }

                    break;

                default:
                    break;
            }
        }

        MigrationSubscriptionStateListenerCallback stateListenerCallback = new MigrationSubscriptionStateListenerCallback(
                subscriptions -> subscriptions.getState() == MigrationSubscriptionState.SYNCED, () -> {
            migration.setState(MigrationState.SYNCED);
            MigrationHandlerSource.this.sendMigSyncAck(migration);
        });
        stateListenerCallback.addAllAndVerify(allSyncedSubscriptions);
    }

    private void sendMigSyncAck(Migration migration) {
        MigSyncAckPacket message = new MigSyncAckPacket(migration.getMigrationId());
        migration.getBrokerOpponent().getMessageClient().sendSync(message);
    }

}
