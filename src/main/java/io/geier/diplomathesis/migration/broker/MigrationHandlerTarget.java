package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.migration.broker.domain.Subscription;
import io.geier.diplomathesis.migration.communication.MigrationPacketClient;
import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncPacket;
import io.geier.diplomathesis.migration.communication.packet.MigToAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigToPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MigrationHandlerTarget extends MigrationHandlerBase {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationHandlerTarget.class);

    protected final BrokerMigrationHandler migrationHandler;

    public MigrationHandlerTarget(BrokerMigrationHandler migrationHandler) {
        super(migrationHandler);
        this.migrationHandler = migrationHandler;
    }

    @Override
    protected MigrationRole getRole() {
        return MigrationRole.TARGET;
    }

    public void handleMigratePacket(Channel channel, MigratePacket packet) {
        if (migrationStore.isMigrating(packet.getMigrationId())) {
            LOG.warn("Migration already exists. migrationId={}", packet.getMigrationId());
            sendMigAckError(channel, packet.getMigrationId());
        } else {
            MigrationPacketClient sourcePacketClient = migrationPacketService.createPacketClient(channel);
            Migration migration = createMigrationOnTarget(packet.getMigrationId(), packet.getClientId(), packet.getClientHost(), packet.getClientPort(), sourcePacketClient, packet.getSubscriptions());

            // barrier is active

            // open connection to the client
            migration.getClient().getPacketChannel().startSync();

            sendMigToPacketToClient(migration); // TODO error handling if client does not respond
        }
    }

    public Migration createMigrationOnTarget(String migrationId, String clientId, String clientHost, int clientPort, MigrationPacketClient sourceMessageClient, List<MigratePacket.MigrateSubscription> migrationSubscriptions) {
        List<Subscription> subscriptions = migrationSubscriptions.stream().map(migSub -> {
            return new Subscription(clientId, migSub.getTopicName(), migSub.getRequestedQos());
        }).collect(Collectors.toList());

        Migration migration = createMigration(migrationId, clientId, clientHost, clientPort, sourceMessageClient, MigrationRole.TARGET, subscriptions);

        migration.addStateListener((migration2, oldState, newState) -> {
            if (newState == MigrationState.SYNCED) {
                sendMigAck(migration2);
            }
        });

        migration.addStateListener((migration2, oldState, newState) -> {
            if (newState == MigrationState.FINISHED) {
                finishMigration(migration2);
            }
        });

        // communicate with client
        migration.getClient().setPacketChannel(migrationPacketService.createPacketClient(migration.getClient().getHost(), migration.getClient().getPort(), migrationHandler));

        // set correct message ids
        for (MigratePacket.MigrateSubscription migrationSubscription : migrationSubscriptions) {
            String topicName = migrationSubscription.getTopicName();
            Migration.Subscription subscription = migration.getSubscriptionByTopicName(topicName);
            // subscription.lastProcessedGlobalMessageId will be set on INITIALIZED by target
            subscription.setLastProcessedGlobalMessageIdByOpponent(migrationSubscription.getLastProcessedGlobalMessageId());
        }

        return afterCreateMigration(migration);
    }

    private void sendMigToPacketToClient(Migration migration) {
        MigToPacket packet = new MigToPacket(migration.getMigrationId(), thisBrokerHost, thisBrokerPort);

        LOG.info("Send MIGTO. host={}, port={}, packet={}", migration.getClient().getHost(), migration.getClient().getPort(), packet);
        migration.getClient().getPacketChannel().sendSync(packet);
    }

    public void handleMigToAckPacket(MigToAckPacket packet) {
        Migration migration = getVerifiedMigration(packet.getMigrationId());

        subscribeClient(migration.getClient().getId(), migration.getSubscriptions());

        // last processed message for each topic
        // this message was NOT sent to the client. only messages after this one are send.
        for (Migration.Subscription subscription : migration.getSubscriptions()) {
            subscription.setLastProcessedGlobalMessageId(migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(subscription.getTopicName()));
            subscription.setState(MigrationSubscriptionState.SYNCING);
        }

        migration.setState(MigrationState.SYNCING);

        // for QoS 1, 2
        List<MigSyncPacket.MigSyncSubscription> migSyncSubscriptions = new ArrayList<>();

        for (Migration.Subscription subscription : migration.getSubscriptions()) {

            switch (subscription.getQos()) {
                case AT_MOST_ONCE:
                    subscription.setOpponentSYNCED();
                    break;
                case AT_LEAST_ONCE:
                case EXACTLY_ONCE:
                    break;
                default:
                    throw new MigrationException("Unknown qos. qos=" + subscription.getQos());
            }

            if (subscription.getLastProcessedGlobalMessageId().isEqualTo(subscription.getLastProcessedGlobalMessageIdByOpponent())) {
                // synced
                switch (subscription.getQos()) {
                    case AT_MOST_ONCE:
                        subscription.setThisSYNCED();
                        break;
                    case AT_LEAST_ONCE:
                        subscription.setThisSYNCED();
                        subscription.setOpponentSYNCED();
                        break;
                    case EXACTLY_ONCE:
                        subscription.setThisSYNCED();
                        subscription.setOpponentSYNCED();
                        break;
                    default:
                        throw new MigrationException("Unknown qos. qos=" + subscription.getQos());
                }
            } else if (subscription.getLastProcessedGlobalMessageId().isGreaterTo(subscription.getLastProcessedGlobalMessageIdByOpponent())) {
                // ahead
                switch (subscription.getQos()) {
                    case AT_MOST_ONCE:
                        subscription.setThisSYNCED();
                        break;
                    case AT_LEAST_ONCE:
                        subscription.setThisSYNCED();
                        migSyncSubscriptions.add(new MigSyncPacket.MigSyncSubscription(subscription.getTopicName(), subscription.getLastProcessedGlobalMessageId()));
                        break;
                    case EXACTLY_ONCE:
                        subscription.setThisSYNCED();
                        migSyncSubscriptions.add(new MigSyncPacket.MigSyncSubscription(subscription.getTopicName(), subscription.getLastProcessedGlobalMessageId()));
                        break;
                    default:
                        throw new MigrationException("Unknown qos. qos=" + subscription.getQos());
                }
            } else if (subscription.getLastProcessedGlobalMessageId().isLessTo(subscription.getLastProcessedGlobalMessageIdByOpponent())) {
                // behind
                switch (subscription.getQos()) {
                    case AT_MOST_ONCE:
                        // not synced, waiting...
                        break;
                    case AT_LEAST_ONCE:
                        // not synced, waiting...
                        subscription.setOpponentSYNCED();
                        break;
                    case EXACTLY_ONCE:
                        // not synced, waiting...
                        subscription.setOpponentSYNCED();
                        break;
                    default:
                        throw new MigrationException("Unknown qos. qos=" + subscription.getQos());
                }
            } else {
                // error
                LOG.error("This should not happen. subscription={}", subscription);
            }
        }

        // for QoS 1, 2
        if (!migSyncSubscriptions.isEmpty()) {
            MigSyncPacket migSyncPacket = new MigSyncPacket(migration.getMigrationId(), migSyncSubscriptions);
            sendMigSyncMessage(migration.getBrokerOpponent().getMessageClient(), migSyncPacket, migration);
        }

        if (migration.getSubscriptions().isEmpty()) {
            migration.setState(MigrationState.SYNCED);
        }
    }

    private void sendMigSyncMessage(MigrationPacketClient client, MigSyncPacket migSyncPacket, Migration migration) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Send MIGSYNC. migrationId={}, clientId={}, message={}", migration.getMigrationId(), migration.getClient().getId(), migSyncPacket);
        } else {
            LOG.info("Send MIGSYNC. migrationId={}, clientId={}", migration.getMigrationId(), migration.getClient().getId());
        }
        client.sendSync(migSyncPacket);
    }

    public void handleMigSyncAckPacket(MigSyncAckPacket packet) {
        Migration migration = getVerifiedMigration(packet.getMigrationId(), MigrationState.SYNCING);

        for (Migration.Subscription subscription : migration.getSubscriptions()) {
            switch (subscription.getQos()) {
                case AT_MOST_ONCE:
                    // do nothing
                    break;

                case AT_LEAST_ONCE:
                case EXACTLY_ONCE:
                    if (subscription.getState() == MigrationSubscriptionState.SYNCING || subscription.getState() == MigrationSubscriptionState.SYNCING_OPPONENT) {
                        subscription.setOpponentSYNCED();
                    }
                    break;

                default:
                    throw new MigrationException("Unknown qos. qos=" + subscription.getQos());
            }
        }
    }

    private void sendMigAck(Migration migration) {
        MigAckPacket migAckPacket = new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Send MIGACK. id={}, clientId={}, message={}", migration.getMigrationId(), migration.getClient().getId(), migAckPacket);
        } else {
            LOG.info("Send MIGACK. id={}, clientId={}", migration.getMigrationId(), migration.getClient().getId());
        }

        migration.getBrokerOpponent().getMessageClient().send(migAckPacket).addListener(future -> {
            finalizeMigration(migration);
        });
    }

    private void finalizeMigration(Migration migration) {
        if (migration.getSubscriptions().isEmpty()) {
            // if there are no subscriptions, we set it to FINISHED, since the onFINISH trigger will not be called
            migration.setState(MigrationState.FINISHED);
        } else {
            // all subscriptions that did not need any synchronisation will be set to FINISHED
            for (Migration.Subscription subscription : migration.getSubscriptions()) {
                if (subscription.getQos() == MqttQoS.AT_LEAST_ONCE || subscription.getQos() == MqttQoS.EXACTLY_ONCE) {
                    if (subscription.getState() != MigrationSubscriptionState.INITIALIZED && subscription.getState() != MigrationSubscriptionState.SYNCED) {
                        LOG.error("This should not happen. State error. migrationId={}, subscription={}", migration.getMigrationId(), subscription);
                    }
                }
                subscription.setState(MigrationSubscriptionState.FINISHED);
            }

            if (migration.getState() != MigrationState.FINISHED) {
                LOG.error("Migration is not FINISHED. It should be finished, since all Subscriptions should are FINISHED. migrationId={}, subscription={}", migration.getMigrationId(), migration.getSubscriptions());
            }
        }
    }

    private void sendMigAckError(Channel channel, String migrationId) {
        MigAckPacket migAckPacket = new MigAckPacket(migrationId, MigAckPacket.MigStatus.ERROR);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Send MIGACK ERROR. id={}, message={}", migrationId, migAckPacket);
        } else {
            LOG.info("Send MIGACK ERROR. id={}", migrationId);
        }
        channel.writeAndFlush(migAckPacket);
    }

    private void finishMigration(Migration migration) {
        verifyRole(migration, MigrationRole.TARGET);
        verifyMigrationState(migration, MigrationState.FINISHED);

        migration.getClient().getPacketChannel().close();
        migrationStore.remove(migration);
    }

}
