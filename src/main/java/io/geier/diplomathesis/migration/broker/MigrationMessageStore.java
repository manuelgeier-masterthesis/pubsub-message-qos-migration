package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.messaging.comparator.GlobalMessageIdComparator;
import io.geier.diplomathesis.migration.communication.packet.MessageInspector;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MigrationMessageStore {

    private final Logger LOG = LoggerFactory.getLogger(MigrationMessageStore.class);

    private Map<String, GlobalMessageId> processedGlobalMessageIdsPerTopic = new HashMap<>();
    private Map<String, Map<String, Map<GlobalMessageId, StoredMessage>>> clientTopicMessageStore = new HashMap<>();

    private StoredMessageComparator storedMessageComparator = new StoredMessageComparator();

    interface MatchingCondition {
        boolean match(GlobalMessageId globalMessageId, StoredMessage storedMessage);
    }

    public synchronized void processedMessage(MqttPublishMessage msg) {
        String topicName = msg.variableHeader().topicName();
        GlobalMessageId globalMessageId = MessageInspector.getGlobalMessageId(msg);
        if (globalMessageId != null) {
            processedGlobalMessageIdsPerTopic.put(topicName, globalMessageId);
        } else {
            LOG.warn("Unsupported Message format. GlobalMessageId not found.");
        }
    }

    public synchronized void store(String clientId, String topicName, GlobalMessageId globalMessageId, MqttPublishMessage msg) {
        Map<String, Map<GlobalMessageId, StoredMessage>> topicMessageStore = clientTopicMessageStore.computeIfAbsent(clientId, k -> new HashMap<>());
        Map<GlobalMessageId, StoredMessage> messageStore = topicMessageStore.computeIfAbsent(topicName, k -> new HashMap<>());

        // check that messages get not put into store multiple times
        if (!messageStore.containsKey(globalMessageId)) {
            LOG.debug("Message stored. clientId={}, topicName={}, globalMessageId={}", clientId, topicName, globalMessageId);
            messageStore.put(globalMessageId, new StoredMessage(clientId, topicName, msg.fixedHeader().qosLevel(), globalMessageId, msg));
        } else {
            LOG.debug("Message already in store. Not stored again. clientId={}, topicName={}, globalMessageId={}", clientId, topicName, globalMessageId);
        }
    }

    public synchronized GlobalMessageId getLastProcessedGlobalMessageIdForTopic(String topicName) {
        return this.processedGlobalMessageIdsPerTopic.get(topicName);
    }

    public synchronized void clear() {
        processedGlobalMessageIdsPerTopic.clear();
        clientTopicMessageStore.clear();
    }

    public synchronized int size(String clientId, String topicName) {
        Map<GlobalMessageId, StoredMessage> messageStore = getMessageStore(clientId, topicName);
        if (messageStore == null) {
            return 0;
        }
        return messageStore.size();
    }


    public synchronized Collection<? extends StoredMessage> getAll(String clientId, String topicName) {
        Map<GlobalMessageId, StoredMessage> messageStore = getMessageStore(clientId, topicName);
        if (messageStore == null) {
            return new ArrayList<>();
        }
        return messageStore.values();
    }

    private Map<GlobalMessageId, StoredMessage> getMessageStore(String clientId, String topicName) {
        Map<String, Map<GlobalMessageId, StoredMessage>> topicMessageStore = clientTopicMessageStore.get(clientId);
        if (topicMessageStore == null) {
            return null;
        }
        return topicMessageStore.get(topicName);
    }

    public synchronized void release(StoredMessage storedMessage) {
        Map<GlobalMessageId, StoredMessage> messageStore = getMessageStore(storedMessage.clientId, storedMessage.topicName);
        if (messageStore != null) {
            messageStore.remove(storedMessage.globalMessageId);
        }
    }

    public synchronized void release(String clientId) {
        clientTopicMessageStore.remove(clientId);
    }

    public synchronized List<StoredMessage> searchMatching(String clientId, String topicName, MatchingCondition condition) {
        Map<String, Map<GlobalMessageId, StoredMessage>> topicMessageStore = clientTopicMessageStore.get(clientId);

        if (topicMessageStore == null) {
            LOG.debug("No messages stored for client. clientId={}", topicName);
            return new ArrayList<>();
        }

        Map<GlobalMessageId, StoredMessage> messageStore = topicMessageStore.get(topicName);

        if (messageStore == null) {
            LOG.debug("No messages stored for topic. clientId={}, topicName={}", clientId, topicName);
            return new ArrayList<>();
        }

        LOG.debug("searchMatching scanning all stored messages, presents are {}", messageStore.size());

        List<StoredMessage> results = new ArrayList<>();

        for (Map.Entry<GlobalMessageId, StoredMessage> entry : messageStore.entrySet()) {
            StoredMessage storedMsg = entry.getValue();
            if (condition.match(entry.getKey(), entry.getValue())) {
                results.add(storedMsg);
            }
        }

        results.sort(storedMessageComparator);

        return results;
    }

    public synchronized List<StoredMessage> findAllBetweenLowExAndUp(String clientId, String topicName, GlobalMessageId lowerBoundExcluded, GlobalMessageId upperBound) {
        return searchMatching(clientId, topicName, (globalMessageId, storedMessage) -> globalMessageId.isGreaterTo(lowerBoundExcluded) && globalMessageId.isLessOrEqualTo(upperBound));
    }

    public static class StoredMessage {
        private String clientId;
        private String topicName;
        private MqttQoS qos;
        private GlobalMessageId globalMessageId;
        private MqttPublishMessage publishMessage;

        public StoredMessage(String clientId, String topicName, MqttQoS qos, GlobalMessageId globalMessageId, MqttPublishMessage publishMessage) {
            this.clientId = clientId;
            this.topicName = topicName;
            this.qos = qos;
            this.globalMessageId = globalMessageId;
            this.publishMessage = publishMessage;
        }

        public GlobalMessageId getGlobalMessageId() {
            return globalMessageId;
        }

        public MqttPublishMessage getPublishMessage() {
            return publishMessage;
        }

        public String getClientId() {
            return clientId;
        }

        public String getTopicName() {
            return topicName;
        }

        public MqttQoS getQos() {
            return qos;
        }
    }

    private class StoredMessageComparator implements Comparator<StoredMessage> {

        private GlobalMessageIdComparator globalMessageIdComparator = new GlobalMessageIdComparator();

        @Override
        public int compare(StoredMessage o1, StoredMessage o2) {
            return globalMessageIdComparator.compare(o1.globalMessageId, o2.globalMessageId);
        }
    }

}
