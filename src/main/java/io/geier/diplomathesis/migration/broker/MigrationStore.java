package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.migration.broker.domain.Migration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO support multiple migrations
 */
public class MigrationStore {

    private final Logger LOG = LoggerFactory.getLogger(MigrationStore.class);

    private Map<String, Migration> migrations = new HashMap<>();
    private Map<String, Migration> migrationsByClient = new HashMap<>();
    private List<Migration> migrationsHistory = new ArrayList<>();
    private Map<String, List<Migration>> migrationsHistoryByClient = new HashMap<>();

    public synchronized boolean isMigratingByClientId(String clientId) {
        return migrationsByClient.containsKey(clientId);
    }

    public synchronized Migration getMigrationByClientId(String clientId) {
        return migrationsByClient.get(clientId);
    }

    public synchronized boolean isMigrating(String migrationId) {
        return migrations.containsKey(migrationId);
    }

    public synchronized Migration getMigration(String migrationId) {
        return migrations.get(migrationId);
    }

    public synchronized void addMigration(Migration migration) {
        migrations.put(migration.getMigrationId(), migration);
        migrationsByClient.put(migration.getClient().getId(), migration);
    }

    public synchronized void remove(Migration migration) {
        synchronized (migration) {
            migration.getStatistic().stopTime();

            // TODO create wrapper class for synchronized object
            migrations.remove(migration.getMigrationId());
            migrationsByClient.remove(migration.getClient().getId());

            // put to history
            migrationsHistory.add(migration);
            migrationsHistoryByClient.computeIfAbsent(migration.getClient().getId(), (k) -> new ArrayList<>()).add(migration);
        }
    }

    public synchronized Map<String, Migration> getMigrations() {
        return migrations;
    }

    public synchronized List<Migration> getMigrationsHistory() {
        return migrationsHistory;
    }

    public synchronized List<Migration> getMigrationsHistoryByClient(String clientId) {
        return migrationsHistoryByClient.get(clientId);
    }
}
