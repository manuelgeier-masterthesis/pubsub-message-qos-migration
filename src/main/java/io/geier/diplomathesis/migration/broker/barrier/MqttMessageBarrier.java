package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.MigrationMessageStore;
import io.geier.diplomathesis.migration.broker.MigrationStore;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.geier.diplomathesis.migration.communication.packet.MessageInspector;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MqttMessageBarrier {

    private static final Logger LOG = LoggerFactory.getLogger(MqttMessageBarrier.class);

    private final MigrationStore migrationStore;
    private final MigrationMessageStore migrationMessageStore;
    private final MqttMessageBarrierQoS0 messageBarrierQoS0;
    private final MqttMessageBarrierQoS1 messageBarrierQoS1;
    private final MqttMessageBarrierQoS2 messageBarrierQoS2;

    public MqttMessageBarrier(MigrationStore migrationStore,
                              MigrationMessageStore migrationMessageStore,
                              MqttMessageBarrierQoS0 messageBarrierQoS0,
                              MqttMessageBarrierQoS1 messageBarrierQoS1,
                              MqttMessageBarrierQoS2 messageBarrierQoS2) {
        this.migrationStore = migrationStore;
        this.migrationMessageStore = migrationMessageStore;
        this.messageBarrierQoS0 = messageBarrierQoS0;
        this.messageBarrierQoS1 = messageBarrierQoS1;
        this.messageBarrierQoS2 = messageBarrierQoS2;
    }

    public boolean shouldDiscard(MqttPublishMessage pubMessage, String clientId, int messageId, String topicName, MqttQoS qos) {
        GlobalMessageId globalMessageId = MessageInspector.getGlobalMessageId(pubMessage);
        if (globalMessageId == null) {
            LOG.debug("Unsupported message format with MqttPayload. MessageId={}, CId={}, topic={}", messageId, clientId, topicName);
            return false;
        } else {
            return processMessage(pubMessage, clientId, messageId, topicName, qos, globalMessageId);
        }
    }

    public boolean processMessage(MqttPublishMessage pubMessage, String clientId, int messageId, String topicName, MqttQoS qos, GlobalMessageId globalMessageId) {
        boolean shouldDiscardMessage = false;
        try {
            if (migrationStore != null && migrationStore.isMigratingByClientId(clientId)) {

                Migration migration = migrationStore.getMigrationByClientId(clientId);
                LOG.info("Migration is active, CId={}, role={}, topic={}, qos={}", clientId, migration.getRole(), topicName, qos);

                boolean canSendMessage = canSendMessage(pubMessage, clientId, messageId, topicName, qos, globalMessageId);
                if (!canSendMessage) {

                    Migration.Subscription subscription = migration.getSubscriptionByTopicName(topicName);
                    if (subscription.shouldStore()) {
                        if (subscription.canMessageByPass(globalMessageId)) {
                            LOG.info("Store by-pass. globalMessageId={}, CId={}, topic={}, qos={}, reqQos={}", globalMessageId, clientId, topicName, qos, subscription.getQos());
                        } else {
                            migrationMessageStore.store(clientId, topicName, globalMessageId, pubMessage);
                        }
                    }

                    // discard sending
                    shouldDiscardMessage = true;
                }
            }
        } catch (MigrationException ex) {
            LOG.error(ex.getMessage(), ex);
        }
        return shouldDiscardMessage;
    }

    public boolean canSendMessage(MqttPublishMessage pubMessage, String clientId, int messageId, String topicName, MqttQoS msgQoS, GlobalMessageId globalMessageId) {
        boolean canSendMessage = true;

        if (migrationStore.isMigratingByClientId(clientId)) {

            Migration migration = migrationStore.getMigrationByClientId(clientId);
            LOG.info("Migration is active, CId={}, role={}, topic={}, msgQoS={}", clientId, migration.getRole(), topicName, msgQoS);

            synchronized (migration) {
                Migration.Subscription subscription = migration.getSubscriptionByTopicName(topicName);

                if (subscription == null) {
                    LOG.info("Subscription not in migration process. messageId={}, CId={}, topic={}, msgQos={}, barrierQos={} globalMessageId={}", messageId, clientId, topicName, msgQoS, globalMessageId);
                    return true;
                }

                MqttQoS barrierQos = subscription.getQos();

                verifyQos(clientId, messageId, topicName, msgQoS, globalMessageId, subscription, barrierQos);
                LOG.debug("Message check. messageId={}, CId={}, topic={}, msgQos={}, reqQos={}, barrierQos={}, state={}, globalMessageId={}, lastProcessedGlobalMessageId={}", messageId, clientId, topicName, msgQoS, subscription.getQos(), barrierQos, subscription.getState(), globalMessageId, subscription.getLastProcessedGlobalMessageId());

                if (subscription.canMessageByPass(globalMessageId)) {
                    LOG.info("Barrier by-pass. globalMessageId={}, CId={}, topic={}, msgQos={}, reqQos={}", globalMessageId, clientId, topicName, msgQoS, subscription.getQos());
                } else {
                    switch (barrierQos) {
                        case AT_MOST_ONCE:
                            canSendMessage = messageBarrierQoS0.canSendMessage(subscription, globalMessageId, migration);
                            break;
                        case AT_LEAST_ONCE:
                            canSendMessage = messageBarrierQoS1.canSendMessage(subscription, globalMessageId, migration);
                            break;
                        case EXACTLY_ONCE:
                            canSendMessage = messageBarrierQoS2.canSendMessage(subscription, globalMessageId, migration);
                            break;
                        default:
                            LOG.warn("Unknown msgQoS. globalMessageId={}, CId={}, topic={}, msgQoS={}, reqQos={}", globalMessageId,
                                    clientId, topicName, msgQoS, subscription.getQos());
                    }
                }

                if (canSendMessage) {
                    // set first send message id
                    // this only works, because we ask for this message id, before we send it from the internal store
                    if (!subscription.hasFirstSendMessageId()) {
                        subscription.setFirstSendMessageId(globalMessageId);
                    }
                } else {
                    LOG.info("Message dropped. messageId={}, CId={}, topic={}, msgQoS={}, reqQos={}, barrierQos={}, globalMessageId={}, lastProcessedGlobalMessageId={}", messageId, clientId, topicName, msgQoS, subscription.getQos(), barrierQos, globalMessageId, subscription.getLastProcessedGlobalMessageId());
                }
            }
        }

        return canSendMessage;
    }

    private void verifyQos(String clientId, int messageId, String topicName, MqttQoS qos, GlobalMessageId globalMessageId, Migration.Subscription subscription, MqttQoS barrierQos) {
        if (subscription.getQos() != qos) {
            LOG.error("QoS not the same. Assumption broken. messageId={}, CId={}, topic={}, qos={}, reqQos={}, barrierQos={} globalMessageId={}, lastProcessedGlobalMessageId={}", messageId, clientId, topicName, qos, subscription.getQos(), barrierQos, globalMessageId, subscription.getLastProcessedGlobalMessageId());
        }
    }
}
