package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MqttMessageBarrierQoS0 {

    private static final Logger LOG = LoggerFactory.getLogger(MqttMessageBarrierQoS0.class);

    private MqttMessageBarrierQoS0Source messageBarrierQoS0Source;
    private MqttMessageBarrierQoS0Target messageBarrierQoS0Target;

    public MqttMessageBarrierQoS0(MqttMessageBarrierQoS0Source messageBarrierQoS0Source,
                                  MqttMessageBarrierQoS0Target messageBarrierQoS0Target) {
        this.messageBarrierQoS0Source = messageBarrierQoS0Source;
        this.messageBarrierQoS0Target = messageBarrierQoS0Target;
    }

    public boolean canSendMessage(Migration.Subscription subscription, GlobalMessageId globalMessageId, Migration migration) {
        if (migration.getRole() == MigrationRole.SOURCE) {
            return messageBarrierQoS0Source.canSendMessage(subscription, globalMessageId);
        } else if (migration.getRole() == MigrationRole.TARGET) {
            return messageBarrierQoS0Target.canSendMessage(subscription, globalMessageId);
        } else {
            throw new MigrationException("Unknown role. role=" + migration.getRole());
        }
    }

}
