package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MqttMessageBarrierQoS0Target {

    private static final Logger LOG = LoggerFactory.getLogger(MqttMessageBarrierQoS0Target.class);

    public boolean canSendMessage(Migration.Subscription subscription, GlobalMessageId globalMessageId) {
        boolean canSendMessage;

        GlobalMessageId lastProcSource = subscription.getLastProcessedGlobalMessageIdByOpponent();
        GlobalMessageId lastProcTarget = subscription.getLastProcessedGlobalMessageId();

        LOG.debug("Message Barrier decision. globalMessageId={}, lastProcSource={}, lastProcTarget={}", globalMessageId, lastProcSource, lastProcTarget);

        if (lastProcTarget != null && globalMessageId.isLessOrEqualTo(lastProcTarget)) {
            return false;
        }

        switch (subscription.getState()) {
            case INITIALIZED:
                canSendMessage = false;
                break;
            case SYNCING:
            case SYNCING_THIS: // SYNCING_THIS, because we use the same mechanism as with QoS 2
                if (globalMessageId.isLessOrEqualTo(lastProcSource)) {
                    canSendMessage = false;

                    // with the last message we are synced
                    if (globalMessageId.isEqualTo(lastProcSource)) {
                        subscription.setThisSYNCED();
                    }
                } else {
                    // we are beyond and therefore SYNCED
                    // this may happen in case of a gap
                    canSendMessage = true;
                    subscription.setThisSYNCED();
                }
                break;
            case SYNCED:
            case FINISHED:
                canSendMessage = true;
                break;
            case ERROR:
                canSendMessage = false;
                break;
            default:
                throw new MigrationException("Unsupported state. state=" + subscription.getState());
        }

        return canSendMessage;
    }

}
