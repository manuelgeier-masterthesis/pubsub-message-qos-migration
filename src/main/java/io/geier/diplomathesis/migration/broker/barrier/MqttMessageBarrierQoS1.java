package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MqttMessageBarrierQoS1 {

    private static final Logger LOG = LoggerFactory.getLogger(MqttMessageBarrierQoS1.class);

    private MqttMessageBarrierQoS1Source messageBarrierQoS1Source;
    private MqttMessageBarrierQoS1Target messageBarrierQoS1Target;

    public MqttMessageBarrierQoS1(MqttMessageBarrierQoS1Source messageBarrierQoS1Source,
                                  MqttMessageBarrierQoS1Target messageBarrierQoS1Target) {
        this.messageBarrierQoS1Source = messageBarrierQoS1Source;
        this.messageBarrierQoS1Target = messageBarrierQoS1Target;
    }

    public boolean canSendMessage(Migration.Subscription subscription, GlobalMessageId globalMessageId, Migration migration) {
        if (migration.getRole() == MigrationRole.SOURCE) {
            return messageBarrierQoS1Source.canSendMessage(subscription, globalMessageId);
        } else if (migration.getRole() == MigrationRole.TARGET) {
            return messageBarrierQoS1Target.canSendMessage(subscription, globalMessageId);
        } else {
            throw new MigrationException("Unknown role. role=" + migration.getRole());
        }
    }

}
