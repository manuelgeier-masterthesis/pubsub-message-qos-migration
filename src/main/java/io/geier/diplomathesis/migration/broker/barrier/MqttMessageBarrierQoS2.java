package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MqttMessageBarrierQoS2 {

    private static final Logger LOG = LoggerFactory.getLogger(MqttMessageBarrierQoS2.class);

    private MqttMessageBarrierQoS2Source messageBarrierQoS2Source;
    private MqttMessageBarrierQoS2Target messageBarrierQoS2Target;

    public MqttMessageBarrierQoS2(MqttMessageBarrierQoS2Source messageBarrierQoS2Source,
                                  MqttMessageBarrierQoS2Target messageBarrierQoS2Target) {
        this.messageBarrierQoS2Source = messageBarrierQoS2Source;
        this.messageBarrierQoS2Target = messageBarrierQoS2Target;
    }

    public boolean canSendMessage(Migration.Subscription subscription, GlobalMessageId globalMessageId, Migration migration) {
        if (migration.getRole() == MigrationRole.SOURCE) {
            return messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);
        } else if (migration.getRole() == MigrationRole.TARGET) {
            return messageBarrierQoS2Target.canSendMessage(subscription, globalMessageId);
        } else {
            throw new MigrationException("Unknown role. role=" + migration.getRole());
        }
    }

}
