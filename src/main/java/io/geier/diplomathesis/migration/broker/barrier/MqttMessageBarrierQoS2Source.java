package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MqttMessageBarrierQoS2Source {

    private static final Logger LOG = LoggerFactory.getLogger(MqttMessageBarrierQoS2Source.class);

    public boolean canSendMessage(Migration.Subscription subscription, GlobalMessageId globalMessageId) {
        boolean canSendMessage;

        GlobalMessageId lastProcSource = subscription.getLastProcessedGlobalMessageId();
        GlobalMessageId lastProcTarget = subscription.getLastProcessedGlobalMessageIdByOpponent();

        LOG.debug("Message Barrier decision. globalMessageId={}, lastProcSource={}, lastProcTarget={}", globalMessageId, lastProcSource, lastProcTarget);

        if (lastProcSource != null && globalMessageId.isLessOrEqualTo(lastProcSource)) {
            return true;
        }

        switch (subscription.getState()) {
            case INITIALIZED:
                canSendMessage = false;
                break;
            case SYNCING:
                if (globalMessageId.isGreaterTo(lastProcSource) && globalMessageId.isLessOrEqualTo(lastProcTarget)) {
                    canSendMessage = true;

                    // with the last message, we are synced
                    if (globalMessageId.isEqualTo(lastProcTarget)) {
                        subscription.setState(MigrationSubscriptionState.SYNCED);
                    }
                } else {
                    // beyond because of a gap
                    canSendMessage = false;
                    subscription.setState(MigrationSubscriptionState.SYNCED);
                }
                break;
            case SYNCED:
            case FINISHED:
                canSendMessage = false;
                break;
            case ERROR:
                canSendMessage = true;
                break;
            default:
                throw new MigrationException("Unsupported state. state=" + subscription.getState());
        }

        return canSendMessage;
    }

}
