package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MqttMessageBarrierQoS2Target {

    private static final Logger LOG = LoggerFactory.getLogger(MqttMessageBarrierQoS2Target.class);

    public boolean canSendMessage(Migration.Subscription subscription, GlobalMessageId globalMessageId) {
        boolean canSendMessage;

        GlobalMessageId lastProcSource = subscription.getLastProcessedGlobalMessageIdByOpponent();
        GlobalMessageId lastProcTarget = subscription.getLastProcessedGlobalMessageId();

        LOG.debug("Message Barrier decision. globalMessageId={}, lastProcSource={}, lastProcTarget={}", globalMessageId, lastProcSource, lastProcTarget);

        if (lastProcTarget != null && globalMessageId.isLessOrEqualTo(lastProcTarget)) {
            return false;
        }

        switch (subscription.getState()) {
            case INITIALIZED:
                canSendMessage = false;
                break;
            case SYNCING:
            case SYNCING_THIS:
                if (globalMessageId.isLessOrEqualTo(lastProcSource)) {
                    // will skip all messaged that already have been send by Source Broker
                    canSendMessage = false;

                    // with the last message, we are synced
                    if (globalMessageId.isEqualTo(lastProcSource)) {
                        subscription.setThisSYNCED();
                    }
                } else {
                    canSendMessage = true;
                    subscription.setThisSYNCED();
                }
                break;
            case SYNCING_OPPONENT:
            case SYNCED:
            case FINISHED:
                canSendMessage = true;
                break;
            case ERROR:
                canSendMessage = false;
                break;
            default:
                throw new MigrationException("Unsupported state. state=" + subscription.getState());
        }

        return canSendMessage;
    }

}
