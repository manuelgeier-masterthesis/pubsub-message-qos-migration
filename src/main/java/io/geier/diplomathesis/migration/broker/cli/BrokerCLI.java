package io.geier.diplomathesis.migration.broker.cli;

import io.geier.diplomathesis.cli.AbstractCLI;
import io.geier.diplomathesis.migration.broker.BrokerMigrationHandler;
import io.geier.diplomathesis.migration.broker.MigrationHandlerMigrateStatus;
import io.geier.diplomathesis.migration.broker.MigrationStore;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.Subscription;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class BrokerCLI extends AbstractCLI {

    private static final Logger LOG = LoggerFactory.getLogger(BrokerCLI.class);

    public BrokerCLI(final int consolePort, BrokerMigrationHandler migrationHandler, MigrationStore migrationStore) {
        super(consolePort);

        addCommand("clients", new Command(
                "clients",
                "clients",
                commandInput -> {
                    Map<String, List<Subscription>> subscriptionsMap = migrationHandler.getSubscriptions();

                    if (subscriptionsMap.isEmpty()) {
                        commandInput.out.println("info: no clients found");
                    } else {
                        for (Map.Entry<String, List<Subscription>> entry : subscriptionsMap.entrySet()) {
                            String clientId = entry.getKey();
                            commandInput.out.println(clientId);

                            List<Subscription> subscriptions = entry.getValue();
                            for (Subscription subscription : subscriptions) {
                                commandInput.out.print("  ");
                                commandInput.out.println(subscription.toString());
                            }
                        }
                    }
                }));

        addCommand("subadd", new Command(
                "subadd ([\\S]+)( ([\\S]+)( ([012]))?)?",
                "subadd {clientId} [ {topic} [ {qos} ] ]",
                commandInput -> {
                    String clientId = commandInput.matcher.group(1);
                    String topic = commandInput.matcher.group(3);
                    String qos = commandInput.matcher.group(5);

                    if (topic == null) {
                        topic = "topic1";
                    }
                    MqttQoS qoS = MqttQoS.AT_MOST_ONCE;
                    if (qos != null) {
                        qoS = MqttQoS.valueOf(Integer.parseInt(qos));
                    }

                    commandInput.out.println("clientId " + clientId);
                    commandInput.out.println("topic    " + topic);
                    commandInput.out.println("qos      " + qoS);

                    migrationHandler.addSubscription(clientId, topic, qoS);
                }));

        addCommand("subrem", new Command(
                "subrem ([\\S]+)( ([\\S]+)?)?",
                "subrem {clientId} [ {topic:topic1}]",
                commandInput -> {
                    String clientId = commandInput.matcher.group(1);
                    String topic = commandInput.matcher.group(3);

                    if (topic == null) {
                        topic = "topic1";
                    }

                    commandInput.out.println("clientId " + clientId);
                    commandInput.out.println("topic    " + topic);

                    migrationHandler.removeSubscription(clientId, topic);
                }));

        addCommand("migrate", new Command(
                "migrate (\\S+) (\\d*) (\\S+) (\\S+) (\\d+)",
                "migrate {targetBrokerHost} {targetBrokerMessagePort} {clientId} {clientHost} {clientMigPort}",
                commandInput -> {
                    String targetBrokerHost = commandInput.matcher.group(1);
                    int targetBrokerMessagePort = Integer.parseInt(commandInput.matcher.group(2));
                    String clientId = commandInput.matcher.group(3);
                    String clientHost = commandInput.matcher.group(4);
                    int clientMigPort = Integer.parseInt(commandInput.matcher.group(5));

                    if (!migrationHandler.clientIdExists(clientId)) {
                        commandInput.out.println("error: client id does not exist");
                    } else {
                        MigrationHandlerMigrateStatus status = migrationHandler.migrate(targetBrokerHost, targetBrokerMessagePort, clientId, clientHost, clientMigPort);
                        switch (status) {
                            case STARTED:
                                commandInput.out.println("Migration started.");
                                break;
                            case ERROR_CLIENT_NOT_FOUND:
                                commandInput.out.println("Migration error. Client id not found.");
                                break;
                            case ERROR_CLIENT_ALREADY_MIGRATING:
                                commandInput.out.println("Migration error. Client is already migrating.");
                                break;
                            default:
                                commandInput.out.println("Unknown status. status=" + status);
                        }

                    }
                }));

        addCommand("migrations", new Command(
                "migrations",
                "migrations",
                commandInput -> {
                    Map<String, Migration> migrations = migrationStore.getMigrations();
                    if (migrations.isEmpty()) {
                        commandInput.out.println("No migrations.");
                    } else {
                        for (Map.Entry<String, Migration> migrationEntry : migrations.entrySet()) {
                            commandInput.out.println(migrationEntry.getValue().toPrettyString());
                        }
                    }
                }));

        addCommand("history", new Command(
                "history\\s*(.+)?",
                "history [ {clientId} ]",
                commandInput -> {
                    String clientId = commandInput.matcher.group(1);

                    List<Migration> migrations;
                    if (clientId != null) {
                        migrations = migrationStore.getMigrationsHistoryByClient(clientId);
                    } else {
                        migrations = migrationStore.getMigrationsHistory();
                    }

                    if (migrations.isEmpty()) {
                        commandInput.out.println("No history.");
                    } else {
                        for (Migration migration : migrations) {
                            Migration.Statistic statistic = migration.getStatistic();
                            commandInput.out.println(migration.getClient().getId() + ", " + migration.getMigrationId() + ", " + statistic.getStartTime() + ", " + statistic.getEndTime() + ", " + statistic.getDurationInMs());
                        }
                    }
                }));
    }
}
