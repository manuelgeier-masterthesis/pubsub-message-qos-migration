package io.geier.diplomathesis.migration.broker.domain;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.Metric;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.geier.diplomathesis.migration.communication.MigrationPacketClient;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Migration {

    private static final Logger LOG = LoggerFactory.getLogger(Migration.class);

    private String migrationId;
    private BrokerOpponent brokerOpponent;
    private final Metric metric;
    private Client client;
    private MigrationRole role;
    private List<Subscription> subscriptions = new ArrayList<>();
    private Map<String, Subscription> subscriptionsByTopicName = new HashMap<>();
    private MigrationState state;

    private Statistic statistic = new Statistic();
    private List<MigrationStateListener> stateListeners = new ArrayList<>();

    public Migration(String migrationId, String clientId, String clientHost, int clientPort, MigrationPacketClient opponentPacketClient, MigrationRole role) {
        this(migrationId, clientId, clientHost, clientPort, opponentPacketClient, role, null);
    }

    public Migration(String migrationId, String clientId, String clientHost, int clientPort, MigrationPacketClient opponentPacketClient, MigrationRole role, Metric metric) {
        this.migrationId = migrationId;
        this.brokerOpponent = new BrokerOpponent(opponentPacketClient);
        this.metric = metric;
        this.client = new Client(clientId, clientHost, clientPort);
        this.role = role;

        this.setState(MigrationState.INITIALIZED);
    }

    public String getMigrationId() {
        return migrationId;
    }

    public void setMigrationId(String migrationId) {
        this.migrationId = migrationId;
    }

    public BrokerOpponent getBrokerOpponent() {
        return brokerOpponent;
    }

    public void setBrokerOpponent(BrokerOpponent brokerOpponent) {
        this.brokerOpponent = brokerOpponent;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public MigrationRole getRole() {
        return role;
    }

    public void setRole(MigrationRole role) {
        this.role = role;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public Map<String, Subscription> getSubscriptionsByTopicName() {
        return subscriptionsByTopicName;
    }

    public MigrationState getState() {
        return state;
    }

    public void setState(MigrationState newState) {
        MigrationState oldState = this.state;

        if (oldState == newState) {
            LOG.warn("Migration state is already set. migrationId={}, oldState={}, newState={}", migrationId, oldState, newState);
            return;
        }

        this.state = newState;
        LOG.debug("Migration state changed. migrationId={}, oldState={}, newState={}", migrationId, oldState, newState);
        if (metric != null) {
            metric.addEvent(newState.name(), migrationId);
        }

        stateListeners.forEach((migrationStateListener -> migrationStateListener.stateChanged(this, oldState, newState)));
    }

    public List<MigrationStateListener> getStateListeners() {
        return stateListeners;
    }

    public Statistic getStatistic() {
        return statistic;
    }

    @Override
    public String toString() {
        return "Migration{" +
                "migrationId='" + migrationId + '\'' +
                ", clientId='" + client.id + '\'' +
                ", clientHost='" + client.host + '\'' +
                ", clientPort='" + client.port + '\'' +
                ", role=" + role +
                ", state=" + state +
                ", subscriptions=" + subscriptions +
                '}';
    }

    public String toPrettyString() {
        String s = "[\n";
        for (Subscription subscription : subscriptions) {
            s += subscription.toPrettyString() + ",\n";
        }
        s += "]";
        return "Migration{" + "\n" +
                "  migrationId='" + migrationId + '\'' + "\n" +
                "  clientId='" + client.id + '\'' + "\n" +
                "  clientHost='" + client.host + '\'' + "\n" +
                "  clientPort='" + client.port + '\'' + "\n" +
                "  role=" + role + "\n" +
                "  state=" + state + "\n" +
                "  subscriptions=" + s + "\n" +
                '}';
    }

    public Subscription getSubscriptionByTopicName(String topicName) {
        return subscriptionsByTopicName.get(topicName);
    }

    public Subscription addSubscription(String topic, MqttQoS requestedQos) {
        Subscription subscription = new Subscription(this, topic, requestedQos);
        subscriptions.add(subscription);
        subscriptionsByTopicName.put(subscription.topicName, subscription);

        // set migration finished if all subscriptions are finished
        subscription.addStateListener(this::subscriptionStateListener);

        return subscription;
    }

    private void subscriptionStateListener(Subscription subscription, MigrationSubscriptionState oldState, MigrationSubscriptionState newState) {
        updateState();
    }

    private void updateState() {
        boolean areAllSyncedOrFinished = true;
        boolean areAllFinished = true;
        for (Subscription subscription : subscriptions) {
            if (!(subscription.getState() == MigrationSubscriptionState.SYNCED
                    || subscription.getState() == MigrationSubscriptionState.FINISHED)) {
                areAllSyncedOrFinished = false;
            }
            if (!(subscription.getState() == MigrationSubscriptionState.FINISHED)) {
                areAllFinished = false;
            }
        }
        if (areAllSyncedOrFinished && this.state != MigrationState.FINISHED) {
            // should only work on on Target, since the Source sets it by itself
            if (getRole() == MigrationRole.TARGET) {
                setState(MigrationState.SYNCED);
            }
        }
        if (areAllFinished) {
            setState(MigrationState.FINISHED);
        }
    }

    public void addStateListener(MigrationStateListener migrationStateListener) {
        stateListeners.add(migrationStateListener);
    }

    public static class Subscription {
        private Migration migration;
        private String topicName;
        private MqttQoS qos;
        private GlobalMessageId lastProcessedGlobalMessageId; // will be set outside
        private GlobalMessageId lastProcessedGlobalMessageIdByOpponent; // will be set outside

        private GlobalMessageId firstSendMessageId = null;
        private Set<GlobalMessageId> messagesToByPass = new HashSet<>();

        private MigrationSubscriptionState state;

        private List<MigrationSubscriptionStateListener> stateListeners = new ArrayList<>();

        public Subscription(Migration migration, String topicName, MqttQoS qos) {
            this.migration = migration;
            this.topicName = topicName;
            this.qos = qos;

            this.setState(MigrationSubscriptionState.INITIALIZED);
        }

        public MigrationSubscriptionState getState() {
            return state;
        }

        public void setState(MigrationSubscriptionState state) {
            MigrationSubscriptionState oldState = this.state;
            MigrationSubscriptionState newState = state;

            if (oldState == newState) {
                LOG.warn("Migration Subscription state is already set. migrationId={}, topicName={}, qos={}, oldState={}, newState={}", migration.migrationId, topicName, qos, oldState, newState);
                return;
            }

            this.state = newState;
            LOG.debug("Migration Subscription state changed. migrationId={}, topicName={}, qos={}, oldState={}, newState={}", migration.migrationId, topicName, qos, oldState, newState);
            if (migration.metric != null) {
                migration.metric.addEvent(topicName + "-" + newState.name(), migration.migrationId);
            }

            stateListeners.forEach(stateListener -> stateListener.stateChanged(this, oldState, newState));
        }

        public void setThisSYNCED() {
            if (state == MigrationSubscriptionState.SYNCED) {
                LOG.error("Subscription already SYNCED. subscription={}", this);
            }
            if (state == MigrationSubscriptionState.SYNCING_OPPONENT) {
                LOG.error("Subscription already SYNCING_THIS. subscription={}", this);
            }

            if (state == MigrationSubscriptionState.SYNCING) {
                setState(MigrationSubscriptionState.SYNCING_OPPONENT);
            } else {
                if (state == MigrationSubscriptionState.SYNCING_THIS) {
                    setState(MigrationSubscriptionState.SYNCED);
                } else {
                    LOG.error("Subscription already SYNCING_THIS. subscription={}", this);
                }
            }
        }

        public void setOpponentSYNCED() {
            if (state == MigrationSubscriptionState.SYNCED) {
                LOG.error("Subscription already SYNCED. subscription={}", this);
            }
            if (state == MigrationSubscriptionState.SYNCING_THIS) {
                LOG.error("Subscription already SYNCING_OPPONENT. subscription={}", this);
            }

            if (state == MigrationSubscriptionState.SYNCING) {
                setState(MigrationSubscriptionState.SYNCING_THIS);
            } else {
                if (state == MigrationSubscriptionState.SYNCING_OPPONENT) {
                    setState(MigrationSubscriptionState.SYNCED);
                } else {
                    LOG.error("Subscription already SYNCING_OPPONENT. subscription={}", this);
                }
            }
        }

        public void addStateListener(MigrationSubscriptionStateListener stateListener) {
            stateListeners.add(stateListener);
        }

        public Migration getMigration() {
            return migration;
        }

        public void setMigration(Migration migration) {
            this.migration = migration;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }

        public MqttQoS getQos() {
            return qos;
        }

        public void setQos(MqttQoS qos) {
            this.qos = qos;
        }

        public GlobalMessageId getLastProcessedGlobalMessageId() {
            return lastProcessedGlobalMessageId;
        }

        public void setLastProcessedGlobalMessageId(GlobalMessageId lastProcessedGlobalMessageId) {
            this.lastProcessedGlobalMessageId = lastProcessedGlobalMessageId;
        }

        public GlobalMessageId getLastProcessedGlobalMessageIdByOpponent() {
            return lastProcessedGlobalMessageIdByOpponent;
        }

        public void setLastProcessedGlobalMessageIdByOpponent(GlobalMessageId lastProcessedGlobalMessageIdByOpponent) {
            this.lastProcessedGlobalMessageIdByOpponent = lastProcessedGlobalMessageIdByOpponent;
        }

        @Override
        public String toString() {
            return "Subscription{" +
                    "topicName='" + topicName + '\'' +
                    ", qos=" + qos +
                    ", state=" + state +
                    ", lastProcessedGlobalMessageId=" + lastProcessedGlobalMessageId +
                    ", lastProcessedGlobalMessageIdByOpponent=" + lastProcessedGlobalMessageIdByOpponent +
                    ", firstSendMessageId=" + firstSendMessageId +
                    '}';
        }

        public String toPrettyString() {
            return "Subscription{" + "\n" +
                    "  topicName='" + topicName + '\'' + "\n" +
                    "  qos=" + qos + "\n" +
                    "  state=" + state + "\n" +
                    "  lastProcessedGlobalMessageId=" + lastProcessedGlobalMessageId + "\n" +
                    "  lastProcessedGlobalMessageIdByOpponent=" + lastProcessedGlobalMessageIdByOpponent + "\n" +
                    "  firstSendMessageId=" + firstSendMessageId + "\n" +
                    '}';
        }

        public boolean hasFirstSendMessageId() {
            return firstSendMessageId != null;
        }

        public GlobalMessageId getFirstSendMessageId() {
            return firstSendMessageId;
        }

        public void setFirstSendMessageId(GlobalMessageId firstSendMessageId) {
            this.firstSendMessageId = firstSendMessageId;
        }

        public boolean canMessageByPass(GlobalMessageId globalMessageId) {
            return messagesToByPass.contains(globalMessageId);
        }

        public void addMessageToByPass(GlobalMessageId globalMessageId) {
            messagesToByPass.add(globalMessageId);
        }

        public Set<GlobalMessageId> getMessagesToByPass() {
            return messagesToByPass;
        }

        public void clearMessageByPass() {
            messagesToByPass.clear();
        }

        public boolean shouldStore() {
            switch (migration.getRole()) {
                case SOURCE:
                    return qos == MqttQoS.AT_LEAST_ONCE || qos == MqttQoS.EXACTLY_ONCE;
                case TARGET:
                    return false;
                default:
                    throw new MigrationException("Unknown role. role=" + migration.getRole());
            }
        }
    }

    public static class BrokerOpponent {
        /**
         * messageClient of our opponent (source or target), depending on our role
         */
        private MigrationPacketClient messageClient;

        public BrokerOpponent(MigrationPacketClient messageClient) {
            this.messageClient = messageClient;
        }

        public MigrationPacketClient getMessageClient() {
            return messageClient;
        }
    }

    public static class Client {
        private String id;
        private String host;
        private int port;

        /**
         * Channel to the client that will be migrated
         */
        private MigrationPacketClient packetChannel;

        public Client(String id, String host, int port) {
            this.id = id;
            this.host = host;
            this.port = port;
        }

        public String getId() {
            return id;
        }

        public String getHost() {
            return host;
        }

        public int getPort() {
            return port;
        }

        public MigrationPacketClient getPacketChannel() {
            return packetChannel;
        }

        public void setPacketChannel(MigrationPacketClient packetChannel) {
            this.packetChannel = packetChannel;
        }
    }

    public static class Statistic {

        private Long startTime;
        private Long endTime;
        private Long durationInMs;

        public Statistic() {
            startTime();
        }

        private void startTime() {
            startTime(System.currentTimeMillis());
        }

        private void startTime(Long startTime) {
            this.startTime = startTime;
        }

        public void stopTime() {
            this.endTime = System.currentTimeMillis();
            this.durationInMs = this.endTime - this.startTime;
        }

        public Long getStartTime() {
            return startTime;
        }

        public Long getEndTime() {
            return endTime;
        }

        public Long getDurationInMs() {
            return durationInMs;
        }

        @Override
        public String toString() {
            return "Statistic{" +
                    "startTime=" + startTime +
                    ", endTime=" + endTime +
                    ", durationInMs=" + durationInMs +
                    '}';
        }
    }
}
