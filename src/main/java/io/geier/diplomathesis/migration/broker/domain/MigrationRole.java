package io.geier.diplomathesis.migration.broker.domain;

public enum MigrationRole {

    /**
     * This broker will transfer the client
     */
    SOURCE,

    /**
     * This broker will receive the client
     */
    TARGET;

}
