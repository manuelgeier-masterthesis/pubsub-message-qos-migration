package io.geier.diplomathesis.migration.broker.domain;

public enum MigrationState {
    INITIALIZED, SYNCING, SYNCED, FINISHED, ERROR;
}
