package io.geier.diplomathesis.migration.broker.domain;

public interface MigrationStateListener {

    void stateChanged(Migration migration, MigrationState oldState, MigrationState newState);

}
