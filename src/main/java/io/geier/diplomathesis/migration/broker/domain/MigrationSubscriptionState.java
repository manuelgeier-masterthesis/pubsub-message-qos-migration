package io.geier.diplomathesis.migration.broker.domain;

public enum MigrationSubscriptionState {
    INITIALIZED, SYNCING, SYNCING_THIS, SYNCING_OPPONENT, SYNCED, FINISHED, ERROR;
}
