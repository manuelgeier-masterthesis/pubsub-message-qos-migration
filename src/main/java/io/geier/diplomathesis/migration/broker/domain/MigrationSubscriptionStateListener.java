package io.geier.diplomathesis.migration.broker.domain;

public interface MigrationSubscriptionStateListener {

    void stateChanged(Migration.Subscription subscription, MigrationSubscriptionState oldState, MigrationSubscriptionState newState);

}
