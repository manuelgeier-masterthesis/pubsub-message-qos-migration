package io.geier.diplomathesis.migration.broker.domain;

import java.util.ArrayList;
import java.util.List;

public class MigrationSubscriptionStateListenerCallback {

    private final Check check;
    private final Callback callback;
    private MigrationSubscriptionStateListener stateListener;
    private List<Migration.Subscription> subscriptions = new ArrayList<>();

    public MigrationSubscriptionStateListenerCallback(Check check, Callback callback) {
        this.check = check;
        this.callback = callback;
        stateListener = (subscription, oldState, newState) -> verify();
    }

    public void verify() {
        boolean isValid = true;
        for (Migration.Subscription subscription1 : subscriptions) {
            isValid &= check.isValidState(subscription1);
        }
        if (isValid) {
            callback.call();
        }
    }

    public void add(Migration.Subscription subscription) {
        subscriptions.add(subscription);
        subscription.addStateListener(stateListener);
    }

    public void addAllAndVerify(List<Migration.Subscription> subscriptions) {
        for (Migration.Subscription subscription : subscriptions) {
            add(subscription);
        }
        verify();
    }

    public int size() {
        return subscriptions.size();
    }

    public interface Check {
        boolean isValidState(Migration.Subscription subscriptions);
    }

    public interface Callback {
        void call();
    }
}
