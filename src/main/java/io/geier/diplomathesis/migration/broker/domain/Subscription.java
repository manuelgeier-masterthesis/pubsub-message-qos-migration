package io.geier.diplomathesis.migration.broker.domain;

import io.netty.handler.codec.mqtt.MqttQoS;

public final class Subscription {

    private final String clientId;
    private final String topicName;
    private final MqttQoS requestedQos;

    public Subscription(String clientId, String topicName, MqttQoS requestedQos) {
        this.clientId = clientId;
        this.topicName = topicName;
        this.requestedQos = requestedQos;
    }

    public String getClientId() {
        return clientId;
    }

    public String getTopicName() {
        return topicName;
    }

    public MqttQoS getRequestedQos() {
        return requestedQos;
    }
}
