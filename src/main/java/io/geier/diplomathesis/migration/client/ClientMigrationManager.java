package io.geier.diplomathesis.migration.client;

import io.geier.diplomathesis.migration.communication.MigrationPacketHandler;
import io.geier.diplomathesis.migration.communication.MigrationPacketServer;
import io.geier.diplomathesis.migration.communication.packet.MigToAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigToPacket;
import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class ClientMigrationManager implements MigrationPacketHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ClientMigrationManager.class);

    private final MigrationPacketServer messageServer;
    private final MigrationClientBridge clientBridge;

    public ClientMigrationManager(final int messageServerPort, MigrationClientBridge clientBridge) {
        this.clientBridge = clientBridge;
        this.messageServer = new MigrationPacketServer(messageServerPort, this);
        this.messageServer.start();
    }

    public void close() {
        this.messageServer.stop();
    }

    @Override
    public void handle(Channel channel, MigrationPacket packet) {
        if (packet instanceof MigToPacket) {
            handleMigToMeRequest(channel, (MigToPacket) packet);
        } else {
            LOG.error("Unknown packet, packet={}", packet);
        }
    }

    private void handleMigToMeRequest(Channel channel, MigToPacket packet) {
        Future future = clientBridge.connect(packet.getHost(), packet.getPort());
        new Thread(() -> {
            try {
                future.get();
                sendMigrateToMeAckMessage(channel, packet.getMigrationId());
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
                LOG.error("Could not connect to host. migrationId={}, host={}, port={}, packet={}", packet.getMigrationId(), packet.getHost(), packet.getPort(), e.getMessage());
            }
        }).start();
    }

    private void sendMigrateToMeAckMessage(Channel channel, String migrationId) {
        MigToAckPacket packet = new MigToAckPacket(migrationId);
        channel.writeAndFlush(packet).addListener(future -> {
            if (future.isSuccess()) {
                LOG.info("Sent MIGTOACK. migrationId={}", migrationId);
            } else {
                LOG.error("Error sending MIGTOACK. migrationId={}, packet={}", migrationId, future.cause().getMessage());
            }
        });
    }

}
