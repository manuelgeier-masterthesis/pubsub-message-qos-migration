package io.geier.diplomathesis.migration.client;

import io.geier.diplomathesis.migration.client.cli.domain.BrokerInfo;

import java.util.List;
import java.util.concurrent.Future;

public interface MigrationClientBridge {

    Future connect(String host, int port);

    List<BrokerInfo> getBrokers();
}
