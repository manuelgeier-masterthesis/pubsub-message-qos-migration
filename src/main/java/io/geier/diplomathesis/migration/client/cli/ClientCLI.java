package io.geier.diplomathesis.migration.client.cli;

import io.geier.diplomathesis.cli.AbstractCLI;
import io.geier.diplomathesis.migration.client.MigrationClientBridge;
import io.geier.diplomathesis.migration.client.cli.domain.BrokerInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ClientCLI extends AbstractCLI {

    private static final Logger LOG = LoggerFactory.getLogger(ClientCLI.class);

    public ClientCLI(final int cliPort, MigrationClientBridge migrationClientBridge) {
        super(cliPort);

        addCommand("brokers", new AbstractCLI.Command(
                "brokers$",
                "brokers",
                commandInput -> {
                    List<BrokerInfo> brokers = migrationClientBridge.getBrokers();
                    for (int i = 0; i < brokers.size(); i++) {
                        BrokerInfo broker = brokers.get(i);
                        commandInput.out.println("host: " + broker.getHost() + ", port: " + broker.getPort());
                    }
                }));
    }
}
