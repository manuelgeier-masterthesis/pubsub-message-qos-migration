package io.geier.diplomathesis.migration.client.cli.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BrokerInfo {

    private static final Logger LOG = LoggerFactory.getLogger(BrokerInfo.class);

    private String host;
    private int port;

    public BrokerInfo(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "BrokerInfo{" +
                "host='" + host + '\'' +
                ", port=" + port +
                '}';
    }
}
