package io.geier.diplomathesis.migration.communication;

import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;

public class ChannelMigrationPacketClient implements MigrationPacketClient {

    private static final Logger LOG = LoggerFactory.getLogger(ChannelMigrationPacketClient.class);

    private SocketAddress socketAddress;
    private Channel channel;

    public ChannelMigrationPacketClient(Channel channel) {
        this.socketAddress = channel.remoteAddress();
        this.channel = channel;
        this.channel.closeFuture().addListener((future -> {
            LOG.info("Close packetChannel. success={}, socketAddress={}", future.isSuccess(), socketAddress);
        }));
    }

    @Override
    public void startSync() {
        // nothing to do; we use already an open channel
    }

    public void sendSync(MigrationPacket packet) {
        try {
            send(packet).sync();
        } catch (InterruptedException e) {
            LOG.error("Connection error", e);
        }
    }

    public ChannelFuture send(MigrationPacket packet) {
        LOG.info("Send migration packet. packet={}", packet);
        return channel.writeAndFlush(packet);
    }

    public ChannelFuture close() {
        return channel.close();
    }

    public Channel getChannel() {
        return channel;
    }

}
