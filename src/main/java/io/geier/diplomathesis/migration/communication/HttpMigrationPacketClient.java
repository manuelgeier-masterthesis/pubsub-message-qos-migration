package io.geier.diplomathesis.migration.communication;

import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpMigrationPacketClient implements MigrationPacketClient {

    private static final Logger LOG = LoggerFactory.getLogger(HttpMigrationPacketClient.class);

    private final String host;
    private final int serverTargetPort;
    private final MigrationPacketHandler requestHandler;

    private NioEventLoopGroup group;
    private ChannelMigrationPacketClient channelMigrationPacketClient;

    public HttpMigrationPacketClient(String host, int serverTargetPort, MigrationPacketHandler requestHandler) {
        this.host = host;
        this.serverTargetPort = serverTargetPort;
        this.requestHandler = requestHandler;
    }

    @Override
    public void startSync() {
        try {
            group = new NioEventLoopGroup();
            Bootstrap bootstrap = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new MigrationPacketSocketChannelChannelInitializer(requestHandler));

            // this is blocking
            Channel channel = bootstrap.connect(host, serverTargetPort).sync().channel();
            LOG.info("Connected, host={}, port={}", host, serverTargetPort);

            this.channelMigrationPacketClient = new ChannelMigrationPacketClient(channel);

            // close listener
            channel.closeFuture().addListener((future -> close()));
        } catch (InterruptedException e) {
            LOG.error("Connection error", e);
        }
    }

    public void sendSync(MigrationPacket packet) {
        this.channelMigrationPacketClient.sendSync(packet);
    }

    public ChannelFuture send(MigrationPacket packet) {
        return this.channelMigrationPacketClient.send(packet);
    }

    public Channel getChannel() {
        return this.channelMigrationPacketClient.getChannel();
    }

    public ChannelFuture close() {
        LOG.info("Close connection, host={}, port={}", host, serverTargetPort);
        this.channelMigrationPacketClient.close(); // TODO maybe this leads to an exception, since we call this method on messageChannel close
        group.shutdownGracefully();
        return null;
    }

}
