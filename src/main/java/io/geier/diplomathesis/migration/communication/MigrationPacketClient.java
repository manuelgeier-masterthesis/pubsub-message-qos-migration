package io.geier.diplomathesis.migration.communication;

import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.channel.ChannelFuture;

public interface MigrationPacketClient {

    void startSync();

    void sendSync(MigrationPacket packet);

    ChannelFuture send(MigrationPacket packet);

    ChannelFuture close();
}
