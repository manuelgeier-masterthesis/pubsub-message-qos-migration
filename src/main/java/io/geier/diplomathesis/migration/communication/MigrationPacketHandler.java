package io.geier.diplomathesis.migration.communication;

import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.channel.Channel;

public interface MigrationPacketHandler {

    void handle(Channel channel, MigrationPacket packet);

}
