package io.geier.diplomathesis.migration.communication;

import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class MigrationPacketInboundHandler extends SimpleChannelInboundHandler<MigrationPacket> {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationPacketSocketChannelChannelInitializer.class);

    private final MigrationPacketHandler requestHandler;

    public MigrationPacketInboundHandler(MigrationPacketHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    protected void channelRead0(ChannelHandlerContext ctx, MigrationPacket msg) {
        try {
            LOG.info("Migration packet received. packet={}", msg);
            requestHandler.handle(ctx.channel(), msg);
        } catch (MigrationException e) {
            LOG.error("MigrationException occurred.", e);
            throw e;
        }
    }
}
