package io.geier.diplomathesis.migration.communication;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * We want to migration all subscriptions from another broker to us
 * <p>
 * TODO after migration is done, kill/disconnect/cleanup client (session)
 * TODO kill thread somehow
 * TODO server currently only processes one request per client connection
 */
public class MigrationPacketServer {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationPacketServer.class);

    private final int serverPort;
    private final MigrationPacketHandler migrationPacketHandler;

    private NioEventLoopGroup bossEventGroup;
    private NioEventLoopGroup workerEventGroup;

    public MigrationPacketServer(final int serverPort, MigrationPacketHandler migrationPacketHandler) {
        this.serverPort = serverPort;
        this.migrationPacketHandler = migrationPacketHandler;
    }

    public synchronized void start() {
        try {
            bossEventGroup = new NioEventLoopGroup();
            workerEventGroup = new NioEventLoopGroup();
            final ServerBootstrap bootstrap = new ServerBootstrap()
                .group(bossEventGroup, workerEventGroup)
                .channel(NioServerSocketChannel.class)
                    .childHandler(new MigrationPacketSocketChannelChannelInitializer(migrationPacketHandler));

            bootstrap.bind(serverPort).sync();
            LOG.info("Migration Message Server started, port={}", serverPort);
        } catch (InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public synchronized void stop() {
        LOG.info("Migration Message Server shutdown. port={}", serverPort);
        bossEventGroup.shutdownGracefully();
        workerEventGroup.shutdownGracefully();
    }

}
