package io.geier.diplomathesis.migration.communication;

import io.netty.channel.Channel;

public class MigrationPacketService {

    public MigrationPacketClient createPacketClient(final String targetBrokerHost, final int targetBrokerMessagePort, MigrationPacketHandler migrationPacketHandler) {
        return new HttpMigrationPacketClient(targetBrokerHost, targetBrokerMessagePort, migrationPacketHandler);
    }

    public MigrationPacketClient createPacketClient(Channel channel) {
        return new ChannelMigrationPacketClient(channel);
    }
}
