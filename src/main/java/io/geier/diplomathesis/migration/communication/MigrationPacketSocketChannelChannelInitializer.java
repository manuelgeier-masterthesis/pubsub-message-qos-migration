package io.geier.diplomathesis.migration.communication;

import io.geier.diplomathesis.migration.communication.coder.MigrationPacketToJsonEncoder;
import io.geier.diplomathesis.migration.communication.coder.JsonToMigrationPacketDecoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.json.JsonObjectDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class MigrationPacketSocketChannelChannelInitializer extends ChannelInitializer<SocketChannel> {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationPacketSocketChannelChannelInitializer.class);

    private final MigrationPacketHandler requestHandler;

    public MigrationPacketSocketChannelChannelInitializer(MigrationPacketHandler requestHandler) {
        this.requestHandler = requestHandler;
    }

    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();

        // in (evaluation: down to top)
        pipeline.addLast(new StringEncoder());
        pipeline.addLast(new MigrationPacketToJsonEncoder());

        // out (evaluation: top to down)
        pipeline.addLast(new JsonObjectDecoder());
        pipeline.addLast(new StringDecoder());
        pipeline.addLast(new JsonToMigrationPacketDecoder());
        pipeline.addLast(new MigrationPacketInboundHandler(requestHandler));
    }

}
