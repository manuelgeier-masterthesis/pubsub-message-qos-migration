package io.geier.diplomathesis.migration.communication.coder;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class JsonToMigrationPacketDecoder extends MessageToMessageDecoder<String> {

    private static final Logger LOG = LoggerFactory.getLogger(JsonToMigrationPacketDecoder.class);

    private static ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, String jsonPacket, List<Object> out) throws Exception {
        LOG.trace("decode packet, json={}", jsonPacket);
        MigrationPacket packet = mapper.readValue(jsonPacket, MigrationPacket.class);
        LOG.debug("decode packet, packet={}", packet);
        out.add(packet);
    }
}
