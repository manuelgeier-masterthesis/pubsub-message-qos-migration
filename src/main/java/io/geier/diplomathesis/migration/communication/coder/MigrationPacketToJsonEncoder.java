package io.geier.diplomathesis.migration.communication.coder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class MigrationPacketToJsonEncoder extends MessageToMessageEncoder<MigrationPacket> {

    private static final Logger LOG = LoggerFactory.getLogger(MigrationPacketToJsonEncoder.class);

    private static ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, MigrationPacket packet, List<Object> out) throws Exception {
        try {
            LOG.trace("encode packet, packet={}", packet);
            String jsonPacket = mapper.writeValueAsString(packet);
            LOG.debug("encode packet, json={}", jsonPacket);
            out.add(jsonPacket);
        } catch (JsonProcessingException e) {
            LOG.error("encode packet, json processing error", e);
            throw e;
        }
    }
}
