package io.geier.diplomathesis.migration.communication.packet;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.messaging.MqttPayload;
import io.netty.handler.codec.mqtt.MqttPublishMessage;

public class MessageInspector {

    public static GlobalMessageId getGlobalMessageId(MqttPublishMessage publishMessage) {
        // this might be a heavy method
        return MqttPayload.fromByteBuf(publishMessage.payload()).getHeader().getGlobalMessageId();
    }
}
