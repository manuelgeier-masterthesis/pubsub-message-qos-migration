package io.geier.diplomathesis.migration.communication.packet;

public class MigAckPacket extends MigrationPacket {

    private String migrationId;
    private MigStatus statusCode;

    public MigAckPacket() {
    }

    public MigAckPacket(String migrationId, MigStatus statusCode) {
        this.migrationId = migrationId;
        this.statusCode = statusCode;
    }

    public String getMigrationId() {
        return migrationId;
    }

    public MigStatus getStatusCode() {
        return statusCode;
    }

    @Override
    public String toString() {
        return "MigAckPacket{" +
                "migrationId='" + migrationId + '\'' +
                ", statusCode=" + statusCode +
                '}';
    }

    public enum MigStatus {
        OK(0),
        ERROR(1);

        private int statusCode;

        MigStatus(int statusCode) {
            this.statusCode = statusCode;
        }

        @Override
        public String toString() {
            return "" + statusCode;
        }
    }
}
