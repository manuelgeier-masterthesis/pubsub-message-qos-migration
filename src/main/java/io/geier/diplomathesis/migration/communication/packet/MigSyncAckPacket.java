package io.geier.diplomathesis.migration.communication.packet;

public class MigSyncAckPacket extends MigrationPacket {

    private String migrationId;

    public MigSyncAckPacket() {
    }

    public MigSyncAckPacket(String migrationId) {
        this.migrationId = migrationId;
    }

    public String getMigrationId() {
        return migrationId;
    }

    @Override
    public String toString() {
        return "MigSyncAckPacket{" +
            "migrationId='" + migrationId + '\'' +
            '}';
    }
}
