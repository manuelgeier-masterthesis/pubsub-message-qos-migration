package io.geier.diplomathesis.migration.communication.packet;

import io.geier.diplomathesis.messaging.GlobalMessageId;

import java.util.List;

public class MigSyncPacket extends MigrationPacket {

    private String migrationId;
    private List<MigSyncSubscription> subscriptions;

    public MigSyncPacket() {
    }

    public MigSyncPacket(String migrationId, List<MigSyncSubscription> subscriptions) {
        this.migrationId = migrationId;
        this.subscriptions = subscriptions;
    }

    public String getMigrationId() {
        return migrationId;
    }

    public void setMigrationId(String migrationId) {
        this.migrationId = migrationId;
    }

    public List<MigSyncSubscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<MigSyncSubscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    @Override
    public String toString() {
        return "MigSyncPacket{" +
            "migrationId='" + migrationId + '\'' +
            ", subscriptions=" + subscriptions +
            '}';
    }

    public static class MigSyncSubscription {

        private String topicName;
        private GlobalMessageId lastProcessedGlobalMessageId;

        public MigSyncSubscription() {
        }

        public MigSyncSubscription(String topicName, GlobalMessageId lastProcessedGlobalMessageId) {
            this.topicName = topicName;
            this.lastProcessedGlobalMessageId = lastProcessedGlobalMessageId;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }

        public GlobalMessageId getLastProcessedGlobalMessageId() {
            return lastProcessedGlobalMessageId;
        }

        public void setLastProcessedGlobalMessageId(GlobalMessageId lastProcessedGlobalMessageId) {
            this.lastProcessedGlobalMessageId = lastProcessedGlobalMessageId;
        }

        @Override
        public String toString() {
            return "Subscription{" +
                "topicName='" + topicName + '\'' +
                ", lastProcessedGlobalMessageId=" + lastProcessedGlobalMessageId +
                '}';
        }
    }
}
