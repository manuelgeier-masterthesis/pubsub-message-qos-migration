package io.geier.diplomathesis.migration.communication.packet;

public class MigToAckPacket extends MigrationPacket {

    private String migrationId;

    public MigToAckPacket() {
    }

    public MigToAckPacket(String migrationId) {
        this.migrationId = migrationId;
    }

    public String getMigrationId() {
        return migrationId;
    }

    @Override
    public String toString() {
        return "MigToAckPacket{" +
            "migrationId='" + migrationId + '\'' +
            '}';
    }
}
