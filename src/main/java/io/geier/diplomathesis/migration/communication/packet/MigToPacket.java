package io.geier.diplomathesis.migration.communication.packet;

public class MigToPacket extends MigrationPacket {

    private String migrationId;
    private String host;
    private int port;

    public MigToPacket() {
    }

    public MigToPacket(String migrationId, String host, int port) {
        this.migrationId = migrationId;
        this.host = host;
        this.port = port;
    }

    public String getMigrationId() {
        return migrationId;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "MigToPacket{" +
            "migrationId='" + migrationId + '\'' +
            "host='" + host + '\'' +
            "port='" + port + '\'' +
            '}';
    }
}
