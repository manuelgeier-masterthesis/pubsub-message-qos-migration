package io.geier.diplomathesis.migration.communication.packet;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.netty.handler.codec.mqtt.MqttQoS;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MigratePacket extends MigrationPacket {

    private String migrationId;
    private String clientId;
    private String clientHost;
    private int clientPort;
    private List<MigrateSubscription> subscriptions = new ArrayList<>();

    public MigratePacket() {
    }

    public MigratePacket(String migrationId, String clientId, String clientHost, int clientPort, List<MigrateSubscription> subscriptions) {
        this.migrationId = migrationId;
        this.clientId = clientId;
        this.clientHost = clientHost;
        this.clientPort = clientPort;
        this.subscriptions = subscriptions;
    }

    public String getMigrationId() {
        return migrationId;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientHost() {
        return clientHost;
    }

    public int getClientPort() {
        return clientPort;
    }

    public List<MigrateSubscription> getSubscriptions() {
        return subscriptions;
    }

    @Override
    public String toString() {
        return "MigPacket{" +
                "migrationId='" + migrationId + '\'' +
                "clientId='" + clientId + '\'' +
                "clientHost='" + clientHost + '\'' +
                "clientPort='" + clientPort + '\'' +
                ", subscriptions=" + subscriptions +
                '}';
    }

    public static class MigrateSubscription {

        private MqttQoS requestedQos;
        private String topicName;
        private GlobalMessageId lastProcessedGlobalMessageId;

        public MigrateSubscription() {
        }

        public MigrateSubscription(MqttQoS requestedQos, String topicName, GlobalMessageId lastProcessedGlobalMessageId) {
            this.requestedQos = requestedQos;
            this.topicName = topicName;
            this.lastProcessedGlobalMessageId = lastProcessedGlobalMessageId;
        }

        public MqttQoS getRequestedQos() {
            return requestedQos;
        }

        public void setRequestedQos(MqttQoS requestedQos) {
            this.requestedQos = requestedQos;
        }

        public String getTopicName() {
            return topicName;
        }

        public void setTopicName(String topicName) {
            this.topicName = topicName;
        }

        public GlobalMessageId getLastProcessedGlobalMessageId() {
            return lastProcessedGlobalMessageId;
        }

        public void setLastProcessedGlobalMessageId(GlobalMessageId lastProcessedGlobalMessageId) {
            this.lastProcessedGlobalMessageId = lastProcessedGlobalMessageId;
        }

        @Override
        public String toString() {
            return "MigrationSubscription{" +
                    "requestedQos=" + requestedQos +
                    ", topicName=" + topicName +
                    ", lastProcessedGlobalMessageId=" + lastProcessedGlobalMessageId +
                    '}';
        }
    }
}






