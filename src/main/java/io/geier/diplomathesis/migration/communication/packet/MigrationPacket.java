package io.geier.diplomathesis.migration.communication.packet;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public abstract class MigrationPacket {

    public MigrationPacket() {
    }

    @Override
    public String toString() {
        return "MigrationPacket{}";
    }
}
