package io.geier.diplomathesis.migration;

import org.apache.log4j.BasicConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

@RunWith(MockitoJUnitRunner.Silent.class)
public abstract class BaseTest {

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule()
//            .strictness(Strictness.STRICT_STUBS)
//            .strictness(Strictness.LENIENT)
            .silent();

    @BeforeClass
    public static void beforeClass() {
        // log4j setup
        BasicConfigurator.configure();
    }

    @AfterClass
    public static void afterClass() {
        // log4j reset (otherwise appenders would be added multiple times)
        BasicConfigurator.resetConfiguration();
    }
}
