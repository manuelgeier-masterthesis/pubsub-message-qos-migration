package io.geier.diplomathesis.migration;

import io.geier.diplomathesis.migration.broker.BrokerMigrationHandler;
import io.geier.diplomathesis.migration.communication.MigrationPacketClient;
import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MigrationMessageClientConnector {

    public final Channel sourceToTargetChannel;
    public final Channel targetToSourceChannel;
    public MigrationPacketClient sourceToTargetClient;
    public MigrationPacketClient targetToSourceClient;

    public MigrationMessageClientConnector(BrokerMigrationHandler migrationHandlerSource,
                                           BrokerMigrationHandler migrationHandlerTarget) {

        targetToSourceChannel = mock(Channel.class);
        when(targetToSourceChannel.writeAndFlush(any())).then(new Answer<ChannelFuture>() {
            @Override
            public ChannelFuture answer(InvocationOnMock invocation) {
                MigrationPacket message = invocation.getArgument(0);
                migrationHandlerSource.handle(sourceToTargetChannel, message);
                return null;
            }
        });

        sourceToTargetChannel = mock(Channel.class);
        when(sourceToTargetChannel.writeAndFlush(any())).then(new Answer<ChannelFuture>() {
            @Override
            public ChannelFuture answer(InvocationOnMock invocation) {
                MigrationPacket message = invocation.getArgument(0);
                migrationHandlerTarget.handle(targetToSourceChannel, message);
                return null;
            }
        });


        sourceToTargetClient = mock(MigrationPacketClient.class);
        doAnswer(new Answer<ChannelFuture>() {
            @Override
            public ChannelFuture answer(InvocationOnMock invocation) {
                MigrationPacket message = invocation.getArgument(0);
                migrationHandlerSource.handle(sourceToTargetChannel, message);
                return null;
            }
        }).when(sourceToTargetClient).sendSync(any());

        targetToSourceClient = mock(MigrationPacketClient.class);
        doAnswer(new Answer<ChannelFuture>() {
            @Override
            public ChannelFuture answer(InvocationOnMock invocation) {
                MigrationPacket message = invocation.getArgument(0);
                migrationHandlerTarget.handle(targetToSourceChannel, message);
                return null;
            }
        }).when(sourceToTargetClient).sendSync(any());
    }

}
