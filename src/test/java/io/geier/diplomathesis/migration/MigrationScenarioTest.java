package io.geier.diplomathesis.migration;

import io.geier.diplomathesis.migration.broker.BrokerMigrationBridge;
import io.geier.diplomathesis.migration.broker.BrokerMigrationHandler;
import io.geier.diplomathesis.migration.broker.MigrationHandlerMigrateStatus;
import io.geier.diplomathesis.migration.broker.MigrationMessageStore;
import io.geier.diplomathesis.migration.broker.MigrationStore;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.communication.HttpMigrationPacketClient;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MigrationScenarioTest {

    private static final String BROKER_SOURCE_HOST = "10.0.0.1";
    private static final int BROKER_SOURCE_PORT = 1234;
    private static final String BROKER_TARGET_HOST = "10.0.0.2";
    private static final int BROKER_TARGET_PORT = 1234;
    private static final String CLIENT_A_HOST = "10.0.1.1";
    private static final int CLIENT_A_PORT = 1234;
    private static final String CLIENT_A_ID = "Client A";
    private static final String CLIENT_B_HOST = "10.0.1.2";
    private static final int CLIENT_B_PORT = 1234;
    private static final String CLIENT_B_ID = "Client B";
    private static final String TOPIC_1 = "Topic 1";
    private static final String TOPIC_2 = "Topic 2";
    private static final String TOPIC_3 = "Topic 3";

    private BrokerMigrationHandler migrationHandlerSource;
    private BrokerMigrationHandler migrationHandlerTarget;
    private MigrationPacketService messageServiceSource;
    private BrokerMigrationBridge migrationBridgeSource;
    private MigrationPacketService messageServiceTarget;
    private HttpMigrationPacketClient messageClientTarget;
    private BrokerMigrationBridge migrationBridgeTarget;
    private MigrationMessageClientConnector connectorToTarget;
    private MigrationMessageClientConnector connectorToClient;

    @Before
    public void setUp() {
        migrationBridgeSource = mock(BrokerMigrationBridge.class);
        when(migrationBridgeSource.clientIdExists(CLIENT_A_ID)).thenReturn(true);
        when(migrationBridgeSource.clientIdExists(CLIENT_B_ID)).thenReturn(true);
        messageServiceSource = mock(MigrationPacketService.class);
        migrationHandlerSource = Mockito.spy(new BrokerMigrationHandler(
                BROKER_SOURCE_HOST,
                BROKER_SOURCE_PORT,
                mock(MqttMessageBarrier.class),
                new MigrationStore(),
                new MigrationMessageStore(),
                migrationBridgeSource,
                messageServiceSource
        ));

        migrationBridgeTarget = mock(BrokerMigrationBridge.class);
        messageServiceTarget = mock(MigrationPacketService.class);
        migrationHandlerTarget = Mockito.spy(new BrokerMigrationHandler(
                BROKER_TARGET_HOST,
                BROKER_TARGET_PORT,
                mock(MqttMessageBarrier.class),
                new MigrationStore(),
                new MigrationMessageStore(),
                migrationBridgeTarget,
                messageServiceTarget
        ));

        connectorToTarget = new MigrationMessageClientConnector(migrationHandlerSource, migrationHandlerTarget);
        connectorToTarget = spy(connectorToTarget);
        when(messageServiceSource.createPacketClient(eq(BROKER_TARGET_HOST), anyInt(), any())).thenReturn(connectorToTarget.sourceToTargetClient);
        when(messageServiceSource.createPacketClient(eq(connectorToTarget.targetToSourceChannel))).thenReturn(connectorToTarget.targetToSourceClient);

        connectorToClient = new MigrationMessageClientConnector(migrationHandlerSource, migrationHandlerTarget);
        connectorToClient = spy(connectorToClient);
        when(messageServiceTarget.createPacketClient(eq(CLIENT_A_HOST), anyInt(), any())).thenReturn(connectorToClient.targetToSourceClient);
        when(messageServiceTarget.createPacketClient(eq(connectorToClient.targetToSourceChannel))).thenReturn(connectorToClient.targetToSourceClient);
    }

    @Test
    @Ignore("not finished yet")
    public void migrate_withNoSubscriptions() {
        when(migrationBridgeSource.getSubscriptionsByClientId(CLIENT_A_ID)).thenReturn(new ArrayList<>());

        MigrationHandlerMigrateStatus status = migrationHandlerSource.migrate(
                BROKER_TARGET_HOST, BROKER_TARGET_PORT, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT);

        assertEquals(MigrationHandlerMigrateStatus.STARTED, status);

        ArgumentCaptor<MigratePacket> messageCaptor = ArgumentCaptor.forClass(MigratePacket.class);
        verify(connectorToTarget.sourceToTargetClient).sendSync(messageCaptor.capture());
        MigratePacket capturedMessage = messageCaptor.getValue();
        assertNotNull(capturedMessage.getMigrationId());
        assertEquals(CLIENT_A_ID, capturedMessage.getClientId());
        assertEquals(CLIENT_A_HOST, capturedMessage.getClientHost());
        assertEquals(CLIENT_A_PORT, capturedMessage.getClientPort());
        assertEquals(0, capturedMessage.getSubscriptions().size());


        ArgumentCaptor<MigrationPacket> messageCaptorSource = ArgumentCaptor.forClass(MigrationPacket.class);
        verify(migrationHandlerSource).handle(any(), messageCaptorSource.capture());
        System.out.println(messageCaptorSource.getValue());

        ArgumentCaptor<MigrationPacket> messageCaptorClient = ArgumentCaptor.forClass(MigrationPacket.class);
        verify(migrationHandlerSource).handle(any(), messageCaptorSource.capture());
        System.out.println(messageCaptorSource.getValue());

        ArgumentCaptor<MigrationPacket> messageCaptorTarget = ArgumentCaptor.forClass(MigrationPacket.class);
        verify(migrationHandlerTarget).handle(any(), messageCaptorTarget.capture());
        System.out.println(messageCaptorTarget.getValue());
    }

}
