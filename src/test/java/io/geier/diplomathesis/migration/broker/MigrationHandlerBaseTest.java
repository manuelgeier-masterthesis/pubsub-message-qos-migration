package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;


public class MigrationHandlerBaseTest extends BaseTest {

    private static final MigrationRole MIGRATION_ROLE = MigrationRole.SOURCE;
    private static final String BROKER_TARGET_HOST = "10.0.0.2";
    private static final int BROKER_TARGET_PORT = 1234;
    private static final String MIGRATION_ID = "12345";

    private MigrationHandlerBase migrationHandler;
    private BrokerMigrationBridge migrationBridge;
    private MigrationStore migrationStore;
    private MigrationMessageStore migrationMessageStore;
    private MigrationPacketService messageService;

    @Before
    public void setUp() {
        migrationBridge = mock(BrokerMigrationBridge.class);
        migrationStore = Mockito.spy(new MigrationStore());
        migrationMessageStore = mock(MigrationMessageStore.class);
        messageService = mock(MigrationPacketService.class);
        migrationHandler = new MigrationHandlerBase(
                migrationBridge,
                mock(MqttMessageBarrier.class),
                migrationStore,
                migrationMessageStore,
                BROKER_TARGET_HOST,
                BROKER_TARGET_PORT,
                messageService,
                mock(Metric.class)
        ) {
            @Override
            protected MigrationRole getRole() {
                return MIGRATION_ROLE;
            }
        };
    }

    @Test
    public void getRole() {
        MigrationRole role = migrationHandler.getRole();

        assertEquals(MIGRATION_ROLE, role);
    }

    @Test
    public void verifyRole() {
        Migration migration = mock(Migration.class);
        doReturn(MigrationRole.SOURCE).when(migration).getRole();

        migrationHandler.verifyRole(migration, MigrationRole.SOURCE);
    }

    @Test(expected = MigrationException.class)
    public void verifyRole_withIncorrectRole_shouldThrowException() {
        Migration migration = mock(Migration.class);
        doReturn(MigrationRole.SOURCE).when(migration).getRole();

        migrationHandler.verifyRole(migration, MigrationRole.TARGET);
    }

    @Test(expected = MigrationException.class)
    public void verifyRole_withNullRole_shouldThrowException() {
        Migration migration = mock(Migration.class);
        doReturn(MigrationRole.SOURCE).when(migration).getRole();

        migrationHandler.verifyRole(migration, null);
    }

    @Test(expected = MigrationException.class)
    public void verifyRole_withNullMigration_shouldThrowException() {
        migrationHandler.verifyRole(null, MigrationRole.SOURCE);
    }

    @Test(expected = MigrationException.class)
    public void verifyRole_withNullMigrationAndNullRole_shouldThrowException() {
        migrationHandler.verifyRole(null, null);
    }

    @Test
    public void verifyMigrationState() {
        Migration migration = mock(Migration.class);
        doReturn(MigrationState.FINISHED).when(migration).getState();

        migrationHandler.verifyMigrationState(migration, MigrationState.FINISHED);
    }

    @Test(expected = MigrationException.class)
    public void verifyMigrationState_withIncorrectState_shouldThrowException() {
        Migration migration = mock(Migration.class);
        doReturn(MigrationState.SYNCED).when(migration).getState();

        migrationHandler.verifyMigrationState(migration, MigrationState.FINISHED);
    }

    @Test(expected = MigrationException.class)
    public void verifyMigrationState_withNullState_shouldThrowException() {
        Migration migration = mock(Migration.class);
        doReturn(MigrationState.FINISHED).when(migration).getState();

        migrationHandler.verifyMigrationState(migration, null);
    }

    @Test(expected = MigrationException.class)
    public void verifyMigrationState_withNullMigration_shouldThrowException() {
        migrationHandler.verifyMigrationState(null, MigrationState.FINISHED);
    }

    @Test(expected = MigrationException.class)
    public void verifyMigrationState_withNullMigrationAndNullState_shouldThrowException() {
        migrationHandler.verifyMigrationState(null, null);
    }

    @Test(expected = MigrationException.class)
    public void getVerifiedMigration_withNullId() {
        migrationHandler.getVerifiedMigration(null);
    }

    @Test(expected = MigrationException.class)
    public void getVerifiedMigration_withUnknownId() {
        doReturn(false).when(migrationStore).isMigrating(eq("123"));

        migrationHandler.getVerifiedMigration("123");
    }

    @Test
    public void getVerifiedMigration() {
        doReturn(true).when(migrationStore).isMigrating(eq(MIGRATION_ID));
        Migration migration = mock(Migration.class);
        doReturn(MIGRATION_ROLE).when(migration).getRole();
        doReturn(migration).when(migrationStore).getMigration(eq(MIGRATION_ID));

        migrationHandler.getVerifiedMigration(MIGRATION_ID);
    }

    @Test(expected = MigrationException.class)
    public void getVerifiedMigration_withIncorrectRole() {
        doReturn(true).when(migrationStore).isMigrating(eq(MIGRATION_ID));
        Migration migration = mock(Migration.class);
        doReturn(MigrationRole.TARGET).when(migration).getRole();
        doReturn(migration).when(migrationStore).getMigration(eq(MIGRATION_ID));

        migrationHandler.getVerifiedMigration(MIGRATION_ID);
    }
}
