package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.geier.diplomathesis.migration.broker.domain.Subscription;
import io.geier.diplomathesis.migration.communication.MigrationPacketClient;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncPacket;
import io.geier.diplomathesis.migration.messaging.GlobalMessageIdMatcher;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class MigrationHandlerSourceTest extends BaseTest {

    private static final String BROKER_SOURCE_HOST = "10.0.0.1";
    private static final int BROKER_SOURCE_PORT = 1234;
    private static final String BROKER_TARGET_HOST = "10.0.0.2";
    private static final int BROKER_TARGET_PORT = 1234;
    private static final String CLIENT_A_HOST = "10.0.1.1";
    private static final int CLIENT_A_PORT = 1234;
    private static final String CLIENT_A_ID = "Client A";
    private static final String CLIENT_B_HOST = "10.0.1.2";
    private static final int CLIENT_B_PORT = 1234;
    private static final String CLIENT_B_ID = "Client B";
    private static final String PUBLISHER_ID = "publisher_1";
    private static final String TOPIC_A = "Topic 1";
    private static final String TOPIC_B = "Topic 2";
    private static final String TOPIC_B2 = "Topic 2.2";
    private static final String TOPIC_C = "Topic 3";
    private final String MIGRATION_ID = "12345";

    private MigrationHandlerSource migrationHandlerSource;
    private BrokerMigrationBridge migrationBridge;
    private MigrationStore migrationStore;
    private Migration migration;
    private MigrationMessageStore migrationMessageStore;
    private List<Subscription> migrationSubscriptions;

    @Before
    public void setUp() {
        migrationBridge = mock(BrokerMigrationBridge.class);
        migrationStore = Mockito.spy(new MigrationStore());
        migrationMessageStore = mock(MigrationMessageStore.class);
        MigrationPacketService messageService = mock(MigrationPacketService.class);
        BrokerMigrationHandler migrationHandler = new BrokerMigrationHandler(
                BROKER_SOURCE_HOST,
                BROKER_SOURCE_PORT,
                mock(MqttMessageBarrier.class),
                migrationStore,
                migrationMessageStore,
                migrationBridge,
                messageService
        );
        migrationHandlerSource = new MigrationHandlerSource(migrationHandler);

        // create a migration
        migrationSubscriptions = new ArrayList<>();
        migrationSubscriptions.add(new Subscription(CLIENT_A_ID, TOPIC_A, MqttQoS.AT_MOST_ONCE));
        migrationSubscriptions.add(new Subscription(CLIENT_A_ID, TOPIC_B, MqttQoS.AT_LEAST_ONCE));
        migrationSubscriptions.add(new Subscription(CLIENT_A_ID, TOPIC_B2, MqttQoS.AT_LEAST_ONCE));
        migrationSubscriptions.add(new Subscription(CLIENT_A_ID, TOPIC_C, MqttQoS.EXACTLY_ONCE));

        doReturn(true).when(migrationBridge).clientIdExists(eq(CLIENT_A_ID));
        doReturn(mock(MigrationPacketClient.class)).when(messageService).createPacketClient(eq(BROKER_TARGET_HOST), eq(BROKER_TARGET_PORT), any());
        doReturn(migrationSubscriptions).when(migrationBridge).getSubscriptionsByClientId(eq(CLIENT_A_ID));
        doReturn(new GlobalMessageId(PUBLISHER_ID, "10")).when(migrationMessageStore).getLastProcessedGlobalMessageIdForTopic(any());

        migration = migrationHandlerSource.createSourceMigration(CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT, mock(MigrationPacketClient.class), migrationSubscriptions);

        doReturn(new GlobalMessageId(PUBLISHER_ID, "20")).when(migrationMessageStore).getLastProcessedGlobalMessageIdForTopic(any());
    }

    @Test
    public void getRole() {
        MigrationRole role = migrationHandlerSource.getRole();

        assertEquals(MigrationRole.SOURCE, role);
    }

    @Test
    public void migrate() {
        doReturn(false).when(migrationStore).isMigratingByClientId(eq(CLIENT_A_ID));

        migrationHandlerSource.migrate(BROKER_TARGET_HOST, BROKER_TARGET_PORT, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT);
//TODO
//        verify(migrationMessageStore, never()).startStoringMessages(eq(TOPIC_A));
//        verify(migrationMessageStore, never()).startStoringMessages(eq(TOPIC_B));
//        verify(migrationMessageStore, never()).startStoringMessages(eq(TOPIC_B2));
//        verify(migrationMessageStore).startStoringMessages(eq(TOPIC_C));
    }

    @Test
    public void handleMigAckMessage_withStatusOk_shouldFinishMigration() {
        MigAckPacket packet = new MigAckPacket(
                migration.getMigrationId(),
                MigAckPacket.MigStatus.OK);

        migrationHandlerSource.handleMigAckPacket(packet);

        verify(migrationBridge).removeSubscription(eq(CLIENT_A_ID), eq(TOPIC_A));
        verify(migrationBridge).removeSubscription(eq(CLIENT_A_ID), eq(TOPIC_B));
        verify(migrationBridge).removeSubscription(eq(CLIENT_A_ID), eq(TOPIC_C));
        verify(migrationBridge).disconnectClient(eq(CLIENT_A_ID));
        verify(migrationStore).remove(eq(migration));
        assertEquals(MigrationState.FINISHED, migration.getState());
    }

    @Test
    public void handleMigAckMessage_withStatusOk_withNoSubscriptions_shouldFinishMigration() {
        migration.getSubscriptions().clear();

        MigAckPacket packet = new MigAckPacket(
                migration.getMigrationId(),
                MigAckPacket.MigStatus.OK);

        migrationHandlerSource.handleMigAckPacket(packet);

        verify(migrationBridge, never()).publish(eq(CLIENT_A_ID), any());
        verify(migrationBridge, never()).removeSubscription(eq(CLIENT_A_ID), any());
        verify(migrationBridge).disconnectClient(eq(CLIENT_A_ID));
        verify(migrationStore).remove(eq(migration));
        assertEquals(MigrationState.FINISHED, migration.getState());
    }

    @Test
    public void handleMigAckMessage_withStatusError_shouldRollback() {
        MigAckPacket packet = new MigAckPacket(
                migration.getMigrationId(),
                MigAckPacket.MigStatus.ERROR);

        List<MigrationMessageStore.StoredMessage> storedMessagesTopic2 = new ArrayList<>();
        storedMessagesTopic2.add(new MigrationMessageStore.StoredMessage(CLIENT_A_ID, TOPIC_B, null, mock(GlobalMessageId.class), null));
        storedMessagesTopic2.add(new MigrationMessageStore.StoredMessage(CLIENT_A_ID, TOPIC_B, null, mock(GlobalMessageId.class), null));
        doReturn(storedMessagesTopic2).when(migrationMessageStore).getAll(eq(CLIENT_A_ID), eq(TOPIC_B));
        List<MigrationMessageStore.StoredMessage> storedMessagesTopic3 = new ArrayList<>();
        storedMessagesTopic3.add(new MigrationMessageStore.StoredMessage(CLIENT_A_ID, TOPIC_C, null, mock(GlobalMessageId.class), null));
        doReturn(storedMessagesTopic3).when(migrationMessageStore).getAll(eq(CLIENT_A_ID), eq(TOPIC_C));

        migrationHandlerSource.handleMigAckPacket(packet);

        verify(migrationMessageStore, never()).getAll(eq(CLIENT_A_ID), eq(TOPIC_A));
        verify(migrationMessageStore).getAll(eq(CLIENT_A_ID), eq(TOPIC_B));
        verify(migrationMessageStore).getAll(eq(CLIENT_A_ID), eq(TOPIC_C));
        ArgumentCaptor<List> messageToPublishCaptor = ArgumentCaptor.forClass(List.class);
        verify(migrationBridge).publish(eq(CLIENT_A_ID), messageToPublishCaptor.capture());
        assertEquals(3, messageToPublishCaptor.getValue().size());
        verify(migrationBridge, never()).removeSubscription(eq(CLIENT_A_ID), any());
        verify(migrationBridge, never()).disconnectClient(eq(CLIENT_A_ID));
        verify(migrationStore).remove(eq(migration));
        assertEquals(MigrationState.ERROR, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withNoSubscriptions_shouldBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS0Subscriptions_shouldWarnAndBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_A, mock(GlobalMessageId.class)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS1Subscription_thatIsBeforeOpponent_subscriptionShouldBeSyncing() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "21";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCING, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS1Subscription_thatIsEqualToOpponent_subscriptionShouldBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "20";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS1Subscription_thatIsBeforeOpponent_subscriptionShouldBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "19";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS1Subscription_oneThatIsBeforeAndOneThatIsAfterOpponent_subscriptionShouldBeSyncing() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "19";
        String messageId2OfOpponent = "21";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_B2, new GlobalMessageId(PUBLISHER_ID, messageId2OfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

//        verify(migrationMessageStore).startStoringMessages(TOPIC_B);
//        verify(migrationMessageStore, never()).startStoringMessages(TOPIC_B2);
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationSubscriptionState.SYNCING, migration.getSubscriptionByTopicName(TOPIC_B2).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());

        // if later the topic is synced, the migration should be synced
        migration.getSubscriptionByTopicName(TOPIC_B2).setState(MigrationSubscriptionState.SYNCED);

//        verify(migrationMessageStore).startStoringMessages(TOPIC_B2);
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_B2).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS2Subscription_opponentIsBeforeStopped_subscriptionShouldBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "9";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

        verify(migrationBridge, never()).publish(any(), any());
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS2Subscription_opponentIsEqualToStopped_subscriptionShouldBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "10";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

        verify(migrationBridge, never()).publish(any(), any());
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS2Subscription_opponentIsAfterToStopped_subscriptionShouldBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "11";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);

        migrationHandlerSource.handleMigSyncPacket(packet);

        verify(migrationMessageStore).findAllBetweenLowExAndUp(eq(CLIENT_A_ID), eq(TOPIC_C),
                GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "10")),
                GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "11")));
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS2Subscription_opponentIsBeforeCurrent_subscriptionShouldBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "19";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);
        doReturn(Arrays.asList(new MigrationMessageStore.StoredMessage(CLIENT_A_ID, TOPIC_A, null, new GlobalMessageId(PUBLISHER_ID, "11"), null)))
                .when(migrationMessageStore)
                .findAllBetweenLowExAndUp(eq(CLIENT_A_ID), eq(TOPIC_C),
                        GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "10")),
                        GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "19")));

        migrationHandlerSource.handleMigSyncPacket(packet);

        verify(migrationMessageStore).findAllBetweenLowExAndUp(eq(CLIENT_A_ID), eq(TOPIC_C),
                GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "10")),
                GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "19")));
        verify(migrationBridge).publish(eq(CLIENT_A_ID), any());
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS2Subscription_opponentIsEqualToCurrent_subscriptionShouldBeSynced() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "20";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);
        doReturn(Arrays.asList(new MigrationMessageStore.StoredMessage(CLIENT_A_ID, TOPIC_A, null, new GlobalMessageId(PUBLISHER_ID, "11"), null)))
                .when(migrationMessageStore)
                .findAllBetweenLowExAndUp(eq(CLIENT_A_ID), eq(TOPIC_C),
                        GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "10")),
                        GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "20")));

        migrationHandlerSource.handleMigSyncPacket(packet);

        verify(migrationMessageStore).findAllBetweenLowExAndUp(eq(CLIENT_A_ID), eq(TOPIC_C),
                GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "10")),
                GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "20")));
        verify(migrationBridge).publish(eq(CLIENT_A_ID), any());
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncMessage_withQoS2Subscription_opponentIsAfterCurrent_subscriptionShouldBeSyncing() {
        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        String messageIdOfOpponent = "21";
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOfOpponent)));
        MigSyncPacket packet = new MigSyncPacket(migration.getMigrationId(), subscriptions);
        doReturn(Arrays.asList(new MigrationMessageStore.StoredMessage(CLIENT_A_ID, TOPIC_A, null, new GlobalMessageId(PUBLISHER_ID, "11"), null)))
                .when(migrationMessageStore)
                .findAllBetweenLowExAndUp(eq(CLIENT_A_ID), eq(TOPIC_C),
                        GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "10")),
                        GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "20")));

        migrationHandlerSource.handleMigSyncPacket(packet);

        verify(migrationMessageStore).findAllBetweenLowExAndUp(eq(CLIENT_A_ID), eq(TOPIC_C),
                GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "10")),
                GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER_ID, "20")));
        verify(migrationBridge).publish(eq(CLIENT_A_ID), any());
        assertEquals(MigrationSubscriptionState.SYNCING, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }
}
