package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import io.geier.diplomathesis.migration.communication.MigrationPacketClient;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncPacket;
import io.geier.diplomathesis.migration.communication.packet.MigToAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigToPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.netty.util.concurrent.GenericFutureListener;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


public class MigrationHandlerTargetTest extends BaseTest {

    private static final String BROKER_SOURCE_HOST = "10.0.0.1";
    private static final int BROKER_SOURCE_PORT = 1234;
    private static final String BROKER_TARGET_HOST = "10.0.0.2";
    private static final int BROKER_TARGET_PORT = 1234;
    private static final String CLIENT_A_HOST = "10.0.1.1";
    private static final int CLIENT_A_PORT = 1234;
    private static final String CLIENT_A_ID = "Client A";
    private static final String CLIENT_B_HOST = "10.0.1.2";
    private static final int CLIENT_B_PORT = 1234;
    private static final String CLIENT_B_ID = "Client B";
    private static final String PUBLISHER_ID = "publisher_1";
    private static final String TOPIC_A = "Topic 1";
    private static final String TOPIC_B = "Topic 2";
    private static final String TOPIC_B2 = "Topic 2.2";
    private static final String TOPIC_C = "Topic 3";
    private static final String TOPIC_C2 = "Topic 3.2";
    private final String MIGRATION_ID = "12345";
    private final String MIGRATION2_ID = "678";

    private MigrationHandlerTarget migrationHandlerTarget;
    private BrokerMigrationBridge migrationBridge;
    private MigrationStore migrationStore;
    private Migration migration;
    private MigrationMessageStore migrationMessageStore;
    private MigrationPacketClient clientPacketClient;
    private MigrationPacketService migrationPacketService;
    private MigrationPacketClient sourceBrokerMessageClient;

    @Before
    public void setUp() {
        migrationBridge = mock(BrokerMigrationBridge.class);
        migrationStore = Mockito.spy(new MigrationStore());
        migrationMessageStore = mock(MigrationMessageStore.class);
        migrationPacketService = mock(MigrationPacketService.class);
        BrokerMigrationHandler migrationHandler = new BrokerMigrationHandler(
                BROKER_TARGET_HOST,
                BROKER_TARGET_PORT,
                mock(MqttMessageBarrier.class),
                migrationStore,
                migrationMessageStore,
                migrationBridge,
                migrationPacketService
        );
        migrationHandlerTarget = new MigrationHandlerTarget(migrationHandler);

        sourceBrokerMessageClient = mock(MigrationPacketClient.class);
        ChannelFuture channelFuture = mock(ChannelFuture.class);
        doAnswer(invocation -> {
            GenericFutureListener l = invocation.getArgument(0);
            // do not call, to prevent from FINISH migration
            //l.operationComplete(null);
            return channelFuture;
        }).when(channelFuture).addListener(any());
        doReturn(channelFuture).when(sourceBrokerMessageClient).send(any());

        clientPacketClient = mock(MigrationPacketClient.class);
        doReturn(clientPacketClient).when(migrationPacketService).createPacketClient(eq(CLIENT_A_HOST), eq(CLIENT_A_PORT), any());
    }

    private void setupMigration(List<MigratePacket.MigrateSubscription> migrationSubscriptions) {
        doReturn(new GlobalMessageId(PUBLISHER_ID, "10")).when(migrationMessageStore).getLastProcessedGlobalMessageIdForTopic(any());

        migration = migrationHandlerTarget.createMigrationOnTarget(MIGRATION_ID, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT, sourceBrokerMessageClient, migrationSubscriptions);
    }

    @Test
    public void getRole() {
        MigrationRole role = migrationHandlerTarget.getRole();

        assertEquals(MigrationRole.TARGET, role);
    }

    @Test
    public void handleMigrateMessage_withExistingMigration_shouldReturnError() {
        setupMigration(new ArrayList<>());

        MigratePacket packet = new MigratePacket(MIGRATION_ID, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT, new ArrayList<>());

        Channel channel = mock(Channel.class);
        migrationHandlerTarget.handleMigratePacket(channel, packet);

        ArgumentCaptor<MigAckPacket> migAckMessageCaptor = ArgumentCaptor.forClass(MigAckPacket.class);
        verify(channel).writeAndFlush(migAckMessageCaptor.capture());
        MigAckPacket migAckPacket = migAckMessageCaptor.getValue();
        assertEquals(MIGRATION_ID, migAckPacket.getMigrationId());
        assertEquals(MigAckPacket.MigStatus.ERROR, migAckPacket.getStatusCode());
    }

    @Test
    public void handleMigrateMessage_withEmptySubscription() {
        List<MigratePacket.MigrateSubscription> subscriptions = new ArrayList<>();
        MigratePacket packet = new MigratePacket(MIGRATION2_ID, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT, subscriptions);

        Channel channel = mock(Channel.class);
        migrationHandlerTarget.handleMigratePacket(channel, packet);

        // no packet to broker
        verify(channel, never()).writeAndFlush(any());
        // packet to client
        ArgumentCaptor<MigToPacket> migToPacketCaptor = ArgumentCaptor.forClass(MigToPacket.class);
        verify(clientPacketClient).sendSync(migToPacketCaptor.capture());
        MigToPacket migAckMessage = migToPacketCaptor.getValue();
        assertEquals(MIGRATION2_ID, migAckMessage.getMigrationId());
        assertEquals(BROKER_TARGET_HOST, migAckMessage.getHost());
        assertEquals(BROKER_TARGET_PORT, migAckMessage.getPort());

        Migration migration = migrationStore.getMigration(MIGRATION2_ID);
        // migration created
        assertNotNull(migration);
        assertEquals(2, migration.getStateListeners().size());
    }

    @Test
    public void handleMigrateMessage_withQos0Subscription() {
        List<MigratePacket.MigrateSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.AT_MOST_ONCE, TOPIC_A, new GlobalMessageId(PUBLISHER_ID, "10")));
        MigratePacket packet = new MigratePacket(MIGRATION2_ID, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT, subscriptions);

        Channel channel = mock(Channel.class);
        migrationHandlerTarget.handleMigratePacket(channel, packet);

        // no packet to broker
        verify(channel, never()).writeAndFlush(any());
        // packet to client
        ArgumentCaptor<MigToPacket> migToPacketCaptor = ArgumentCaptor.forClass(MigToPacket.class);
        verify(clientPacketClient).sendSync(migToPacketCaptor.capture());
        MigToPacket migAckMessage = migToPacketCaptor.getValue();
        assertEquals(MIGRATION2_ID, migAckMessage.getMigrationId());
        assertEquals(BROKER_TARGET_HOST, migAckMessage.getHost());
        assertEquals(BROKER_TARGET_PORT, migAckMessage.getPort());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos0Subscription_thatSourceIsBehind() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "9";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.AT_MOST_ONCE, TOPIC_A, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_A).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos0Subscription_thatSourceIsEqual() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.AT_MOST_ONCE, TOPIC_A, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_A).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos0Subscription_thatSourceIsAhead() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "11";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.AT_MOST_ONCE, TOPIC_A, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        verify(sourceBrokerMessageClient, never()).sendSync(any());
        assertEquals(MigrationSubscriptionState.SYNCING_THIS, migration.getSubscriptionByTopicName(TOPIC_A).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos1Subscription_thatSourceIsBehind() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "9";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.AT_LEAST_ONCE, TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        ArgumentCaptor<MigSyncPacket> migSyncMessageArgumentCaptor = ArgumentCaptor.forClass(MigSyncPacket.class);
        verify(sourceBrokerMessageClient).sendSync(migSyncMessageArgumentCaptor.capture());
        MigSyncPacket migSyncPacket = migSyncMessageArgumentCaptor.getValue();
        assertEquals(MIGRATION_ID, migSyncPacket.getMigrationId());
        assertTrue(BROKER_TARGET_HOST, contains(migSyncPacket.getSubscriptions(), TOPIC_B, new GlobalMessageId(PUBLISHER_ID, "10")));
        assertEquals(MigrationSubscriptionState.SYNCING_OPPONENT, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos1Subscription_thatSourceIsEqual() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.AT_LEAST_ONCE, TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        verify(sourceBrokerMessageClient, never()).sendSync(any());
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos1Subscription_thatSourceIsAhead() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "11";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.AT_LEAST_ONCE, TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        verify(sourceBrokerMessageClient, never()).sendSync(any());
        assertEquals(MigrationSubscriptionState.SYNCING_THIS, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos2Subscription_thatSourceIsBehind() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "9";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        ArgumentCaptor<MigSyncPacket> migSyncMessageArgumentCaptor = ArgumentCaptor.forClass(MigSyncPacket.class);
        verify(sourceBrokerMessageClient).sendSync(migSyncMessageArgumentCaptor.capture());
        MigSyncPacket migSyncPacket = migSyncMessageArgumentCaptor.getValue();
        assertEquals(MIGRATION_ID, migSyncPacket.getMigrationId());
        assertTrue(BROKER_TARGET_HOST, contains(migSyncPacket.getSubscriptions(), TOPIC_C, new GlobalMessageId(PUBLISHER_ID, "10")));
        assertEquals(MigrationSubscriptionState.SYNCING_OPPONENT, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos2Subscription_thatSourceIsEqual() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        verify(sourceBrokerMessageClient, never()).sendSync(any());
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigToAckMessageMessage_withQos2Subscription_thatSourceIsAhead() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "11";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);

        MigToAckPacket packet = new MigToAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigToAckPacket(packet);

        verify(sourceBrokerMessageClient, never()).sendSync(any());
        assertEquals(MigrationSubscriptionState.SYNCING_THIS, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test(expected = MigrationException.class)
    public void handleMigSyncAckMessage_syncedMigration_shouldThrowException() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCED);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);
    }

    @Test
    public void handleMigSyncAckMessage_withQos0_shouldWarn() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_A, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);
    }

    @Test
    public void handleMigSyncAckMessage_withQos1_SYNCING() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);
        migration.getSubscriptionByTopicName(TOPIC_B).setState(MigrationSubscriptionState.SYNCING);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCING_THIS, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigSyncAckMessage_withQos1_SYNCING_OPPONENT() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);
        migration.getSubscriptionByTopicName(TOPIC_B).setState(MigrationSubscriptionState.SYNCING_OPPONENT);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncAckMessage_withQos1_SYNCING_THIS_shouldWarn() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);
        migration.getSubscriptionByTopicName(TOPIC_B).setState(MigrationSubscriptionState.SYNCING_THIS);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCING_THIS, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigSyncAckMessage_withQos1_SYNCED() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_B, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_B2, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);
        migration.getSubscriptionByTopicName(TOPIC_B).setState(MigrationSubscriptionState.SYNCED);
        migration.getSubscriptionByTopicName(TOPIC_B2).setState(MigrationSubscriptionState.SYNCING_OPPONENT);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_B).getState());
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_B2).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncAckMessage_withQos2_SYNCING() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);
        migration.getSubscriptionByTopicName(TOPIC_C).setState(MigrationSubscriptionState.SYNCING);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCING_THIS, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigSyncAckMessage_withQos2_SYNCING_OPPONENT() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);
        migration.getSubscriptionByTopicName(TOPIC_C).setState(MigrationSubscriptionState.SYNCING_OPPONENT);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    @Test
    public void handleMigSyncAckMessage_withQos2_SYNCING_THIS_shouldWarn() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);
        migration.getSubscriptionByTopicName(TOPIC_C).setState(MigrationSubscriptionState.SYNCING_THIS);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCING_THIS, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationState.SYNCING, migration.getState());
    }

    @Test
    public void handleMigSyncAckMessage_withQos2_SYNCED() {
        List<MigratePacket.MigrateSubscription> migrationSubscriptions = new ArrayList<>();
        String messageIdOpponent = "10";
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_C, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        migrationSubscriptions.add(new MigratePacket.MigrateSubscription(MqttQoS.EXACTLY_ONCE, TOPIC_C2, new GlobalMessageId(PUBLISHER_ID, messageIdOpponent)));
        setupMigration(migrationSubscriptions);
        migration.setState(MigrationState.SYNCING);
        migration.getSubscriptionByTopicName(TOPIC_C).setState(MigrationSubscriptionState.SYNCED);
        migration.getSubscriptionByTopicName(TOPIC_C2).setState(MigrationSubscriptionState.SYNCING_OPPONENT);

        MigSyncAckPacket packet = new MigSyncAckPacket(MIGRATION_ID);
        migrationHandlerTarget.handleMigSyncAckPacket(packet);

        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C).getState());
        assertEquals(MigrationSubscriptionState.SYNCED, migration.getSubscriptionByTopicName(TOPIC_C2).getState());
        assertEquals(MigrationState.SYNCED, migration.getState());
    }

    private boolean contains(List<MigSyncPacket.MigSyncSubscription> subscriptions, String topicName, GlobalMessageId globalMessageId) {
        for (MigSyncPacket.MigSyncSubscription subscription : subscriptions) {
            if (subscription.getTopicName().equals(topicName) && subscription.getLastProcessedGlobalMessageId().isEqualTo(globalMessageId)) {
                return true;
            }
        }
        return false;
    }

}
