package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.domain.Subscription;
import io.geier.diplomathesis.migration.communication.HttpMigrationPacketClient;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MigrationHandlerTest extends BaseTest {

    private static final String BROKER_SOURCE_HOST = "10.0.0.1";
    private static final int BROKER_SOURCE_PORT = 1234;
    private static final String BROKER_TARGET_HOST = "10.0.0.2";
    private static final int BROKER_TARGET_PORT = 1234;
    private static final String CLIENT_A_HOST = "10.0.1.1";
    private static final int CLIENT_A_PORT = 1234;
    private static final String CLIENT_A_ID = "Client A";
    private static final String CLIENT_B_HOST = "10.0.1.2";
    private static final int CLIENT_B_PORT = 1234;
    private static final String CLIENT_B_ID = "Client B";
    private static final String TOPIC_1 = "Topic 1";
    private static final String TOPIC_2 = "Topic 2";
    private static final String TOPIC_3 = "Topic 3";

    private BrokerMigrationHandler migrationHandler;
    private MigrationPacketService messageService;
    private HttpMigrationPacketClient messageClient;
    private BrokerMigrationBridge migrationBridge;

    @Before
    public void setUp() {
        migrationBridge = mock(BrokerMigrationBridge.class);
        doReturn(true).when(migrationBridge).clientIdExists(eq(CLIENT_A_ID));

        messageService = mock(MigrationPacketService.class);
        messageClient = mock(HttpMigrationPacketClient.class);
        when(messageService.createPacketClient(eq(BROKER_TARGET_HOST), anyInt(), any())).thenReturn(messageClient);

        MigrationMessageStore migrationMessageStore = mock(MigrationMessageStore.class);

        migrationHandler = new BrokerMigrationHandler(
                BROKER_SOURCE_HOST,
                BROKER_SOURCE_PORT,
                mock(MqttMessageBarrier.class),
                new MigrationStore(),
                migrationMessageStore,
                migrationBridge,
                messageService
        );

        when(migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(anyString())).thenReturn(mock(GlobalMessageId.class));
    }

    @Test
    public void migrate_withNoSubscriptions() {
        when(migrationBridge.getSubscriptionsByClientId(CLIENT_A_ID)).thenReturn(new ArrayList<>());

        MigrationHandlerMigrateStatus status = migrationHandler.migrate(
                BROKER_TARGET_HOST, BROKER_TARGET_PORT, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT);

        assertEquals(MigrationHandlerMigrateStatus.STARTED, status);

        ArgumentCaptor<MigratePacket> messageCaptor = ArgumentCaptor.forClass(MigratePacket.class);
        verify(messageClient).sendSync(messageCaptor.capture());
        MigratePacket capturedMessage = messageCaptor.getValue();
        assertNotNull(capturedMessage.getMigrationId());
        assertEquals(CLIENT_A_ID, capturedMessage.getClientId());
        assertEquals(CLIENT_A_HOST, capturedMessage.getClientHost());
        assertEquals(CLIENT_A_PORT, capturedMessage.getClientPort());
        assertEquals(0, capturedMessage.getSubscriptions().size());
    }

    @Test
    public void migrate_withSubscriptions() {
        List<Subscription> clientASubscriptions = new ArrayList<>();
        clientASubscriptions.add(new Subscription(CLIENT_A_ID, TOPIC_1, MqttQoS.EXACTLY_ONCE));
        clientASubscriptions.add(new Subscription(CLIENT_A_ID, TOPIC_2, MqttQoS.EXACTLY_ONCE));
        when(migrationBridge.getSubscriptionsByClientId(CLIENT_A_ID)).thenReturn(clientASubscriptions);

        MigrationHandlerMigrateStatus status = migrationHandler.migrate(
                BROKER_TARGET_HOST, BROKER_TARGET_PORT, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT);

        assertEquals(MigrationHandlerMigrateStatus.STARTED, status);

        ArgumentCaptor<MigratePacket> messageCaptor = ArgumentCaptor.forClass(MigratePacket.class);
        verify(messageClient).sendSync(messageCaptor.capture());
        MigratePacket capturedMessage = messageCaptor.getValue();
        assertNotNull(capturedMessage.getMigrationId());
        assertEquals(CLIENT_A_ID, capturedMessage.getClientId());
        assertEquals(CLIENT_A_HOST, capturedMessage.getClientHost());
        assertEquals(CLIENT_A_PORT, capturedMessage.getClientPort());
        assertEquals(2, capturedMessage.getSubscriptions().size());
    }

    @Test
    public void migrate_withUnknownClient_shouldError() {
        MigrationHandlerMigrateStatus status = migrationHandler.migrate(
                BROKER_TARGET_HOST, BROKER_TARGET_PORT, "Unknown", CLIENT_A_HOST, CLIENT_A_PORT);

        assertEquals(MigrationHandlerMigrateStatus.ERROR_CLIENT_NOT_FOUND, status);
    }

    @Test
    public void migrate_withInProgressClient_shouldError() {
        MigrationHandlerMigrateStatus status = migrationHandler.migrate(
                BROKER_TARGET_HOST, BROKER_TARGET_PORT, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT);
        assertEquals(MigrationHandlerMigrateStatus.STARTED, status);

        status = migrationHandler.migrate(
                BROKER_TARGET_HOST, BROKER_TARGET_PORT, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT);

        assertEquals(MigrationHandlerMigrateStatus.ERROR_CLIENT_ALREADY_MIGRATING, status);
    }

}
