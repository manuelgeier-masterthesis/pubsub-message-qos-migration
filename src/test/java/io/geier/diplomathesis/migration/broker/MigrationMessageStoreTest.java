package io.geier.diplomathesis.migration.broker;

import io.geier.diplomathesis.migration.broker.MigrationMessageStore;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MigrationMessageStoreTest {

    private MigrationMessageStore migrationMessageStore;

    @Before
    public void setUp() {
        migrationMessageStore = new MigrationMessageStore();
    }

    @Test
    public void dummy() {
        Assert.assertNotNull(migrationMessageStore);
    }

}
