package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MqttMessageBarrierQoS0SourceTest extends BaseTest {

    private MqttMessageBarrierQoS0Source messageBarrierQoS0Source;
    private Migration.Subscription subscription;
    private Migration migration;

    @Before
    public void setUp() {
        messageBarrierQoS0Source = new MqttMessageBarrierQoS0Source();
        migration = mock(Migration.class);
        subscription = mock(Migration.Subscription.class);
    }

    @Test
    public void canSendMessage_withStateINITIALIZED() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.INITIALIZED);

        boolean result = messageBarrierQoS0Source.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertFalse(result);
    }

    @Test(expected = MigrationException.class)
    public void canSendMessage_withStateSYNCING_isNotSupportedAndThrowsException() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);

        messageBarrierQoS0Source.canSendMessage(subscription, mock(GlobalMessageId.class));
    }

    @Test
    public void canSendMessage_withStateSYNCED() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCED);

        boolean result = messageBarrierQoS0Source.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertFalse(result);
    }

    @Test
    public void canSendMessage_withStateFINISHED() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.FINISHED);

        boolean result = messageBarrierQoS0Source.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertFalse(result);
    }

    @Test
    public void canSendMessage_withStateERROR() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.ERROR);

        boolean result = messageBarrierQoS0Source.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertTrue(result);
    }

}
