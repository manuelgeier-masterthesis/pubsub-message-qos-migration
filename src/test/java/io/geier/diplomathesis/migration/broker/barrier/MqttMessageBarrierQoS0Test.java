package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MqttMessageBarrierQoS0Test extends BaseTest {

    private MqttMessageBarrierQoS0 messageBarrierQoS0;
    private Migration.Subscription subscription;
    private Migration migration;
    private MqttMessageBarrierQoS0Source messageBarrierQoS0Source;
    private MqttMessageBarrierQoS0Target messageBarrierQoS0Target;
    private GlobalMessageId globalMessageId;
    private Boolean expectedResult;

    @Before
    public void setUp() {
        messageBarrierQoS0Source = mock(MqttMessageBarrierQoS0Source.class);
        messageBarrierQoS0Target = mock(MqttMessageBarrierQoS0Target.class);
        messageBarrierQoS0 = new MqttMessageBarrierQoS0(
                messageBarrierQoS0Source,
                messageBarrierQoS0Target
        );
        migration = mock(Migration.class);
        subscription = mock(Migration.Subscription.class);
        globalMessageId = mock(GlobalMessageId.class);
        expectedResult = Boolean.FALSE;
    }

    @Test
    public void canSendMessage_asSourceMigration_shouldCallSource() {
        when(migration.getRole()).thenReturn(MigrationRole.SOURCE);

        Boolean result = messageBarrierQoS0.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS0Source).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS0Target, never()).canSendMessage(subscription, globalMessageId);
        assertEquals(expectedResult, result);
    }

    @Test
    public void canSendMessage_asTargetMigration_shouldCallTarget() {
        when(migration.getRole()).thenReturn(MigrationRole.TARGET);

        Boolean result = messageBarrierQoS0.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS0Source, never()).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS0Target).canSendMessage(subscription, globalMessageId);
        assertEquals(expectedResult, result);
    }

    @Test(expected = MigrationException.class)
    public void canSendMessage_asUnknownMigration_shouldThrowException() {
        when(migration.getRole()).thenReturn(null);

        messageBarrierQoS0.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS0Source, never()).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS0Target, never()).canSendMessage(subscription, globalMessageId);
    }

}
