package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MqttMessageBarrierQoS1TargetTest extends BaseTest {

    private final String TOPIC_ID = "producer_1_topic_1";

    private MqttMessageBarrierQoS1Target messageBarrierQoS1Target;
    private Migration.Subscription subscription;
    private Migration migration;
    private GlobalMessageId globalMessageId;

    @Before
    public void setUp() {
        messageBarrierQoS1Target = new MqttMessageBarrierQoS1Target();
        migration = mock(Migration.class);
        subscription = mock(Migration.Subscription.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void canSendMessage_withStateINITIALIZED() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.INITIALIZED);

        boolean result = messageBarrierQoS1Target.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertFalse(result);
    }

    @Test
    public void canSendMessage_withStateSYNCING_andBeforeBoundary() {
        String currentId = "9";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS1Target.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setThisSYNCED();
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCING_andEqualBoundary() {
        String currentId = "10";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS1Target.canSendMessage(subscription, globalMessageId);

        verify(subscription).setThisSYNCED();
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCING_andAfterBoundary() {
        String currentId = "11";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS1Target.canSendMessage(subscription, globalMessageId);

        verify(subscription).setThisSYNCED();
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCED_andBeforeBoundary() {
        String currentId = "9";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCED);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS1Target.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setThisSYNCED();
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCED_andEqualBoundary() {
        String currentId = "10";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCED);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS1Target.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setThisSYNCED();
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCED_andAfterBoundary() {
        String currentId = "11";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCED);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS1Target.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setThisSYNCED();
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateFINISHED() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.FINISHED);

        boolean result = messageBarrierQoS1Target.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertTrue(result);
    }

}
