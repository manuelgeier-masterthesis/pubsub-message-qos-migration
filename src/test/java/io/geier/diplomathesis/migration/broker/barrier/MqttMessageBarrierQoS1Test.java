package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MqttMessageBarrierQoS1Test extends BaseTest {

    private MqttMessageBarrierQoS1 messageBarrierQoS1;
    private Migration.Subscription subscription;
    private Migration migration;
    private MqttMessageBarrierQoS1Source messageBarrierQoS1Source;
    private MqttMessageBarrierQoS1Target messageBarrierQoS1Target;
    private GlobalMessageId globalMessageId;
    private Boolean expectedResult;

    @Before
    public void setUp() {
        messageBarrierQoS1Source = mock(MqttMessageBarrierQoS1Source.class);
        messageBarrierQoS1Target = mock(MqttMessageBarrierQoS1Target.class);
        messageBarrierQoS1 = new MqttMessageBarrierQoS1(
                messageBarrierQoS1Source,
                messageBarrierQoS1Target
        );
        migration = mock(Migration.class);
        subscription = mock(Migration.Subscription.class);
        globalMessageId = mock(GlobalMessageId.class);
        expectedResult = Boolean.FALSE;
    }

    @Test
    public void canSendMessage_asSourceMigration_shouldCallSource() {
        when(migration.getRole()).thenReturn(MigrationRole.SOURCE);

        Boolean result = messageBarrierQoS1.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS1Source).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS1Target, never()).canSendMessage(subscription, globalMessageId);
        assertEquals(expectedResult, result);
    }

    @Test
    public void canSendMessage_asTargetMigration_shouldCallTarget() {
        when(migration.getRole()).thenReturn(MigrationRole.TARGET);

        Boolean result = messageBarrierQoS1.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS1Source, never()).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS1Target).canSendMessage(subscription, globalMessageId);
        assertEquals(expectedResult, result);
    }

    @Test(expected = MigrationException.class)
    public void canSendMessage_asUnknownMigration_shouldThrowException() {
        when(migration.getRole()).thenReturn(null);

        messageBarrierQoS1.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS1Source, never()).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS1Target, never()).canSendMessage(subscription, globalMessageId);
    }

}
