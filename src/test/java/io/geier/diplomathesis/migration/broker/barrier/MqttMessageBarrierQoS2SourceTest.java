package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MqttMessageBarrierQoS2SourceTest extends BaseTest {

    private final String TOPIC_ID = "producer_1_topic_1";

    private MqttMessageBarrierQoS2Source messageBarrierQoS2Source;
    private Migration.Subscription subscription;
    private Migration migration;
    private GlobalMessageId globalMessageId;

    @Before
    public void setUp() {
        messageBarrierQoS2Source = new MqttMessageBarrierQoS2Source();
        migration = mock(Migration.class);
        subscription = mock(Migration.Subscription.class);
    }

    @Test
    public void canSendMessage_withStateINITIALIZED_andBeforeLowerBoundary() {
        String currentId = "4";
        String lowerBound = "5";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.INITIALIZED);
        GlobalMessageId globalMessageIdLowerBound = new GlobalMessageId(TOPIC_ID, lowerBound);
        when(subscription.getLastProcessedGlobalMessageId()).thenReturn(globalMessageIdLowerBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setState(any());
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateINITIALIZED_andIsLowerBoundary() {
        String currentId = "5";
        String lowerBound = "5";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.INITIALIZED);
        GlobalMessageId globalMessageIdLowerBound = new GlobalMessageId(TOPIC_ID, lowerBound);
        when(subscription.getLastProcessedGlobalMessageId()).thenReturn(globalMessageIdLowerBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setState(any());
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateINITIALIZED_andAfterLowerBoundary() {
        String currentId = "6";
        String lowerBound = "5";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.INITIALIZED);
        GlobalMessageId globalMessageIdLowerBound = new GlobalMessageId(TOPIC_ID, lowerBound);
        when(subscription.getLastProcessedGlobalMessageId()).thenReturn(globalMessageIdLowerBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setState(any());
        assertFalse(result);
    }

    @Test
    public void canSendMessage_withStateSYNCING_andBeforeLowerBoundary() {
        String currentId = "4";
        String lowerBound = "5";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);
        GlobalMessageId globalMessageIdLowerBound = new GlobalMessageId(TOPIC_ID, lowerBound);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageId()).thenReturn(globalMessageIdLowerBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setState(any());
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCING_andIsLowerBoundary() {
        String currentId = "5";
        String lowerBound = "5";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);
        GlobalMessageId globalMessageIdLowerBound = new GlobalMessageId(TOPIC_ID, lowerBound);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageId()).thenReturn(globalMessageIdLowerBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setState(any());
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCING_andIsBetweenBoundary() {
        String currentId = "7";
        String lowerBound = "5";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);
        GlobalMessageId globalMessageIdLowerBound = new GlobalMessageId(TOPIC_ID, lowerBound);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageId()).thenReturn(globalMessageIdLowerBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);

        verify(subscription, never()).setState(any());
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCING_andIsUpperBoundary() {
        String currentId = "10";
        String lowerBound = "5";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);
        GlobalMessageId globalMessageIdLowerBound = new GlobalMessageId(TOPIC_ID, lowerBound);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageId()).thenReturn(globalMessageIdLowerBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);

        verify(subscription).setState(eq(MigrationSubscriptionState.SYNCED));
        assertTrue(result);
    }

    @Test
    public void canSendMessage_withStateSYNCING_andIsAfterUpperBoundary() {
        String currentId = "11";
        String lowerBound = "5";
        String upperBound = "10";

        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCING);
        GlobalMessageId globalMessageIdLowerBound = new GlobalMessageId(TOPIC_ID, lowerBound);
        GlobalMessageId globalMessageIdUpperBound = new GlobalMessageId(TOPIC_ID, upperBound);
        when(subscription.getLastProcessedGlobalMessageId()).thenReturn(globalMessageIdLowerBound);
        when(subscription.getLastProcessedGlobalMessageIdByOpponent()).thenReturn(globalMessageIdUpperBound);
        globalMessageId = new GlobalMessageId(TOPIC_ID, currentId);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, globalMessageId);

        verify(subscription).setState(eq(MigrationSubscriptionState.SYNCED));
        assertFalse(result);
    }

    @Test
    public void canSendMessage_withStateSYNCED() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.SYNCED);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertFalse(result);
    }

    @Test
    public void canSendMessage_withStateFINISHED() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.FINISHED);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertFalse(result);
    }

    @Test
    public void canSendMessage_withStateERROR() {
        when(subscription.getState()).thenReturn(MigrationSubscriptionState.ERROR);

        boolean result = messageBarrierQoS2Source.canSendMessage(subscription, mock(GlobalMessageId.class));

        assertTrue(result);
    }

}
