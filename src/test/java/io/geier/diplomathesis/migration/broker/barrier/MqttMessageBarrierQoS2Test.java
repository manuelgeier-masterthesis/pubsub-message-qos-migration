package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.MigrationRole;
import io.geier.diplomathesis.migration.broker.exception.MigrationException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MqttMessageBarrierQoS2Test extends BaseTest {

    private MqttMessageBarrierQoS2 messageBarrierQoS2;
    private Migration.Subscription subscription;
    private Migration migration;
    private MqttMessageBarrierQoS2Source messageBarrierQoS2Source;
    private MqttMessageBarrierQoS2Target messageBarrierQoS2Target;
    private GlobalMessageId globalMessageId;
    private Boolean expectedResult;

    @Before
    public void setUp() {
        messageBarrierQoS2Source = mock(MqttMessageBarrierQoS2Source.class);
        messageBarrierQoS2Target = mock(MqttMessageBarrierQoS2Target.class);
        messageBarrierQoS2 = new MqttMessageBarrierQoS2(
                messageBarrierQoS2Source,
                messageBarrierQoS2Target
        );
        migration = mock(Migration.class);
        subscription = mock(Migration.Subscription.class);
        globalMessageId = mock(GlobalMessageId.class);
        expectedResult = Boolean.FALSE;
    }

    @Test
    public void canSendMessage_asSourceMigration_shouldCallSource() {
        when(migration.getRole()).thenReturn(MigrationRole.SOURCE);

        Boolean result = messageBarrierQoS2.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS2Source).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS2Target, never()).canSendMessage(subscription, globalMessageId);
        assertEquals(expectedResult, result);
    }

    @Test
    public void canSendMessage_asTargetMigration_shouldCallTarget() {
        when(migration.getRole()).thenReturn(MigrationRole.TARGET);

        Boolean result = messageBarrierQoS2.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS2Source, never()).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS2Target).canSendMessage(subscription, globalMessageId);
        assertEquals(expectedResult, result);
    }

    @Test(expected = MigrationException.class)
    public void canSendMessage_asUnknownMigration_shouldThrowException() {
        when(migration.getRole()).thenReturn(null);

        messageBarrierQoS2.canSendMessage(subscription, globalMessageId, migration);

        verify(messageBarrierQoS2Source, never()).canSendMessage(subscription, globalMessageId);
        verify(messageBarrierQoS2Target, never()).canSendMessage(subscription, globalMessageId);
    }

}
