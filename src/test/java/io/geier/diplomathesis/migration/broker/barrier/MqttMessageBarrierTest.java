package io.geier.diplomathesis.migration.broker.barrier;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.BaseTest;
import io.geier.diplomathesis.migration.broker.MigrationMessageStore;
import io.geier.diplomathesis.migration.broker.MigrationStore;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MqttMessageBarrierTest extends BaseTest {

    private final String CLIENTID = "client X";
    private final String CLIENTID_NOT_MIGRATING = "client Y";
    private final String TOPIC_A = "topic A";

    private MqttMessageBarrier messageBarrier;
    private MigrationStore migrationStore;
    private MqttMessageBarrierQoS0 messageBarrierQoS0;
    private MqttMessageBarrierQoS1 messageBarrierQoS1;
    private MqttMessageBarrierQoS2 messageBarrierQoS2;

    private GlobalMessageId globalMessageId;
    private Migration migration;
    private Migration.Subscription subscription;

    @Before
    public void setUp() {
        migrationStore = mock(MigrationStore.class);
        doReturn(true).when(migrationStore).isMigratingByClientId(eq(CLIENTID));
        doReturn(false).when(migrationStore).isMigratingByClientId(eq(CLIENTID_NOT_MIGRATING));
        messageBarrierQoS0 = mock(MqttMessageBarrierQoS0.class);
        messageBarrierQoS1 = mock(MqttMessageBarrierQoS1.class);
        messageBarrierQoS2 = mock(MqttMessageBarrierQoS2.class);

        globalMessageId = mock(GlobalMessageId.class);
        migration = mock(Migration.class);
        when(migrationStore.getMigrationByClientId(eq(CLIENTID))).thenReturn(migration);
        subscription = new Migration.Subscription(migration, TOPIC_A, MqttQoS.EXACTLY_ONCE);
        when(migration.getSubscriptionByTopicName(eq(TOPIC_A))).thenReturn(subscription);

        messageBarrier = new MqttMessageBarrier(migrationStore, mock(MigrationMessageStore.class), messageBarrierQoS0, messageBarrierQoS1, messageBarrierQoS2);
    }

    @Test
    public void canSendMessage_withNonMigratingClient_shouldBeTrue() {
        boolean result = messageBarrier.canSendMessage(null, CLIENTID_NOT_MIGRATING, 0, "", MqttQoS.EXACTLY_ONCE, null);

        verify(messageBarrierQoS0, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS1, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS2, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));

        assertTrue(result);
    }

    @Test
    public void canSendMessage_withQos0_shouldCallBarrierQos0() {
        subscription.setQos(MqttQoS.AT_MOST_ONCE);
        messageBarrier.canSendMessage(null, CLIENTID, 0, TOPIC_A, MqttQoS.AT_MOST_ONCE, globalMessageId);

        verify(messageBarrierQoS0).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS1, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS2, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
    }

    @Test
    public void canSendMessage_withQos1_shouldCallBarrierQos1() {
        subscription.setQos(MqttQoS.AT_LEAST_ONCE);
        messageBarrier.canSendMessage(null, CLIENTID, 0, TOPIC_A, MqttQoS.AT_LEAST_ONCE, globalMessageId);

        verify(messageBarrierQoS0, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS1).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS2, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
    }

    @Test
    public void canSendMessage_withQos2_shouldCallBarrierQos2() {
        messageBarrier.canSendMessage(null, CLIENTID, 0, TOPIC_A, MqttQoS.EXACTLY_ONCE, globalMessageId);

        verify(messageBarrierQoS0, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS1, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS2).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
    }

    @Test
    public void canSendMessage_withByPassMessage_shouldReturnTrue() {
        subscription.addMessageToByPass(globalMessageId);

        messageBarrier.canSendMessage(null, CLIENTID, 0, TOPIC_A, MqttQoS.EXACTLY_ONCE, globalMessageId);

        verify(messageBarrierQoS0, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS1, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
        verify(messageBarrierQoS2, never()).canSendMessage(eq(subscription), eq(globalMessageId), eq(migration));
    }

}
