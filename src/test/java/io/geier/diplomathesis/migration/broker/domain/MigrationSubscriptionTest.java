package io.geier.diplomathesis.migration.broker.domain;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class MigrationSubscriptionTest {

    private final String TOPIC_A = "topic A";

    private GlobalMessageId globalMessageId;
    private Migration migration;
    private Migration.Subscription subscription;

    @Before
    public void setUp() {
        globalMessageId = mock(GlobalMessageId.class);
        migration = mock(Migration.class);
        subscription = new Migration.Subscription(migration, TOPIC_A, MqttQoS.AT_MOST_ONCE);
    }

    @Test
    public void addMessageToByPass() {
        subscription.addMessageToByPass(globalMessageId);

        Set<GlobalMessageId> globalMessageIdSet = subscription.getMessagesToByPass();
        assertNotNull(globalMessageIdSet);
        assertTrue(globalMessageIdSet.contains(globalMessageId));
    }

    @Test
    public void clearMessageByPass() {
        subscription.addMessageToByPass(globalMessageId);
        Set<GlobalMessageId> messagesToByPass = subscription.getMessagesToByPass();
        assertNotNull(messagesToByPass);
        assertEquals(1, messagesToByPass.size());

        subscription.clearMessageByPass();

        messagesToByPass = subscription.getMessagesToByPass();
        assertEquals(0, messagesToByPass.size());
    }

    @Test
    public void canByPass_withUnknownMessage_shouldReturnFalse() {
        boolean result = subscription.canMessageByPass(globalMessageId);

        assertFalse(result);
    }

    @Test
    public void canByPass_withMessage_shouldReturnTrue() {
        subscription.addMessageToByPass(globalMessageId);

        boolean result = subscription.canMessageByPass(globalMessageId);

        assertTrue(result);
    }
}
