package io.geier.diplomathesis.migration.messaging;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import org.mockito.ArgumentMatcher;

import static org.mockito.ArgumentMatchers.argThat;

public class GlobalMessageIdMatcher implements ArgumentMatcher<GlobalMessageId> {

    private GlobalMessageId left;

    public GlobalMessageIdMatcher(GlobalMessageId left) {
        this.left = left;
    }

    @Override
    public boolean matches(GlobalMessageId right) {
        return left.isEqualTo(right);
    }

    @Override
    public String toString() {
        return left.toString();
    }

    public static GlobalMessageId eqGlobalMessageId(GlobalMessageId left) {
        return argThat(new GlobalMessageIdMatcher(left));
    }
}
