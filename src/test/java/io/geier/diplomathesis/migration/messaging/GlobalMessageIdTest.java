package io.geier.diplomathesis.migration.messaging;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import org.junit.Assert;
import org.junit.Test;

public class GlobalMessageIdTest {

    @Test
    public void isBeforeOrEqualTo_withBefore_shouldReturnTrue() {
        GlobalMessageId globalMessageId = new GlobalMessageId("topic2-43");
        GlobalMessageId otherGlobalMessageId = new GlobalMessageId("topic2-44");

        boolean result = globalMessageId.isLessOrEqualTo(otherGlobalMessageId);

        Assert.assertTrue(result);
    }

    @Test
    public void isBeforeOrEqualTo_withEqual_shouldReturnTrue() {
        GlobalMessageId globalMessageId = new GlobalMessageId("topic2-44");
        GlobalMessageId otherGlobalMessageId = new GlobalMessageId("topic2-44");

        boolean result = globalMessageId.isLessOrEqualTo(otherGlobalMessageId);

        Assert.assertTrue(result);
    }

    @Test
    public void isBeforeOrEqualTo_withAfter_shouldReturnFalse() {
        GlobalMessageId globalMessageId = new GlobalMessageId("topic2-45");
        GlobalMessageId otherGlobalMessageId = new GlobalMessageId("topic2-44");

        boolean result = globalMessageId.isLessOrEqualTo(otherGlobalMessageId);

        Assert.assertFalse(result);
    }

    @Test
    public void isEqualTo_withBefore_shouldReturnFalse() {
        GlobalMessageId globalMessageId = new GlobalMessageId("topic2-43");
        GlobalMessageId otherGlobalMessageId = new GlobalMessageId("topic2-44");

        boolean result = globalMessageId.isEqualTo(otherGlobalMessageId);

        Assert.assertFalse(result);
    }

    @Test
    public void isEqualTo_withEqual_shouldReturnTrue() {
        GlobalMessageId globalMessageId = new GlobalMessageId("topic2-44");
        GlobalMessageId otherGlobalMessageId = new GlobalMessageId("topic2-44");

        boolean result = globalMessageId.isEqualTo(otherGlobalMessageId);

        Assert.assertTrue(result);
    }

    @Test
    public void isEqualTo_withAfter_shouldReturnFalse() {
        GlobalMessageId globalMessageId = new GlobalMessageId("topic2-45");
        GlobalMessageId otherGlobalMessageId = new GlobalMessageId("topic2-44");

        boolean result = globalMessageId.isEqualTo(otherGlobalMessageId);

        Assert.assertFalse(result);
    }

    @Test
    public void min_withFirstIsHigher() {
        GlobalMessageId globalMessageId1 = new GlobalMessageId("topic2-45");
        GlobalMessageId globalMessageId2 = new GlobalMessageId("topic2-44");

        GlobalMessageId result = GlobalMessageId.min(globalMessageId1, globalMessageId2);

        Assert.assertEquals(result.toString(), globalMessageId2.toString());
    }

    @Test
    public void min_withSecondIsHigher() {
        GlobalMessageId globalMessageId1 = new GlobalMessageId("topic2-45");
        GlobalMessageId globalMessageId2 = new GlobalMessageId("topic2-46");

        GlobalMessageId result = GlobalMessageId.min(globalMessageId1, globalMessageId2);

        Assert.assertEquals(result.toString(), globalMessageId1.toString());
    }

    @Test
    public void min_withBothEqual() {
        GlobalMessageId globalMessageId1 = new GlobalMessageId("topic2-45");
        GlobalMessageId globalMessageId2 = new GlobalMessageId("topic2-45");

        GlobalMessageId result = GlobalMessageId.min(globalMessageId1, globalMessageId2);

        Assert.assertEquals(result.toString(), globalMessageId1.toString());
    }

    @Test
    public void max_withFirstIsHigher() {
        GlobalMessageId globalMessageId1 = new GlobalMessageId("topic2-45");
        GlobalMessageId globalMessageId2 = new GlobalMessageId("topic2-44");

        GlobalMessageId result = GlobalMessageId.max(globalMessageId1, globalMessageId2);

        Assert.assertEquals(result.toString(), globalMessageId1.toString());
    }

    @Test
    public void max_withSecondIsHigher() {
        GlobalMessageId globalMessageId1 = new GlobalMessageId("topic2-45");
        GlobalMessageId globalMessageId2 = new GlobalMessageId("topic2-46");

        GlobalMessageId result = GlobalMessageId.max(globalMessageId1, globalMessageId2);

        Assert.assertEquals(result.toString(), globalMessageId2.toString());
    }

    @Test
    public void max_withBothEqual() {
        GlobalMessageId globalMessageId1 = new GlobalMessageId("topic2-45");
        GlobalMessageId globalMessageId2 = new GlobalMessageId("topic2-45");

        GlobalMessageId result = GlobalMessageId.max(globalMessageId1, globalMessageId2);

        Assert.assertEquals(result.toString(), globalMessageId1.toString());
    }
}
