package io.geier.diplomathesis.migration.simulation;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.ResultCaptor;
import io.geier.diplomathesis.migration.broker.BrokerMigrationBridge;
import io.geier.diplomathesis.migration.broker.BrokerMigrationHandler;
import io.geier.diplomathesis.migration.broker.MigrationMessageStore;
import io.geier.diplomathesis.migration.broker.MigrationStore;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0Target;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1Target;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2Target;
import io.geier.diplomathesis.migration.broker.domain.Migration;
import io.geier.diplomathesis.migration.broker.domain.Subscription;
import io.geier.diplomathesis.migration.communication.MigrationPacketClient;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncPacket;
import io.geier.diplomathesis.migration.communication.packet.MigToAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigToPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.geier.diplomathesis.migration.messaging.GlobalMessageIdMatcher;
import io.netty.channel.ChannelFuture;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.netty.util.concurrent.GenericFutureListener;
import org.junit.Before;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public abstract class AbstractTargetScenarioTest extends AbstractScenarioTest {

    protected static final String MIGRATION_ID = "1234567890";
    protected static final String TOPIC = "MyTopic";
    protected static final String PUBLISHER = "IAmPublisher";
    private static final String BROKER_SOURCE_HOST = "10.0.0.1";
    private static final int BROKER_SOURCE_PORT = 1234;
    private static final String BROKER_TARGET_HOST = "10.0.0.2";
    private static final int BROKER_TARGET_PORT = 1234;

    protected static final String CLIENT_A_HOST = "10.0.1.1";
    protected static final int CLIENT_A_PORT = 1234;
    protected static final String CLIENT_A_ID = "Client A";

    private BrokerMigrationHandler migrationHandler;
    private MigrationPacketService messageService;
    private BrokerMigrationBridge migrationBridge;
    protected MigrationMessageStore migrationMessageStore;
    protected MigrationStore migrationStore;
    private MqttMessageBarrier messageBarrier;
    protected Environment env;
    private MigrationPacketClient messageClientToTarget;
    private MigrationPacketClient messageClientToSource;
    private MigrationPacketClient messageClientToClient;
    protected Migration migration;
    protected Migration.Subscription subscription;

    protected abstract MqttQoS getQoS();

    @Before
    public void setUp() {
        migrationBridge = mock(BrokerMigrationBridge.class);
        List<Subscription> subscriptions = new ArrayList<>();
        subscriptions.add(new Subscription(CLIENT_A_ID, TOPIC, getQoS()));
        when(migrationBridge.getSubscriptionsByClientId(CLIENT_A_ID)).thenReturn(subscriptions);
        messageService = mock(MigrationPacketService.class);
        migrationMessageStore = spy(new MigrationMessageStore());
        migrationStore = new MigrationStore();
        messageBarrier = spy(new MqttMessageBarrier(migrationStore,
                migrationMessageStore,
                new MqttMessageBarrierQoS0(new MqttMessageBarrierQoS0Source(), new MqttMessageBarrierQoS0Target()),
                new MqttMessageBarrierQoS1(new MqttMessageBarrierQoS1Source(), new MqttMessageBarrierQoS1Target()),
                new MqttMessageBarrierQoS2(new MqttMessageBarrierQoS2Source(), new MqttMessageBarrierQoS2Target())));
        migrationHandler = spy(new BrokerMigrationHandler(
                BROKER_SOURCE_HOST,
                BROKER_SOURCE_PORT,
                messageBarrier,
                migrationStore,
                migrationMessageStore,
                migrationBridge,
                messageService
        ));

        messageClientToTarget = mock(MigrationPacketClient.class);
        messageClientToSource = mock(MigrationPacketClient.class);
        messageClientToClient = mock(MigrationPacketClient.class);
        when(messageService.createPacketClient(eq(BROKER_TARGET_HOST), eq(BROKER_TARGET_PORT), any())).thenReturn(messageClientToTarget);
        when(messageService.createPacketClient(any())).thenReturn(messageClientToSource);
        when(messageService.createPacketClient(eq(CLIENT_A_HOST), eq(CLIENT_A_PORT), any())).thenReturn(messageClientToClient);

        ChannelFuture channelFuture = mock(ChannelFuture.class);
        doAnswer(invocation -> {
            GenericFutureListener l = invocation.getArgument(0);
            l.operationComplete(null);
            return channelFuture;
        }).when(channelFuture).addListener(any());
        doReturn(channelFuture).when(messageClientToSource).send(any());

        when(migrationBridge.clientIdExists(CLIENT_A_ID)).thenReturn(false);

        env = new Environment(migrationHandler, migrationMessageStore, messageBarrier, PUBLISHER, TOPIC, getQoS(), CLIENT_A_ID);
        // delegate published messages
        doAnswer(invocation -> {
            String clientId = invocation.getArgument(0);
            List<MigrationMessageStore.StoredMessage> storedMessages = invocation.getArgument(1);
            for (MigrationMessageStore.StoredMessage storedMessage : storedMessages) {
                env.publishMessage(null, clientId, 0, storedMessage.getTopicName(), storedMessage.getQos(), storedMessage.getGlobalMessageId());
            }
            return null;
        }).when(migrationBridge).publish(any(), any());
    }

    protected void clientConnect() {
        when(migrationBridge.clientIdExists(CLIENT_A_ID)).thenReturn(true);
        env.incomingPacket(new MigToAckPacket(MIGRATION_ID));
    }

    protected MigratePacket migrate() {
        migrationHandler.migrate(BROKER_TARGET_HOST, BROKER_TARGET_PORT, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT);

        ArgumentCaptor<MigratePacket> messageCaptor = ArgumentCaptor.forClass(MigratePacket.class);
        verify(messageClientToTarget).sendSync(messageCaptor.capture());
        MigratePacket migratePacket = messageCaptor.getValue();

        migration = migrationStore.getMigration(migratePacket.getMigrationId());
        subscription = migration.getSubscriptionByTopicName(TOPIC);

        return migratePacket;
    }

    protected MigSyncAckPacket assertMigSyncAck() {
        ArgumentCaptor<MigSyncAckPacket> messageCaptor = ArgumentCaptor.forClass(MigSyncAckPacket.class);
        // we set times 2, since in migrate we already have one and this is the second call
        verify(messageClientToTarget, times(2)).sendSync(messageCaptor.capture());
        MigSyncAckPacket message = messageCaptor.getValue();
        return message;
    }

    protected MigToPacket assertMigTo() {
        ArgumentCaptor<MigToPacket> messageCaptor = ArgumentCaptor.forClass(MigToPacket.class);
        verify(messageClientToClient, times(1)).sendSync(messageCaptor.capture());
        MigToPacket message = messageCaptor.getValue();

        migration = migrationStore.getMigration(message.getMigrationId());
        subscription = migration.getSubscriptionByTopicName(TOPIC);

        return message;
    }

    protected MigSyncPacket assertMigSync() {
        ArgumentCaptor<MigSyncPacket> messageCaptor = ArgumentCaptor.forClass(MigSyncPacket.class);
        verify(messageClientToSource, times(1)).sendSync(messageCaptor.capture());
        MigSyncPacket message = messageCaptor.getValue();
        return message;
    }

    protected MigAckPacket assertMigAck() {
        ArgumentCaptor<MigAckPacket> messageCaptor = ArgumentCaptor.forClass(MigAckPacket.class);
        verify(messageClientToSource, times(1)).sendSync(messageCaptor.capture());
        MigAckPacket message = messageCaptor.getValue();
        return message;
    }

    protected MigAckPacket assertMigAckWithSync() {
        ArgumentCaptor<MigAckPacket> messageCaptor = ArgumentCaptor.forClass(MigAckPacket.class);
        verify(messageClientToSource, times(2)).sendSync(messageCaptor.capture());
        MigAckPacket message = messageCaptor.getValue();
        return message;
    }

    protected MigAckPacket assertMigAckAsync() {
        ArgumentCaptor<MigAckPacket> messageCaptor = ArgumentCaptor.forClass(MigAckPacket.class);
        verify(messageClientToSource, times(1)).send(messageCaptor.capture());
        MigAckPacket message = messageCaptor.getValue();
        return message;
    }

    protected MigAckPacket assertMigAckAsyncWithSync() {
        ArgumentCaptor<MigAckPacket> messageCaptor = ArgumentCaptor.forClass(MigAckPacket.class);
        verify(messageClientToSource, times(2)).send(messageCaptor.capture());
        MigAckPacket message = messageCaptor.getValue();
        return message;
    }

    protected void assertLastProcId(MigratePacket migratePacket, int messageId) {
        assertTrue(new GlobalMessageId(PUBLISHER, "" + messageId).isEqualTo(migratePacket.getSubscriptions().iterator().next().getLastProcessedGlobalMessageId()));
    }

    protected void assertMessageUnprocessed(int messageId) {
        verify(messageBarrier, never()).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
    }

    protected void assertMessageSend(int messageId) {
        boolean canSendMessage = verify(messageBarrier).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
        assertTrue(canSendMessage);
    }

    protected void assertMessageDiscard(int messageId) {
        boolean canSendMessage = verify(messageBarrier).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
        assertFalse(canSendMessage);
    }

    protected void incomingAndAssertUnprocessed(int messageId) {
        env.incomingMessage(messageId);
        verify(messageBarrier, never()).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
    }

    protected void incomingAndAssertSend(int messageId) {
        ResultCaptor<Boolean> resultCaptor = new ResultCaptor<>();
        doAnswer(resultCaptor).when(messageBarrier).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));

        env.incomingMessage(messageId);

        assertTrue(resultCaptor.getResult());
        verify(messageBarrier).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
    }

    protected void incomingAndAssertSendUnprocessed(int messageId) {
        ResultCaptor<Boolean> messageBarrierProgressMessageResultCaptor = new ResultCaptor<>();
        doAnswer(messageBarrierProgressMessageResultCaptor).when(messageBarrier).processMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));

        env.incomingMessage(messageId);

        verify(messageBarrier, never()).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
        assertFalse(messageBarrierProgressMessageResultCaptor.getResult());
        verify(messageBarrier).processMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
    }

    protected void incomingAndAssertDiscard(int messageId) {
        ResultCaptor<Boolean> messageBarrierCanSendMessageResultCaptor = new ResultCaptor<>();
        doAnswer(messageBarrierCanSendMessageResultCaptor).when(messageBarrier).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
        ResultCaptor<Boolean> messageBarrierProgressMessageResultCaptor = new ResultCaptor<>();
        doAnswer(messageBarrierProgressMessageResultCaptor).when(messageBarrier).processMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));

        env.incomingMessage(messageId);

        assertFalse(messageBarrierCanSendMessageResultCaptor.getResult());
        verify(messageBarrier).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
        assertTrue(messageBarrierProgressMessageResultCaptor.getResult());
        verify(messageBarrier).processMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
        verify(migrationMessageStore, never()).store(eq(CLIENT_A_ID), eq(TOPIC), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)), any());
    }

    protected void incomingAndAssertDiscardStored(int messageId) {
        ResultCaptor<Boolean> resultCaptor = new ResultCaptor<>();
        doAnswer(resultCaptor).when(messageBarrier).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));

        env.incomingMessage(messageId);

        assertFalse(resultCaptor.getResult());
        verify(messageBarrier).canSendMessage(any(), eq(CLIENT_A_ID), anyInt(), eq(TOPIC), eq(getQoS()), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)));
        verify(migrationMessageStore).store(eq(CLIENT_A_ID), eq(TOPIC), GlobalMessageIdMatcher.eqGlobalMessageId(new GlobalMessageId(PUBLISHER, "" + messageId)), any());
    }

}
