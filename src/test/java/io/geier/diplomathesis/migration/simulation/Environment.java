package io.geier.diplomathesis.migration.simulation;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.messaging.MqttPayload;
import io.geier.diplomathesis.messaging.MqttPayloadBody;
import io.geier.diplomathesis.messaging.MqttPayloadHeader;
import io.geier.diplomathesis.migration.broker.BrokerMigrationHandler;
import io.geier.diplomathesis.migration.broker.MigrationMessageStore;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.communication.packet.MigrationPacket;
import io.netty.handler.codec.mqtt.MqttFixedHeader;
import io.netty.handler.codec.mqtt.MqttMessageType;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.handler.codec.mqtt.MqttPublishVariableHeader;
import io.netty.handler.codec.mqtt.MqttQoS;

public class Environment {

    private BrokerMigrationHandler migrationHandler;
    private MigrationMessageStore migrationMessageStore;
    private MqttMessageBarrier messageBarrier;
    private String publisher;
    private String topic;
    private MqttQoS qos;
    private String clientId;

    public Environment(BrokerMigrationHandler migrationHandler,
                       MigrationMessageStore migrationMessageStore,
                       MqttMessageBarrier messageBarrier,
                       String publisher,
                       String topic,
                       MqttQoS qos,
                       String clientId) {
        this.migrationHandler = migrationHandler;
        this.migrationMessageStore = migrationMessageStore;
        this.messageBarrier = messageBarrier;
        this.publisher = publisher;
        this.topic = topic;
        this.qos = qos;
        this.clientId = clientId;
    }

    public void incomingMessage(int messageId) {
        GlobalMessageId globalMessageId = new GlobalMessageId(publisher, "" + messageId);
        MqttFixedHeader fixedHeader = new MqttFixedHeader(MqttMessageType.PUBLISH, false, qos, false, 0);
        MqttPublishVariableHeader variableHeader = new MqttPublishVariableHeader(topic, messageId);
        MqttPayload payload = new MqttPayload(new MqttPayloadHeader(globalMessageId), new MqttPayloadBody("test payload ;)"));
        MqttPublishMessage publishMessage = new MqttPublishMessage(fixedHeader, variableHeader, payload.toByteBuf());

        migrationMessageStore.processedMessage(publishMessage);
        publishMessage(publishMessage, clientId, messageId, topic, qos, globalMessageId);
    }

    public void publishMessage(MqttPublishMessage publishMessage, String clientId, int messageId, String topic, MqttQoS qos, GlobalMessageId globalMessageId) {
        messageBarrier.processMessage(publishMessage, clientId, messageId, topic, qos, globalMessageId);
    }

    public void incomingPacket(MigrationPacket message) {
        migrationHandler.handle(null, message);
    }

}
