package io.geier.diplomathesis.migration.simulation;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.BrokerMigrationHandler;
import io.geier.diplomathesis.migration.broker.MigrationMessageStore;
import io.geier.diplomathesis.migration.broker.MigrationStore;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrier;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS0Target;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS1Target;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2Source;
import io.geier.diplomathesis.migration.broker.barrier.MqttMessageBarrierQoS2Target;
import io.geier.diplomathesis.migration.broker.BrokerMigrationBridge;
import io.geier.diplomathesis.migration.communication.MigrationPacketService;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class EnvironmentTest {

    private static final MqttQoS QOS = MqttQoS.AT_MOST_ONCE;
    private static final String TOPIC = "MyTopic";
    private static final String PUBLISHER = "IAmPublisher";

    private static final String BROKER_SOURCE_HOST = "10.0.0.1";
    private static final int BROKER_SOURCE_PORT = 1234;
    private static final String CLIENT_A_ID = "Client A";
    private static final String CLIENT_B_ID = "Client B";

    private BrokerMigrationHandler migrationHandler;
    private MigrationPacketService messageServiceSource;
    private BrokerMigrationBridge migrationBridgeSource;
    private MigrationMessageStore migrationMessageStore;
    private MigrationStore migrationStore;
    private MqttMessageBarrier messageBarrier;
    private Environment env;

    @Before
    public void setUp() {
        migrationBridgeSource = mock(BrokerMigrationBridge.class);
        when(migrationBridgeSource.clientIdExists(CLIENT_A_ID)).thenReturn(true);
        when(migrationBridgeSource.clientIdExists(CLIENT_B_ID)).thenReturn(true);
        messageServiceSource = mock(MigrationPacketService.class);
        migrationMessageStore = new MigrationMessageStore();
        migrationStore = new MigrationStore();
        messageBarrier = new MqttMessageBarrier(migrationStore,
                migrationMessageStore,
                new MqttMessageBarrierQoS0(new MqttMessageBarrierQoS0Source(), new MqttMessageBarrierQoS0Target()),
                new MqttMessageBarrierQoS1(new MqttMessageBarrierQoS1Source(), new MqttMessageBarrierQoS1Target()),
                new MqttMessageBarrierQoS2(new MqttMessageBarrierQoS2Source(), new MqttMessageBarrierQoS2Target()));
        migrationHandler = spy(new BrokerMigrationHandler(
                BROKER_SOURCE_HOST,
                BROKER_SOURCE_PORT,
                messageBarrier,
                migrationStore,
                migrationMessageStore,
                migrationBridgeSource,
                messageServiceSource
        ));

        env = new Environment(migrationHandler, migrationMessageStore, messageBarrier, PUBLISHER, TOPIC, QOS, CLIENT_A_ID);
    }

    @Test
    public void incoming() {
        env.incomingMessage(12);

        GlobalMessageId globalMessageId = migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(TOPIC);
        assertTrue(new GlobalMessageId(PUBLISHER, "12").isEqualTo(globalMessageId));

        env.incomingMessage(13);

        globalMessageId = migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(TOPIC);
        assertTrue(new GlobalMessageId(PUBLISHER, "13").isEqualTo(globalMessageId));
    }

    @Test
    public void incoming_withNoIncoming_isNull() {
        GlobalMessageId globalMessageId = migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(TOPIC);
        assertNull(globalMessageId);
    }

}
