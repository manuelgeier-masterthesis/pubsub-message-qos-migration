package io.geier.diplomathesis.migration.simulation;

import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QoS0SourceGapScenarioTest extends AbstractSourceScenarioTest {

    @Override
    protected MqttQoS getQoS() {
        return MqttQoS.AT_MOST_ONCE;
    }

    @Test
    public void synced1() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

//        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced2() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
//        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced3() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
//        incomingAndAssertDiscard(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced4() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

//        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead1() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

//        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);

        env.incomingPacket(new MigAckPacket(migratePacket.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead2() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
//        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);

        env.incomingPacket(new MigAckPacket(migratePacket.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead3() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
//        incomingAndAssertDiscard(16);

        env.incomingPacket(new MigAckPacket(migratePacket.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead4() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);

        env.incomingPacket(new MigAckPacket(migratePacket.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

//        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void behind1() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

//        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);
        incomingAndAssertDiscard(17);
        incomingAndAssertDiscard(18);
        incomingAndAssertDiscard(19);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(20);
        incomingAndAssertUnprocessed(21);
    }

    @Test
    public void behind2() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
//        incomingAndAssertDiscard(16);
        incomingAndAssertDiscard(17);
        incomingAndAssertDiscard(18);
        incomingAndAssertDiscard(19);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(20);
        incomingAndAssertUnprocessed(21);
    }

    @Test
    public void behind3() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);
        incomingAndAssertDiscard(17);
        incomingAndAssertDiscard(18);
//        incomingAndAssertDiscard(19);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(20);
        incomingAndAssertUnprocessed(21);
    }

    @Test
    public void behind4() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscard(14);
        incomingAndAssertDiscard(15);
        incomingAndAssertDiscard(16);
        incomingAndAssertDiscard(17);
        incomingAndAssertDiscard(18);
        incomingAndAssertDiscard(19);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

//        incomingAndAssertUnprocessed(20);
        incomingAndAssertUnprocessed(21);
    }
}
