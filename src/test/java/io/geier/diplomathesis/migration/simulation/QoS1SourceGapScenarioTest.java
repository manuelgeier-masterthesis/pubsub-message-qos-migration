package io.geier.diplomathesis.migration.simulation;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class QoS1SourceGapScenarioTest extends AbstractSourceScenarioTest {

    @Override
    protected MqttQoS getQoS() {
        return MqttQoS.AT_LEAST_ONCE;
    }

    @Test
    public void synced1() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

//        incomingAndAssertSend(14);
        incomingAndAssertSend(15);
        incomingAndAssertSend(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced2() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
//        incomingAndAssertSend(15);
        incomingAndAssertSend(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced3() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);
//        incomingAndAssertSend(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced4() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);
        incomingAndAssertSend(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

//        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead1() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

//        incomingAndAssertSend(14);
        incomingAndAssertSend(15);
        incomingAndAssertSend(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead2() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
//        incomingAndAssertSend(15);
        incomingAndAssertSend(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead3() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);
//        incomingAndAssertSend(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead4() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);
        incomingAndAssertSend(16);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

//        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void behind1() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

//        incomingAndAssertSend(14);
        incomingAndAssertSend(15);

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertSend(19);
        incomingAndAssertSend(20);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
    }

    @Test
    public void behind2() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
//        incomingAndAssertSend(15);

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertSend(19);
        incomingAndAssertSend(20);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
    }

    @Test
    public void behind3() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());

//        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertSend(19);
        incomingAndAssertSend(20);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
    }

    @Test
    public void behind4() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());

        incomingAndAssertSend(16);
//        incomingAndAssertSend(17);
        incomingAndAssertSend(18);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertSend(19);
        incomingAndAssertSend(20);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
    }

    @Test
    public void behind5() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
//        incomingAndAssertSend(18);

        // it should be synced with 18, but since it is missing, it will be synced with 19
        incomingAndAssertSend(19);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertSend(20);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
    }

    @Test
    public void behind6() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

//        incomingAndAssertSend(19);
        incomingAndAssertSend(20);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
    }

    @Test
    public void behind7() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertSend(19);
//        incomingAndAssertSend(20);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
    }

    @Test
    public void behind8() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);

        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertSend(14);
        incomingAndAssertSend(15);

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertSend(19);
        incomingAndAssertSend(20);

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

//        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
    }

}
