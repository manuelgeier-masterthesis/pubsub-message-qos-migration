package io.geier.diplomathesis.migration.simulation;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.geier.diplomathesis.migration.communication.packet.MigSyncAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class QoS1TargetScenarioTest extends AbstractTargetScenarioTest {

    @Override
    protected MqttQoS getQoS() {
        return MqttQoS.AT_LEAST_ONCE;
    }

    @Test
    public void synced() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);
        assertTrue(new GlobalMessageId(PUBLISHER, "" + 13).isEqualTo(migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(TOPIC)));

        List<MigratePacket.MigrateSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigratePacket.MigrateSubscription(getQoS(), TOPIC, new GlobalMessageId(PUBLISHER, "" + 13)));
        env.incomingPacket(new MigratePacket(MIGRATION_ID, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT, subscriptions));
        assertMigTo();

        clientConnect();
        assertMigAckAsync();
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertSendUnprocessed(14);
        incomingAndAssertSendUnprocessed(15);
        incomingAndAssertSendUnprocessed(16);
        incomingAndAssertSendUnprocessed(17);
        incomingAndAssertSendUnprocessed(18);
        incomingAndAssertSendUnprocessed(19);
    }

    @Test
    public void ahead() {
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);
        incomingAndAssertUnprocessed(14);
        incomingAndAssertUnprocessed(15);
        incomingAndAssertUnprocessed(16);
        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);

        assertTrue(new GlobalMessageId(PUBLISHER, "" + 18).isEqualTo(migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(TOPIC)));

        List<MigratePacket.MigrateSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigratePacket.MigrateSubscription(getQoS(), TOPIC, new GlobalMessageId(PUBLISHER, "" + 13)));
        env.incomingPacket(new MigratePacket(MIGRATION_ID, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT, subscriptions));
        assertMigTo();

        clientConnect();
        assertMigSync();
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING_OPPONENT, subscription.getState());

        incomingAndAssertSend(19);
        incomingAndAssertSend(20);
        incomingAndAssertSend(21);
        incomingAndAssertSend(22);
        incomingAndAssertSend(23);
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING_OPPONENT, subscription.getState());

        env.incomingPacket(new MigSyncAckPacket(MIGRATION_ID));

        assertMigAckAsync();
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertSendUnprocessed(24);
        incomingAndAssertSendUnprocessed(25);
        incomingAndAssertSendUnprocessed(26);
    }

    @Test
    public void behind() {
        incomingAndAssertUnprocessed(9);
        incomingAndAssertUnprocessed(10);

        assertTrue(new GlobalMessageId(PUBLISHER, "" + 10).isEqualTo(migrationMessageStore.getLastProcessedGlobalMessageIdForTopic(TOPIC)));

        List<MigratePacket.MigrateSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigratePacket.MigrateSubscription(getQoS(), TOPIC, new GlobalMessageId(PUBLISHER, "" + 13)));
        env.incomingPacket(new MigratePacket(MIGRATION_ID, CLIENT_A_ID, CLIENT_A_HOST, CLIENT_A_PORT, subscriptions));
        assertMigTo();

        clientConnect();
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING_THIS, subscription.getState());

        incomingAndAssertSend(11);
        incomingAndAssertSend(12);
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING_THIS, subscription.getState());
        incomingAndAssertSend(13);

        assertMigAckAsync();
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());

        incomingAndAssertSendUnprocessed(14);
        incomingAndAssertSendUnprocessed(15);
        incomingAndAssertSendUnprocessed(16);
        incomingAndAssertSendUnprocessed(17);
        incomingAndAssertSendUnprocessed(18);
        incomingAndAssertSendUnprocessed(19);
        incomingAndAssertSendUnprocessed(20);
        incomingAndAssertSendUnprocessed(21);
        incomingAndAssertSendUnprocessed(22);
    }
}
