package io.geier.diplomathesis.migration.simulation;

import io.geier.diplomathesis.messaging.GlobalMessageId;
import io.geier.diplomathesis.migration.communication.packet.MigAckPacket;
import io.geier.diplomathesis.migration.communication.packet.MigSyncPacket;
import io.geier.diplomathesis.migration.communication.packet.MigratePacket;
import io.geier.diplomathesis.migration.broker.domain.MigrationState;
import io.geier.diplomathesis.migration.broker.domain.MigrationSubscriptionState;
import io.netty.handler.codec.mqtt.MqttQoS;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class QoS2SourceGapScenarioTest extends AbstractSourceScenarioTest {

    @Override
    protected MqttQoS getQoS() {
        return MqttQoS.EXACTLY_ONCE;
    }

    @Test
    public void synced1() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

//        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced2() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
//        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced3() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
//        incomingAndAssertDiscardStored(16);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void synced4() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        assertEquals(3, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

//        incomingAndAssertUnprocessed(17);
        incomingAndAssertUnprocessed(18);
        incomingAndAssertUnprocessed(19);
    }

    @Test
    public void ahead1() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

//        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        assertEquals(1, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead2() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
//        incomingAndAssertDiscardStored(15);
        assertEquals(1, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead3() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

//        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead4() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertSend(16);
//        incomingAndAssertSend(17);
        incomingAndAssertSend(18);
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead5() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
//        incomingAndAssertSend(18);
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        // it should be synced with 18, but since it's missing, it's synced with 19
        incomingAndAssertDiscardStored(19);

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscardStored(20);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead6() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

//        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(1, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead7() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscardStored(19);
//        incomingAndAssertDiscardStored(20);
        assertEquals(1, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead8() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "18")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertEquals(MigrationState.SYNCING, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCING, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertSend(16);
        incomingAndAssertSend(17);
        incomingAndAssertSend(18);
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

//        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead_storeOnly1() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

//        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
        incomingAndAssertDiscardStored(18);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "16")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead_storeOnly2() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
//        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
        incomingAndAssertDiscardStored(18);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "16")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead_storeOnly4() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
        incomingAndAssertDiscardStored(18);
        assertEquals(5, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "16")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

//        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(3, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead_storeOnly5() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
        incomingAndAssertDiscardStored(18);
        assertEquals(5, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "16")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertDiscardStored(19);
//        incomingAndAssertDiscardStored(20);
        assertEquals(3, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead_storeOnly6() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
        incomingAndAssertDiscardStored(18);
        assertEquals(5, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "16")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());
        assertEquals(2, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

//        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void ahead_storeOnly3() {
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
//        incomingAndAssertDiscardStored(18);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        ArrayList<MigSyncPacket.MigSyncSubscription> subscriptions = new ArrayList<>();
        subscriptions.add(new MigSyncPacket.MigSyncSubscription(TOPIC, new GlobalMessageId(PUBLISHER, "16")));
        env.incomingPacket(new MigSyncPacket(migration.getMigrationId(), subscriptions));
        assertMigSyncAck();
        assertEquals(MigrationState.SYNCED, migration.getState());
        assertEquals(MigrationSubscriptionState.SYNCED, subscription.getState());
        assertEquals(1, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertDiscardStored(19);
        incomingAndAssertDiscardStored(20);
        assertEquals(3, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(21);
        incomingAndAssertUnprocessed(22);
        incomingAndAssertUnprocessed(23);
        incomingAndAssertUnprocessed(24);
        incomingAndAssertUnprocessed(25);
    }

    @Test
    public void behind1() {
        incomingAndAssertUnprocessed(9);
        incomingAndAssertUnprocessed(10);
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

//        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
        incomingAndAssertDiscardStored(18);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(19);
        incomingAndAssertUnprocessed(20);
    }

    @Test
    public void behind2() {
        incomingAndAssertUnprocessed(9);
        incomingAndAssertUnprocessed(10);
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
//        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
        incomingAndAssertDiscardStored(18);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(19);
        incomingAndAssertUnprocessed(20);
    }

    @Test
    public void behind3() {
        incomingAndAssertUnprocessed(9);
        incomingAndAssertUnprocessed(10);
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
//        incomingAndAssertDiscardStored(18);
        assertEquals(4, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        incomingAndAssertUnprocessed(19);
        incomingAndAssertUnprocessed(20);
    }

    @Test
    public void behind4() {
        incomingAndAssertUnprocessed(9);
        incomingAndAssertUnprocessed(10);
        incomingAndAssertUnprocessed(11);
        incomingAndAssertUnprocessed(12);
        incomingAndAssertUnprocessed(13);

        MigratePacket migratePacket = migrate();
        assertLastProcId(migratePacket, 13);
        assertEquals(MigrationState.INITIALIZED, migration.getState());
        assertEquals(MigrationSubscriptionState.INITIALIZED, subscription.getState());

        incomingAndAssertDiscardStored(14);
        incomingAndAssertDiscardStored(15);
        incomingAndAssertDiscardStored(16);
        incomingAndAssertDiscardStored(17);
        incomingAndAssertDiscardStored(18);
        assertEquals(5, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

        env.incomingPacket(new MigAckPacket(migration.getMigrationId(), MigAckPacket.MigStatus.OK));
        assertEquals(MigrationState.FINISHED, migration.getState());
        assertEquals(MigrationSubscriptionState.FINISHED, subscription.getState());
        assertEquals(0, migrationMessageStore.size(migration.getClient().getId(), TOPIC));

//        incomingAndAssertUnprocessed(19);
        incomingAndAssertUnprocessed(20);
    }

}
